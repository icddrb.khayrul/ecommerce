<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Unit_model extends CI_Model
{

    function unitListingCount($searchText = '')
    {

        $this->db->select('id, unit_name, unit_volume, active');
        $this->db->from('tbl_unit');
        if(!empty($searchText)) { $this->db->or_like('tbl_unit.unit_code', $searchText); $this->db->or_like('tbl_unit.unit_name', $searchText); }
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    function unitListing()
    {
        $this->db->select('id, unit_name,unit_volume, active');
        $this->db->from('tbl_unit');
        return $this->db->get('')->result();
        //return $query->result();
    }
    
    function getUnits()
    {
        $this->db->select('id, unit_name');
        $this->db->from('tbl_unit');
        $this->db->where('active', 1);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function addNewUnit($UnitInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_unit', $UnitInfo);
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function getUnitInfo($unit_id)
    {
        $this->db->select('id, unit_name,unit_volume, active');
        $this->db->from('tbl_unit');
        $this->db->where('id', $unit_id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function editUnit($UnitInfo, $unit_id)
    {
        $this->db->where('id', $unit_id);
        $this->db->update('tbl_unit', $UnitInfo);
        
        return TRUE;
    }
    
    

    function deleteUnit($id)
    {
		
        $this->db->where('id', $id);
        $this->db->delete('tbl_unit');
        
        return TRUE;
    }


 
}

  