<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model
{

    public function getAllProducts(){
        return $this->db->query("SELECT 
        p.`id` AS prod_id,
        chdcol.`name` AS color,
        chdsi.`name` AS size,
        cat.`name` AS category_name,
        p.`active`
        FROM
            tbl_products p
                LEFT JOIN
            tbl_base_setup_chd chdcol ON chdcol.chd_id = p.color_id
                LEFT JOIN
            tbl_base_setup_chd chdsi ON chdsi.chd_id = p.size_id
                LEFT JOIN
            tbl_category_list cat ON cat.id = p.cat_id")->result();
    }
    

 
}

  