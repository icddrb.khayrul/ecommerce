<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pi_model extends CI_Model
{

    function getPiType()
    {
        $this->db->select('id, name, remarks');
        $this->db->from('tbl_pi_list');
        $this->db->where('active', 1);
        $query = $this->db->get();

        return $query->result();
    }

	function piListing()
    {
        $this->db->select('id, name, remarks, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_pi_list');
        $query = $this->db->get();

        return $query->result();
    }


    function addNewPi($UnitInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_pi_list', $UnitInfo);
        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $insert_id;
    }

    function getPiInfo($id)
    {
        $this->db->select('id, name, remarks, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_pi_list');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->result();
    }


    function editPi($IDInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_pi_list', $IDInfo);

        return TRUE;
    }



}

