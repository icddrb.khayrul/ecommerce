<?php

    class Mail_model extends CI_Model {

      function __construct() {
    		parent::__construct();
        //$this->load->library('email');
        $this->load->model('Utilities');
    	}


    /**
     * send mail.
     *
     * @author MD. KHAYRUL HASAN
     * @param string $message
     * @param array $context
     * @return null
     */

      public function send_mail($table, $from, $to, $email_copy, $subject, $body){
        // $data = array('name' => $name,
        //               'user' => $user,
        // );

        // require 'gmail_app/class.phpmailer.php';
        // $mail             = new PHPMailer;
        // $mail->IsSMTP();
        // $mail->Host       = "mail.atilimited.net";
        // $mail->Port       = "465";
        // //$mail->SMTPDebug  = 2;
        // $mail->SMTPAuth   = true;
        // $mail->Username   = "dev@atilimited.net";
        // $mail->Password   = "@ti321$#";
        // $mail->SMTPSecure = 'ssl';
        // $mail->From       = "dev@atilimited.net";
        // $mail->FromName   = "Ahoskti Mukti";
        // $mail->AddAddress($email);
        // $mail->AddReplyTo('dev@atilimited.net');
        // $mail->WordWrap   = 1000;
        // $mail->IsHTML(TRUE);
        // $mail->Subject    = 'Welcome to '.$name;
        // $mail->Body       = $this->load->view('mail/welcome_doc', $data, TRUE);
        // $send             = $mail->Send();

        $mailData = array('email_from' => $from,
                          'email_to' => $to,
                          'subject' => $subject,
                          'email_copy' => $$email_copy,
                          'email_body' => $body,
                          'insert_by' => 1,
                          'insert_datetime' => date('Y-m-d H:i:s')
        );

        $this->Utilities->insertData($mailData, $table);



      }


    /**
     * mail body.
     *
     * @author MD. KHAYRUL HASAN
     * @param string $message
     * @param array $context
     * @return null
     */

     public function mailTemplate(){
      return $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
          <title>' . html_escape('sss') . '</title>
          <style type="text/css">
              body {
                  font-family: Arial, Verdana, Helvetica, sans-serif;
                  font-size: 16px;
              }
          </style>
      </head>
      <body>
      ' . "sssss ddd sss" . '
      </body>
      </html>';
     }



  }
