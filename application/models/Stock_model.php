<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Stock_model extends CI_Model
{

    public function getAllProduct(){
        return $this->db->query("SELECT 
        a.id,
        a.batch_id,
        b.batch_id AS batch_id_number,
        a.quantity,
        a.qnt_less,
        a.qnt_damage,
        a.qnt_return,
        a.active
        FROM
            tbl_stock_list a
                LEFT JOIN
            tbl_batch_list b ON b.id = a.batch_id")->result();
    }



    public function dropdownFromBatchWithCondition()
    {
        return $this->db->query('SELECT 
        a.id, a.batch_id
        FROM
            tbl_batch_list a
        WHERE
            a.id NOT IN (SELECT 
                    s.batch_id
                FROM
                    tbl_stock_list s)
        ')->result();
    }

    public function dropdownFromBatchWith()
    {
        return $this->db->query('SELECT 
        a.id, a.batch_id
        FROM
            tbl_batch_list a')->result();
    }
    

 
}

  