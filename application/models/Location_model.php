<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Location_model extends CI_Model
{

    

    function getLocationType()
    {
        $this->db->select('id, name');
        $this->db->from('tbl_location');
        $this->db->where('active', 1);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function locationListing()
    {
        $this->db->select('id, name, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_location');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function addNewLocation($IdInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_location', $IdInfo);
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function getLocationInfo($id)
    {
        $this->db->select('id, name, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_location');
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function editLocation($IDInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_location', $IDInfo);
        
        return TRUE;
    }
    
    

    function deleteLocation($id)
    {
		
        $this->db->where('id', $id);
        $this->db->delete('tbl_location');
        
        return TRUE;
    }


 
}

  