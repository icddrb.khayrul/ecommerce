<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Progress_model extends CI_Model
{

    function getProgressType()
    {
        $this->db->select('id, name, order');
        $this->db->from('tbl_progress_type');
        $this->db->where('active', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    function existProgressType()
    {
        $this->db->select('id, name, order');
        $this->db->from('tbl_progress_type');
        $this->db->where('active', 1);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function getProgressTypeByLastProgress($last_progress_id)
    {
        $this->db->select('id, name, order');
        $this->db->from('tbl_progress_type');
        $this->db->where('active', 1);
		$this->db->where('id >', $last_progress_id);
		 
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function getProgressTypeByLastProgressUpdate($last_progress_id)
    {
        $this->db->select('id, name, order');
        $this->db->from('tbl_progress_type');
        $this->db->where('active', 1);
		$this->db->where('id >=', $last_progress_id);
		 
        $query = $this->db->get();
        
        return $query->result();
    }
	

	
	function progressListing()
    {
        $this->db->select('id, name, order, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_progress_type');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function addNewProgress($UnitInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_progress_type', $UnitInfo);
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function getProgressInfo($id)
    {
        $this->db->select('id, name, order, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_progress_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function editProgress($IDInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_progress_type', $IDInfo);
        
        return TRUE;
    }
    

 
}

  