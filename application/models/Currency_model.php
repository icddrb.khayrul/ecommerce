<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Currency_model extends CI_Model
{

    function getCurrencyType()
    {
        $this->db->select('id, name, short_name');
        $this->db->from('tbl_currency_type');
        $this->db->where('active', 1);
        $query = $this->db->get();

        return $query->result();
    }

	function currencyListing()
    {
        $this->db->select('id, name, short_name, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_currency_type');
        $query = $this->db->get();

        return $query->result();
    }


    function addNewCurrency($UnitInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_currency_type', $UnitInfo);
        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $insert_id;
    }

    function getCurrencyInfo($id)
    {
        $this->db->select('id, name, short_name, bdt, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_currency_type');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->result();
    }


    function editCurrency($IDInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_currency_type', $IDInfo);

        return TRUE;
    }



}

