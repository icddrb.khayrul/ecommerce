<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Batch_model extends CI_Model
{

    public function getAllBatches(){
        return $this->db->query("SELECT 
        a.id,
        a.batch_id,
        a.prod_id,
        c.`name` AS category_name,
        a.base_price,
        a.selling_price,
        a.discount_price,
        a.active
        FROM
            tbl_batch_list a
                LEFT JOIN
            tbl_products p ON p.id = a.prod_id
                LEFT JOIN
            tbl_category_list c ON c.id = p.cat_id")->result();
    }



    public function dropdownFromProductWithCondition()
    {
        return $this->db->query('SELECT 
        a.id, b.name
        FROM
            tbl_products a
                LEFT JOIN
            tbl_category_list b ON a.cat_id = b.id')->result();
    }
    

 
}

  