<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Variance_model extends CI_Model
{

    function getVarianceType()
    {
        $this->db->select('id, name, order');
        $this->db->from('tbl_progress_variance_type');
        $this->db->where('active', 1);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function getVariance($progress_id)
    {
        $this->db->select('id, name');
        $this->db->from('tbl_progress_variance_type');
        $this->db->where('active', 1);
		$this->db->where('progress_id', $progress_id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function varianceListing()
    {
        $this->db->select('v.id, v.progress_id, v.name, v.order, v.active, v.insertedOn, v.insertedBy, v.updatedOn, v.updatedBy, p.name as progress_name');
        $this->db->from('tbl_progress_variance_type v');
		$this->db->join('tbl_progress_type p','p.id= v.progress_id', 'left');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function addNewVariance($UnitInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_progress_variance_type', $UnitInfo);
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function getVarianceInfo($id)
    {
        $this->db->select('id, progress_id, name, order, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_progress_variance_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function editVariance($IDInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_progress_variance_type', $IDInfo);
        
        return TRUE;
    }
    

 
}

  