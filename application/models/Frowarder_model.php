<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Frowarder_model extends CI_Model
{

    function getFrowarder($getFrowarder)
    {
        $this->db->select('id, name, address,shipment_mode');
        $this->db->from('tbl_frowarder_list');
        $this->db->where('active', 1);
        $this->db->where('shipment_mode', $getFrowarder);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function frowarderListing()
    {
        $this->db->select('id, name, address, active, insertedOn,shipment_mode, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_frowarder_list');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function addNewFrowarder($UnitInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_frowarder_list', $UnitInfo);
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function getFrowarderInfo($id)
    {
        $this->db->select('id, name, address, active, insertedOn, insertedBy, updatedOn, updatedBy,shipment_mode');
        $this->db->from('tbl_frowarder_list');
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function editFrowarder($IDInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_frowarder_list', $IDInfo);
        
        return TRUE;
    }
    

 
}

  