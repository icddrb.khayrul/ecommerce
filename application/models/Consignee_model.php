<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Consignee_model extends CI_Model
{

    function getConsignee()
    {
        $this->db->select('id, document_no, document_date, shipment_type, shipper_id, description_of_goods, remarks,shipper_remarks, pi_id, agent_id, etd, eta, hawb_hbl, mawb_mbl, frowader_id, no_of_package, weight, value, currency_id, document_received_date, shipment_release_date, status, progress_id, variant_id, insertOn, insertBy, updateBy, updateOn, updateByshiper, updateOnshipper');
        $this->db->from('tbl_documents');
        $this->db->where('active', 1);
        $query = $this->db->get();

        return $query->result();
    }

	function consigneeListing()
    {


        $this->db->select('bm.status bill_status, d.id, d.consignment_receive_id, d.document_no, d.shipment_type_no, d.document_date, d.shipment_type, d.shipper_id, d.description_of_goods, d.pi_id,(SELECT po.name FROM tbl_base_setup_chd po WHERE po.chd_id = d.remarks) remarks,d.shipper_remarks, d.agent_id, d.etd, d.eta, d.hawb_hbl,
        d.mawb_mbl, d.frowader_id, d.no_of_package, d.weight, d.value,(SELECT crn.short_name FROM tbl_currency_type crn WHERE d.currency_id = crn.id) shortName, d.currency_id, d.document_received_date, d.shipment_release_date, d.status, d.progress_id, d.variant_id, d.insertOn,
        d.insertOn, d.insertBy, d.updateBy, d.updateOn, d.updateByshiper, d.updateOnshipper, a.name as agentName, s.name as shipperName, p.name as PIName, f.name as frowaderName, dv.progress_date, dv.variance_date, pt.name as progressName, v.name as varianceName');
        $this->db->from('tbl_documents d');
        $this->db->join('tbl_shipper_list s', 'd.shipper_id=s.id', 'left');
        $this->db->join('tbl_billing_mst bm', 'd.id=bm.doc_id', 'left');
        $this->db->join('tbl_agent_list a', 'd.agent_id=a.id', 'left');
        $this->db->join('tbl_pi_list p', 'd.pi_id=p.id', 'left');
        $this->db->join('tbl_frowarder_list f', 'd.frowader_id=f.id', 'left');
        $this->db->join('tbl_documents_progress dv', 'd.document_progress_id_last=dv.id', 'left');
        $this->db->join('tbl_progress_type pt', 'd.progress_id=pt.id', 'left');
        $this->db->join('tbl_progress_variance_type v', 'd.variant_id=v.id', 'left');
        $this->db->or_where('bm.status NOT IN (2)');
        $this->db->or_where('bm.status IS NULL');
        $this->db->order_by('d.id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }


    function consigneeListingHistory($id)
    {
        $this->db->select('bm.status bill_status, bm.id AS bill_id,  d.id, d.document_no, d.document_date, d.shipment_type, d.shipment_type_no, d.shipper_id, d.description_of_goods, d.pi_id,d.remarks,d.shipper_remarks, d.agent_id, d.etd, d.eta, d.hawb_hbl,
		d.mawb_mbl, d.frowader_id, d.no_of_package, d.weight, d.value, d.currency_id, d.document_received_date, d.shipment_release_date, d.status, d.progress_id, d.variant_id, d.insertOn,
		d.insertOn, d.insertBy, d.updateBy, d.updateOn, d.updateByshiper, d.updateOnshipper, a.name as agentName, s.name as shipperName, p.name as PIName, f.name as frowaderName, dv.progress_date, dv.variance_date, pt.name as progressName, v.name as varianceName');
        $this->db->from('tbl_documents d');
         $this->db->join('tbl_shipper_list s', 'd.shipper_id=s.id', 'left');
         $this->db->join('tbl_billing_mst bm', 'd.id=bm.doc_id', 'left');
         $this->db->join('tbl_agent_list a', 'd.agent_id=a.id', 'left');
         $this->db->join('tbl_pi_list p', 'd.pi_id=p.id', 'left');
         $this->db->join('tbl_frowarder_list f', 'd.frowader_id=f.id', 'left');
        $this->db->join('tbl_documents_progress dv', 'd.document_progress_id_last=dv.id', 'left');
        $this->db->join('tbl_progress_type pt', 'd.progress_id=pt.id', 'left');
        $this->db->join('tbl_progress_variance_type v', 'd.variant_id=v.id', 'left');
        $this->db->where('bm.status IN (2)');

        if ($id > 0)
        {
         $this->db->where('d.agent_id', $id);
        }

        $this->db->order_by('d.id', 'DESC');
        $query = $this->db->get();

        return $query->result();
    }


    /**
     * Does something interesting
     *
     * @param Place   where  Where something interesting takes place
     * @param integer repeat How many times something interesting should happen
     *
     * @throws Some_Exception_Class If something interesting cannot happen
     * @author Md. Khayrul Hasan <khayrul.web@gmail.com>
     * @return Status
     */

    function pending_payment_receive_list($id)
    {
        $this->db->select('bm.status bill_status, bm.billing_id, bm.payment_receive_date_time, bm.id AS bill_id, bm.payment_receive_remarks,  d.id, d.document_no, d.document_date, d.shipment_type, d.shipment_type_no, d.shipper_id, d.description_of_goods, d.pi_id,d.remarks,d.shipper_remarks, d.agent_id, d.etd, d.eta, d.hawb_hbl,
		d.mawb_mbl, d.frowader_id, d.no_of_package, d.weight, d.value, d.currency_id, d.document_received_date, d.shipment_release_date, d.status, d.progress_id, d.variant_id, d.insertOn,
		d.insertOn, d.insertBy, d.updateBy, d.updateOn, d.updateByshiper, d.updateOnshipper, a.name as agentName, s.name as shipperName, p.name as PIName, f.name as frowaderName, dv.progress_date, dv.variance_date, pt.name as progressName, v.name as varianceName');
        $this->db->from('tbl_documents d');
         $this->db->join('tbl_shipper_list s', 'd.shipper_id=s.id', 'left');
         $this->db->join('tbl_billing_mst bm', 'd.id=bm.doc_id', 'left');
         $this->db->join('tbl_agent_list a', 'd.agent_id=a.id', 'left');
         $this->db->join('tbl_pi_list p', 'd.pi_id=p.id', 'left');
         $this->db->join('tbl_frowarder_list f', 'd.frowader_id=f.id', 'left');
        $this->db->join('tbl_documents_progress dv', 'd.document_progress_id_last=dv.id', 'left');
        $this->db->join('tbl_progress_type pt', 'd.progress_id=pt.id', 'left');
        $this->db->join('tbl_progress_variance_type v', 'd.variant_id=v.id', 'left');
        $this->db->where('bm.status IN (2)');
        $this->db->where('bm.payment_receive_status IN (0)');

        if ($id > 0)
        {
         $this->db->where('d.agent_id', $id);
        }

        $this->db->order_by('d.id', 'DESC');
        $query = $this->db->get();

        return $query->result();
    }



    /**
     * Does something interesting
     *
     * @param Place   where  Where something interesting takes place
     * @param integer repeat How many times something interesting should happen
     *
     * @throws Some_Exception_Class If something interesting cannot happen
     * @author Md. Khayrul Hasan <khayrul.web@gmail.com>
     * @return Status
     */

    public function receive_payment_receive_list($id) {
        $this->db->select('bm.status bill_status, bm.billing_id, bm.payment_receive_date_time, bm.id AS bill_id, bm.payment_receive_remarks,  d.id, d.document_no, d.document_date, d.shipment_type, d.shipment_type_no, d.shipper_id, d.description_of_goods, d.pi_id,d.remarks,d.shipper_remarks, d.agent_id, d.etd, d.eta, d.hawb_hbl,
		d.mawb_mbl, d.frowader_id, d.no_of_package, d.weight, d.value, d.currency_id, d.document_received_date, d.shipment_release_date, d.status, d.progress_id, d.variant_id, d.insertOn,
		d.insertOn, d.insertBy, d.updateBy, d.updateOn, d.updateByshiper, d.updateOnshipper, a.name as agentName, s.name as shipperName, p.name as PIName, f.name as frowaderName, dv.progress_date, dv.variance_date, pt.name as progressName, v.name as varianceName');
        $this->db->from('tbl_documents d');
         $this->db->join('tbl_shipper_list s', 'd.shipper_id=s.id', 'left');
         $this->db->join('tbl_billing_mst bm', 'd.id=bm.doc_id', 'left');
         $this->db->join('tbl_agent_list a', 'd.agent_id=a.id', 'left');
         $this->db->join('tbl_pi_list p', 'd.pi_id=p.id', 'left');
         $this->db->join('tbl_frowarder_list f', 'd.frowader_id=f.id', 'left');
        $this->db->join('tbl_documents_progress dv', 'd.document_progress_id_last=dv.id', 'left');
        $this->db->join('tbl_progress_type pt', 'd.progress_id=pt.id', 'left');
        $this->db->join('tbl_progress_variance_type v', 'd.variant_id=v.id', 'left');
        $this->db->where('bm.status IN (2)');
        $this->db->where('bm.payment_receive_status IN (1)');

        if ($id > 0)
        {
         $this->db->where('d.agent_id', $id);
        }

        $this->db->order_by('d.id', 'DESC');
        $query = $this->db->get();

        return $query->result();
    }




	function getProgressList($id)
    {
    $this->db->select('d.id, d.document_id, d.file_path, d.progress_date, d.progress_id, d.variance_date, d.variance_id, d.remarks, d.status,
		d.insertedBy, d.insertedOn, d.updatedBy, d.updatedOn, p.name as progress, v.name as variance');
    $this->db->from('tbl_documents_progress d');
		$this->db->join('tbl_progress_type p', 'd.progress_id=p.id', 'left');
		$this->db->join('tbl_progress_variance_type v', 'd.variance_id=v.id', 'left');
		$this->db->where('d.document_id', $id);

        $query = $this->db->get();

        return $query->result();
    }



	function getDayCount($id)
    {
    $this->db->select('d.document_received_date, d.document_received_time, d.updateOnshipper, d.update_consign_receive_on, d.consignment_receive_date_time');
    $this->db->from('tbl_documents d');
    $this->db->where('d.id', $id);
    $query = $this->db->get();
    return $query->row();
    }


	function shippingListing($agent_id)
    {
        $this->db->select('d.id, bm.doc_id, bm.status bill_status, bm.balance balance, d.document_no, d.document_date, d.shipment_type, d.shipper_id, d.description_of_goods, d.pi_id,d.remarks,d.shipper_remarks, d.agent_id, d.etd, d.eta, d.hawb_hbl,
        d.mawb_mbl, d.frowader_id, d.no_of_package, d.weight, d.value, d.currency_id, d.document_received_date, d.shipment_release_date, d.status, d.progress_id, d.variant_id,
        d.insertOn, d.insertBy, d.updateBy, d.updateOn, d.updateByshiper, d.updateOnshipper, a.name as agentName, s.name as shipperName, p.name as PIName, f.name as frowaderName, dv.progress_date, dv.variance_date, pt.name as progressName, v.name as varianceName');
        $this->db->from('tbl_documents d');
        $this->db->join('tbl_shipper_list s', 'd.shipper_id=s.id', 'left');
        $this->db->join('tbl_billing_mst bm', 'd.id=bm.doc_id', 'left');
        $this->db->join('tbl_agent_list a', 'd.agent_id=a.id', 'left');
        $this->db->join('tbl_pi_list p', 'd.pi_id=p.id', 'left');
        $this->db->join('tbl_frowarder_list f', 'd.frowader_id=f.id', 'left');
        $this->db->join('tbl_documents_progress dv', 'd.document_progress_id_last=dv.id', 'left');
        $this->db->join('tbl_progress_type pt', 'd.progress_id=pt.id', 'left');
        $this->db->join('tbl_progress_variance_type v', 'd.variant_id=v.id', 'left');
        $this->db->or_where('bm.status NOT IN (2)');
        $this->db->or_where('bm.status IS NULL');
        $this->db->where('d.consignment_receive_id != 0');

        if ($agent_id > 0)
        {
           $this->db->where('d.agent_id',$agent_id);
        }
        $query = $this->db->get();

        return $query->result();
    }

    // Old query ==================================
    // function consignmentListing($agent_id)
    // {
    //     $this->db->select('d.ids, bm.doc_id, bm.status bill_status, bm.balance balance, d.document_no, d.document_date, d.shipment_type, d.shipper_id, d.description_of_goods, d.pi_id,d.remarks,d.shipper_remarks, d.agent_id, d.etd, d.eta, d.hawb_hbl,
    //     d.mawb_mbl, d.frowader_id, d.no_of_package, d.weight, d.value, d.currency_id, d.document_received_date, d.shipment_release_date, d.status, d.progress_id, d.variant_id,
    //     d.insertOn, d.insertBy, d.updateBy, d.updateOn, d.updateByshiper, d.updateOnshipper, a.name as agentName, s.name as shipperName, p.name as PIName, f.name as frowaderName, dv.progress_date, dv.variance_date, pt.name as progressName, v.name as varianceName');
    //     $this->db->from('tbl_documents d');
    //     $this->db->join('tbl_shipper_list s', 'd.shipper_id=s.id', 'left');
    //     $this->db->join('tbl_billing_mst bm', 'd.id=bm.doc_id', 'left');
    //     $this->db->join('tbl_agent_list a', 'd.agent_id=a.id', 'left');
    //     $this->db->join('tbl_pi_list p', 'd.pi_id=p.id', 'left');
    //     $this->db->join('tbl_frowarder_list f', 'd.frowader_id=f.id', 'left');
    //     $this->db->join('tbl_documents_progress dv', 'd.document_progress_id_last=dv.id', 'left');
    //     $this->db->join('tbl_progress_type pt', 'd.progress_id=pt.id', 'left');
    //     $this->db->join('tbl_progress_variance_type v', 'd.variant_id=v.id', 'left');
    //     $this->db->or_where('bm.status NOT IN (2)');
    //     $this->db->or_where('bm.status IS NULL');
    //     //$this->db->where('d.consignment_receive_id = 0');

    //     if ($agent_id > 0)
    //     {
    //        $this->db->where('d.agent_id',$agent_id);
    //     }
    //     $query = $this->db->get();

    //     return $query->result();
    // }



    function consignmentListing($agent_id)
    {
        return $this->db->query("SELECT
    jk.*
FROM
    (
SELECT
    `d`.`id`,
    `bm`.`doc_id`,
    `bm`.`status` `bill_status`,
    `bm`.`balance` `balance`,
    `d`.`document_no`,
    `d`.`document_date`,
    `d`.`shipment_type`,
    `d`.`shipper_id`,
    `d`.`description_of_goods`,
    `d`.`pi_id`,
    (SELECT po.name FROM tbl_base_setup_chd po WHERE po.chd_id = d.remarks) remarks,
    `d`.`shipper_remarks`,
    `d`.`agent_id`,
    `d`.`etd`,
    `d`.`eta`,
    `d`.`hawb_hbl`,
    `d`.`mawb_mbl`,
    `d`.`frowader_id`,
    `d`.`no_of_package`,
    `d`.`weight`,
    `d`.`value`,
    `d`.`currency_id`,
    (SELECT crn.short_name FROM tbl_currency_type crn WHERE d.currency_id = crn.id) shortName,
    `d`.`document_received_date`,
    `d`.`shipment_release_date`,
    `d`.`status`,
    `d`.`progress_id`,
    `d`.`variant_id`,
    `d`.`insertOn`,
    `d`.`insertBy`,
    `d`.`updateBy`,
    `d`.`updateOn`,
    `d`.`updateByshiper`,
    `d`.`updateOnshipper`,
    `a`.`name` AS `agentName`,
    `s`.`name` AS `shipperName`,
    `p`.`name` AS `PIName`,
    `f`.`name` AS `frowaderName`,
    `dv`.`progress_date`,
    `dv`.`variance_date`,
    `pt`.`name` AS `progressName`,
    `v`.`name` AS `varianceName`
FROM
    `tbl_documents` `d`
    LEFT JOIN `tbl_shipper_list` `s` ON `d`.`shipper_id` = `s`.`id`
    LEFT JOIN `tbl_billing_mst` `bm` ON `d`.`id` = `bm`.`doc_id`
    LEFT JOIN `tbl_agent_list` `a` ON `d`.`agent_id` = `a`.`id`
    LEFT JOIN `tbl_pi_list` `p` ON `d`.`pi_id` = `p`.`id`
    LEFT JOIN `tbl_frowarder_list` `f` ON `d`.`frowader_id` = `f`.`id`
    LEFT JOIN `tbl_documents_progress` `dv` ON `d`.`document_progress_id_last` = `dv`.`id`
    LEFT JOIN `tbl_progress_type` `pt` ON `d`.`progress_id` = `pt`.`id`
    LEFT JOIN `tbl_progress_variance_type` `v` ON `d`.`variant_id` = `v`.`id`
WHERE
    `bm`.`status` NOT IN ( 2 ) AND
    `d`.`consignment_receive_id` IS NULL
    OR `bm`.`status` IS NULL
    ) jk
WHERE
    jk.agent_id = ?", array($agent_id))->result();
    }

   // Old query ==================================

  //   function billingListing($agent_id)
  //   {
  //       $this->db->select('d.id, bm.doc_id, bm.status bill_status, bm.balance balance, d.document_no, d.document_date, d.shipment_type, d.shipper_id, d.description_of_goods, d.pi_id,d.remarks,d.shipper_remarks, d.agent_id, d.etd, d.eta, d.hawb_hbl,
		// d.mawb_mbl, d.frowader_id, d.no_of_package, d.weight, d.value, d.currency_id, d.document_received_date, d.shipment_release_date, d.status, d.progress_id, d.variant_id,
		// d.insertOn, d.insertBy, d.updateBy, d.updateOn, d.updateByshiper, d.updateOnshipper, a.name as agentName, s.name as shipperName, p.name as PIName, f.name as frowaderName, dv.progress_date, dv.variance_date, pt.name as progressName, v.name as varianceName');
  //       $this->db->from('tbl_documents d');
  //       $this->db->join('tbl_shipper_list s', 'd.shipper_id=s.id', 'left');
		// $this->db->join('tbl_billing_mst bm', 'd.id=bm.doc_id', 'left');
		// $this->db->join('tbl_agent_list a', 'd.agent_id=a.id', 'left');
		// $this->db->join('tbl_pi_list p', 'd.pi_id=p.id', 'left');
		// $this->db->join('tbl_frowarder_list f', 'd.frowader_id=f.id', 'left');
		// $this->db->join('tbl_documents_progress dv', 'd.document_progress_id_last=dv.id', 'left');
		// $this->db->join('tbl_progress_type pt', 'd.progress_id=pt.id', 'left');
		// $this->db->join('tbl_progress_variance_type v', 'd.variant_id=v.id', 'left');
  //       $this->db->or_where('bm.status NOT IN (2)');
  //       $this->db->or_where('bm.status IS NULL');
  //       $this->db->where('d.consignment_receive_id != 0');

		// if ($agent_id > 0)
		// {
		//    $this->db->where('d.agent_id',$agent_id);
		// }
  //       $query = $this->db->get();

  //       return $query->result();
  //   }


    function billingListing($agent_id)
    {
       return $this->db->query("SELECT jk.* FROM (SELECT
    `d`.`id`,
    `bm`.`doc_id`,
    `bm`.`status` `bill_status`,
    `bm`.`balance` `balance`,
    `d`.`document_no`,
    `d`.`document_date`,
    `d`.`shipment_type`,
    `d`.`shipper_id`,
    `d`.`description_of_goods`,
    `d`.`pi_id`,
    `d`.`remarks`,
    `d`.`shipper_remarks`,
    `d`.`agent_id`,
    `d`.`etd`,
    `d`.`eta`,
    `d`.`hawb_hbl`,
    `d`.`mawb_mbl`,
    `d`.`frowader_id`,
    `d`.`no_of_package`,
    `d`.`weight`,
    `d`.`value`,
    `d`.`currency_id`,
    `d`.`document_received_date`,
    `d`.`shipment_release_date`,
    `d`.`status`,
    `d`.`progress_id`,
    `d`.`variant_id`,
    `d`.`insertOn`,
    `d`.`insertBy`,
    `d`.`updateBy`,
    `d`.`updateOn`,
    `d`.`updateByshiper`,
    `d`.`updateOnshipper`,
    `a`.`name` AS `agentName`,
    `s`.`name` AS `shipperName`,
    `p`.`name` AS `PIName`,
    `f`.`name` AS `frowaderName`,
    `dv`.`progress_date`,
    `dv`.`variance_date`,
    `pt`.`name` AS `progressName`,
    `v`.`name` AS `varianceName`
FROM
    `tbl_documents` `d`
    LEFT JOIN `tbl_shipper_list` `s` ON `d`.`shipper_id` = `s`.`id`
    LEFT JOIN `tbl_billing_mst` `bm` ON `d`.`id` = `bm`.`doc_id`
    LEFT JOIN `tbl_agent_list` `a` ON `d`.`agent_id` = `a`.`id`
    LEFT JOIN `tbl_pi_list` `p` ON `d`.`pi_id` = `p`.`id`
    LEFT JOIN `tbl_frowarder_list` `f` ON `d`.`frowader_id` = `f`.`id`
    LEFT JOIN `tbl_documents_progress` `dv` ON `d`.`document_progress_id_last` = `dv`.`id`
    LEFT JOIN `tbl_progress_type` `pt` ON `d`.`progress_id` = `pt`.`id`
    LEFT JOIN `tbl_progress_variance_type` `v` ON `d`.`variant_id` = `v`.`id`
WHERE
  `bm`.`status` NOT IN ( 2 )
   OR
  `bm`.`status` IS NULL
 AND `d`.`consignment_receive_id` != 0
 ) jk WHERE `jk`.`agent_id` = ?", array($agent_id))->result();
    }



    function addNewConsignee($UnitInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_documents', $UnitInfo);
        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $insert_id;
    }

	function addProgress($UnitInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_documents_progress', $UnitInfo);
        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $insert_id;
    }

	function getProgressInfo($id)
	{
		$this->db->select('id, document_id, progress_date, progress_id, variance_date, variance_id, remarks, status, insertedBy, insertedOn, updatedBy, updatedOn');
        $this->db->from('tbl_documents_progress');
		$this->db->where('id', $id);

        $query = $this->db->get();

        return $query->result();
	}

	function addProgressWithUpdate($upDateMaster,$document_id)
    {
        $this->db->trans_start();

		$this->db->where('id', $document_id);
        $this->db->update('tbl_documents', $upDateMaster);

        $this->db->trans_complete();

        return $this->db->insert_id();
    }




    function getConsigneeInfo($id)
    {
        $this->db->select('d.id, d.pi_id as PIName, d.budget_code, d.remarks AS remarks_id, (SELECT ar.name FROM tbl_base_setup_chd ar WHERE ar.chd_id = d.remarks) remarks, (SELECT ar.fld_air FROM tbl_base_setup_chd ar WHERE ar.chd_id = d.nature_of_shipment) AIR, (SELECT ar.fld_sea FROM tbl_base_setup_chd ar WHERE ar.chd_id = d.nature_of_shipment) SEA, (SELECT ar.fld_land FROM tbl_base_setup_chd ar WHERE ar.chd_id = d.nature_of_shipment) LAND, d.document_no, d.nature_of_shipment, d.document_date, d.shipment_type, d.shipment_type_no, d.shipment_mode, d.shipper_id, d.description_of_goods, d.pi_id, d.shipper_remarks, d.agent_id, d.etd, d.eta, d.hawb_hbl,
		d.mawb_mbl, d.frowader_id, d.no_of_package, d.weight, d.value, d.currency_id, d.document_received_date, d.document_received_time, d.shipment_release_date, d.status, d.progress_id, d.variant_id,
		d.insertOn, d.insertBy, d.updateBy, d.updateOn, d.updateByshiper, d.updateOnshipper, a.name as agentName, s.name as shipperName');
        $this->db->from('tbl_documents d');
		$this->db->join('tbl_shipper_list s', 'd.shipper_id=s.id', 'left');
		$this->db->join('tbl_agent_list a', 'd.agent_id=a.id', 'left');
        //$this->db->join('tbl_pi_list p', 'd.pi_id=p.id', 'left');
		//$this->db->join('tbl_base_setup_chd set', 'set.chd_id=d.remarks', 'left');
        $this->db->where('d.id', $id);
        $query = $this->db->get();

        return $query->result();
    }


    function editConsignee($IDInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_documents', $IDInfo);

        return TRUE;
    }

	function editConsigneeByShipper($IDInfo, $id)
    {
         $this->db->trans_start();

         $SQL = "INSERT INTO tbl_documents_log (document_id, document_no, document_date, shipment_type, shipper_id, description_of_goods, pi_id, agent_id, etd, eta, hawb_hbl, mawb_mbl, frowader_id, no_of_package, weight, value, currency_id, document_received_date, shipment_release_date, status, remarks, shipper_remarks, progress_id, variant_id, insertOn, insertBy, updateBy, updateOn, updateByshiper, updateOnshipper)
		 SELECT id, document_no, document_date, shipment_type, shipper_id, description_of_goods, pi_id, agent_id, etd, eta, hawb_hbl, mawb_mbl, frowader_id, no_of_package, weight, value, currency_id, document_received_date, shipment_release_date, status, remarks, shipper_remarks, progress_id, variant_id, insertOn, insertBy, updateBy, updateOn, updateByshiper, updateOnshipper FROM  tbl_documents where id=".$id;

		$query = $this->db->query($SQL);

        $this->db->where('id', $id);
        $this->db->update('tbl_documents', $IDInfo);

        $this->db->trans_complete();


        return TRUE;
    }

	function editProgressByShipper($IDInfo, $id)
    {
         $this->db->trans_start();

         $SQL = "INSERT INTO tbl_documents_progress_log (document_progress_id, document_id, progress_date, progress_id, variance_date, variance_id, remarks, status, insertedBy, insertedOn, updatedBy, updatedOn)
		 SELECT id, document_id, progress_date, progress_id, variance_date, variance_id, remarks, status, insertedBy, insertedOn, updatedBy, updatedOn FROM  tbl_documents_progress where id=".$id;

		$query = $this->db->query($SQL);

        $this->db->where('id', $id);
        $this->db->update('tbl_documents_progress', $IDInfo);

        $this->db->trans_complete();


        return TRUE;
    }



}
