<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fontend_model extends CI_Model
{

    
    public function selectCategoryTopThree()
    {
         $this->db->select('name, id');
         $this->db->from('tbl_category_list');
         $this->db->where('level', 0);
         $this->db->limit(2);
         $result = $this->db->get();

        return $result->result();
    }


    public function selectCategoryMore()
    {
         $this->db->select('name, id');
         $this->db->from('tbl_category_list');
         $this->db->where('level', 0);
         $this->db->limit(10, 2);
         $result = $this->db->get();
         return $result->result();
    }

    public function selectAllSubCategoryById($id)
    {
        $this->db->select('name, id');
        $this->db->from('tbl_category_list');
        $this->db->where('parent_id', $id);
        $this->db->where('level', 1);
        $result = $this->db->get();
        return $result->result();
    }


    public function selectAllProductById($id, $limit)
    {

        $result = [];
        $this->db->select('id');
        $this->db->from('tbl_category_list');
        $this->db->where('parent_id', $id);
        $this->db->where('level', 1);
        $result_id = $this->db->get()->result();

        // echo '<pre>';
        // print_r($result_id); exit;

        $offset = $this->pagination($limit);

        foreach($result_id as $id):

        $prod = $this->db->query("SELECT * FROM v_category_prod WHERE parent_id = $id->id ORDER BY id DESC LIMIT $offset, $limit")->result();   
        array_push($result, $prod);
        endforeach;
        
        return $result;

    }


    public function selectAllProdByCatId($id , $limit){

        $offset = $this->pagination($limit);
        $prod = $this->db->query("SELECT * FROM v_prod WHERE parent_id = $id ORDER BY id DESC LIMIT $offset, $limit")->result();   
        return $prod;

    }

    public function selectAllChoiceProduct($limit)
    {
        $offset = $this->pagination($limit);
        $prod = $this->db->query("SELECT * FROM v_prod ORDER BY id DESC LIMIT $offset, $limit")->result();   
        return $prod;
    }


    public function pagination($limit)
    {
        $offset = 0;
        if(isset($_GET['page_id'])):
            if($_GET['page_id'] == '' || $_GET['page_id'] == 1){
                $offset = 0;
            }else{
                $offset = ($_GET['page_id']*$limit) - $limit;
            }
        endif;
        return $offset;
    }


    public function selectDetailProduct($id)
    {
        return $this->db->query("SELECT bac.* FROM v_product_detail bac WHERE bac.id = '$id'")->row();
    }


    public function count_product($ip)
    {
        
        return $this->db->query("SELECT 
        SUM(b.quantity) quantity
        FROM
            tbl_cart_temp_mst a
                LEFT JOIN
            tbl_cart_temp_chd b ON a.id = b.user_uniq_id
        WHERE
            a.user_uniq_id = '$ip'")->row();
    }

    public function getAllCategoryWithImage()
    {
        return $this->db->query("SELECT
        m.id,
        m.parent_id,
        m.cat_name,
        n.path 
        FROM
            ( SELECT a.id, a.parent_id, a.NAME AS cat_name FROM tbl_category_list a WHERE a.LEVEL = 1 ) m
            LEFT JOIN v_category_prod n ON m.id = n.parent_id 
        LIMIT 4")->result();
    }


    public function getCartItemList($ip)
    {
        return $this->db->query("SELECT 
        k.*, j.*
        FROM
        (SELECT 
            tchd.prod_id, SUM(tchd.quantity) quantity
        FROM
            tbl_cart_temp_chd tchd
        LEFT JOIN tbl_cart_temp_mst tmst ON tmst.id = tchd.user_uniq_id
        WHERE
            tmst.user_uniq_id = '$ip'
        GROUP BY tchd.prod_id) k
            LEFT JOIN
        v_category_prod j ON k.prod_id = j.id")->result();
    }
    

 
}

  