<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Status_model extends CI_Model
{

    

    function getStatusType()
    {
        $this->db->select('id, name');
        $this->db->from('tbl_status_type');
        $this->db->where('active', 1);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function statusListing()
    {
        $this->db->select('id, name, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_status_type');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function addNewStatus($IdInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_status_type', $IdInfo);
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function getStatusInfo($id)
    {
        $this->db->select('id, name, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_status_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function editStatus($IDInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_status_type', $IDInfo);
        
        return TRUE;
    }
    
    

    function deleteStatus($id)
    {
		
        $this->db->where('id', $id);
        $this->db->delete('tbl_status_type');
        
        return TRUE;
    }


 
}

  