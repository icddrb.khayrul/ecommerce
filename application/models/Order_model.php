<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends CI_Model
{

    public function getAllOrder()
    {
        return $this->db->query("SELECT
        a.id, 
        a.total_price,
        a.delivery_charges,
        b.name AS customer_name,
        b.email,
        b.mobile,
        b.address
        FROM
            tbl_order_mst a
                LEFT JOIN
            tbl_customers b ON a.customer_id = b.customer_id")->result();
    }


    public function getOrderById($id){
        return $this->db->query("SELECT
        a.id, 
        a.total_price,
        a.delivery_charges,
        b.name AS customer_name,
        b.email,
        b.mobile,
        b.address
        FROM
            tbl_order_mst a
                LEFT JOIN
            tbl_customers b ON a.customer_id = b.customer_id WHERE a.id = $id")->result();
    }


    public function getOrderItemList($id)
    {
        return $this->db->query("SELECT 
        a.quantity, b.*
    FROM
        tbl_order_chd a
            LEFT JOIN
        v_category_prod b ON b.id = a.prod_id WHERE a.order_id = $id")->result();
    }
    

 
}

  