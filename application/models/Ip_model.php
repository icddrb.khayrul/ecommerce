<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ip_model extends CI_Model
{

    
        function get_user_uniq_id()
        {
            if(!isset($_COOKIE['user_id'])) {
                $strNumber = '';
                $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                 .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                 .'0123456789!@#$%^&*()'); // and any other characters
                shuffle($seed); // probably optional since array_is randomized; this may be redundant
                $rand = '';
                foreach (array_rand($seed, 35) as $k) $strNumber .= $seed[$k];
                
                setcookie('user_id', $strNumber, time() + 3600, "/"); // 86400 = 1 day
                
                $_COOKIE['user_id'] = $strNumber;
            } 

            return $_COOKIE['user_id'];

           

        }



        function insertData($post, $tableName) {
            $this->db->trans_start();
            $this->db->insert($tableName, $post);
            $this->db->trans_complete();
            if ($this->db->trans_status() == TRUE) {
                return TRUE;
            } else {
                return FALSE;
            }
        }


        public function identify_to_user(){
            $user = $this->ip_model->get_user_uniq_id();
            $userData = array(
                    'user_uniq_id' => (isset($user)) ? $user : ''
                    );
            $exits = $this->ip_model->findByAttribute('tbl_cart_temp_mst', array('user_uniq_id'=>$user));
            if(!isset($exits)){
               $this->ip_model->insertData($userData,'tbl_cart_temp_mst');
            } 

            return $user;
        }


        function findByAttribute($tableName, $attribute) {
            return $this->db->get_where($tableName, $attribute)->row();
        }



        
        
        
    
        public function pr($data)
        {
    
                echo '<pre>';
                print_r($data);
                exit;
        }
    

 
}

  