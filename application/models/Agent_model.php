<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Agent_model extends CI_Model
{

    function getAgent()
    {
        $this->db->select('id, name, registration_no, address, mobile, phone, email, remarks, active');
        $this->db->from('tbl_agent_list');
        $this->db->where('active', 1);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function agentListing()
    {
        $this->db->select('id, name, registration_no, address, mobile, phone, email, remarks, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_agent_list');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    function addNewAgent($UnitInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_agent_list', $UnitInfo);
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function getAgentInfo($id)
    {
        $this->db->select('id, name, registration_no, address, mobile, phone, email, remarks, active, insertedOn, insertedBy, updatedOn, updatedBy');
        $this->db->from('tbl_agent_list');
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }


    function getCatInfo($id)
    {
        $this->db->select('id, name, active');
        $this->db->from('tbl_category_list');
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->row();
    }

    function getDeleteList($id)
    {
        return $this->db->query("SELECT id
    FROM tbl_category_list
    WHERE parent_id = $id 
    UNION SELECT id
    FROM tbl_category_list WHERE parent_id IN (SELECT id FROM tbl_category_list WHERE parent_id = $id)")->result();

    }
    
    
    function editAgent($IDInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_agent_list', $IDInfo);
        
        return TRUE;
    }
    

 
}

  