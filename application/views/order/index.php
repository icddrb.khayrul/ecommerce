<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
	  
	   <div class="row">
	     <div class="col-xs-6 text-left header-margin">
	        <h3> 
			   Order Management
			</h3>
	     </div>
            <div class="col-xs-6 text-right">
                <div class="form-group">
                    <a class="" href="">&nbsp;</a>
                </div>
            </div>
        </div>
		
    </section>
    <section class="content content-margin">
       
        <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Order List</h3>
					<?php
							$this->load->helper('form');
							$error = $this->session->flashdata('error');
							if($error)
							{
						?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<?php echo $this->session->flashdata('error'); ?>                    
						</div>
						<?php } ?>
						<?php  
							$success = $this->session->flashdata('success');
							if($success)
							{
						?>
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } ?>
				
						
						<div class="row">
							<div class="col-md-12">
								<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
							</div>
						</div>
					
                </div><!-- /.box-header -->
                <div class="box-body">
					<table id="UnitList" class="table table-bordered table-striped">
						<thead>
						<tr>
						  <th>#</th>
						  <th>Customer Name</th>
						  <th>Mobile #</th>
						  <th>Email Address</th>
						  <th>Total Price</th>
						  <th>Delivery Charges</th>
						  <th style="width:150px">Address</th>
						  <th style="width:90px">Action</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if(!empty($result))
						{
							foreach($result as $key=> $record)
							{
						?>
						<tr>
						  <td><?php echo $key+1 ?></td>
						  <td><?php echo $record->customer_name; ?></td>
						  <td><?php echo $record->mobile; ?></td>
						  <td><?php echo $record->email; ?></td>
						  <td>TK <?php echo number_format($record->total_price,2); ?></td>
						  <td>TK <?php echo number_format($record->delivery_charges, 2); ?></td>
						  <td><?php echo $record->address; ?></td>
						  
						  
						  <td>
							  <a class="btn-sm btn-primary" href="<?php echo base_url().'order/view_order/'.$record->id. '?baseID='.$baseID ?>"><i class="fa fa-eye"></i>&nbsp;</a>

							  <a class="btn-sm btn-success" href="<?php echo base_url().'order/accept_order/'.$record->id. '?baseID='.$baseID ?>"><i class="fa fa-check-square"></i>&nbsp;</a>

							  <a class="btn-sm btn-danger" href="<?php echo base_url().'order/reject_order/'.$record->id. '?baseID='.$baseID ?>"><i class="fa fa-trash"></i>&nbsp;</a>
							
						  </td>
						</tr>
						<?php
							}
						}
						?>
						</tbody>
					  </table>
                </div>  
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script>
  $(function () {
        $("#UnitList").DataTable({
        "scrollX": true,
            buttons: [{
                    extend: 'pdf',
                    title: 'Menu List',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },{
                    extend: 'excel',
                    title: 'Menu List',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                }, {
                    extend: 'csv',
                    title: 'Menu List',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                }, {
                    extend: 'print',
                    title: 'Menu List',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                }
            ]
    });
      });
  </script>                    
