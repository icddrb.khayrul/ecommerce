<?php
$name = '';
$mobile = '';
$address = '';
$email = '';

foreach($customer as $val):

	$name = $val->customer_name;
	$mobile = $val->mobile;
	$address = $val->address;
	$email = $val->email;
endforeach;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <div class="row">
		     <div class="col-xs-6 text-left header-margin ">
                   <h3>
					<?php echo $pageTitle ?>
					<?php $baseID = $this->input->get('baseID',TRUE); ?>
				  </h3>
               
            </div>
            <div class="col-xs-6 text-right">
                <div class="form-group">
				<a class="btn-sm btn-primary" href="<?php echo base_url();?>order?baseID=<?php echo $baseID?>">Back List</a>
                </div>
            </div>
        </div>
    </section>
    
    <section class="content content-margin">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $boxTitle ?></h3>
                    </div>
						
						
                        <div class="box-body">
							<h4>Customer Info</h4>
							<hr/>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="Active">Customer Name</label>
                                        <div><span><?php echo $name; ?></span></div>
									</div>
                            	</div> 

								<div class="col-md-2">
                                    <div class="form-group">
                                        <label for="Active">Mobile Number</label>
                                        <div><span><?php echo $mobile; ?></span></div>
									</div>
                            	</div>


								<div class="col-md-2">
                                    <div class="form-group">
                                        <label for="Active">Email Address</label>
                                        <div><span><?php echo $email; ?></span></div>
									</div>
                            	</div>


								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Active">Address</label>
                                        <div><span><?php echo $address; ?></span></div>
									</div>
                            	</div>

							</div>

							<h4>Order Info</h4>
							<hr/>
							<div class="row">
							   <table class="table">
									<tbody>
										<?php 
										$subtotal = 0;
										foreach($cart_item_list as $key=> $value):
												$subtotal += $value->quantity*$value->base_price; 
										?>
										<tr>
										<td class="pull-left" style="padding-left:15px">
											<a href=""><img src="<?php echo base_url(); ?>assets/images/upload/thrum/<?php echo $value->path; ?>" width="50" height="50" alt="NEW Microsoft Surface Go"></a>
										</td>
										<td>
											<h4><?php echo $value->prod_name?></h4>
											<h6 class="text-muted">TK <?php echo number_format($value->base_price,2)?></h6>
											
											<span class="badge badge-success font-weight-light"><?php echo $value->color; ?></span>
											<span class="badge badge-dark font-weight-light"><?php echo $value->size?></span>
											
										</td>
										<td class="pull-left">
											<div class="input-spinner">
												<span><?php echo $value->quantity?></span>
											</div>
											<span class="price">TK <?php echo number_format($value->base_price*$value->quantity,2); ?></span>
										</td>
										</tr>
										<?php endforeach; ?>
										<tr>
											<td colspan="2"> &nbsp;</td>
											<td colspan="1">
												<div class="box-total">
												<h4><span class="price">TK <?php echo number_format($subtotal,2); ?></span></h4>
											</div>
											</td>
										</tr>
										
										</tbody>
									</table>
							</div>
							
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Accept" />
                            <input type="reset" class="btn btn-default" value="Reject" />
                        </div>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                   
                </div>
            </div>
            
        </div>    
    </section>
    
</div>
