
<?php

$success = 'class="success-row"';
$info = 'class="info-row"';

$total = 0;
$pending_check_in = 0;
$total_check_in = 0;
$pending_check_out = 0;
$total_check_out = 0;

$pi_name = "";

if(!empty($dashboard))
{
    foreach ($dashboard as $df)
    {
		$total = $df->total;
		$pending_check_in = $df->pending_check_in;
		$total_check_in = $df->total_check_in;
		$pending_check_out = $df->pending_check_out;
		$total_check_out = $df->total_check_out;




    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" >
      <h1>
        Dashboard
        <small></small>

      </h1>

    </section>


    <section class="content">
    	<caption><h4>Document Info</h4></caption>
    <div class="row">
    	<?php if(!empty($newDocument)){?>
	            <div class="box-header  col-md-12">
	            	<div class="box box-primary">
	            	<table class="table">
	            		<caption>New Document</caption>
					    <thead>
					      <tr>
					        <th>Document</th>
					        <th>PI & FPI</th>
					        <th>Agent Name</th>
					        <th>Shipper Name</th>
					        <th>Date</th>
					      </tr>
					    </thead>
					    <tbody>
					   	<?php foreach ($newDocument as $key => $value) {
					   		# code...
					   	?>
					      <tr <?php if(($key % 2) == 0) {echo $success;}else{ echo $info;} ?>>
					        <td><?php if(isset($value->document_no)) echo $value->document_no; ?></td>
					        <td><?php if(isset($value->pi_name)) echo $value->pi_name; ?></td>
					        <td><?php if(isset($value->agent_name)) echo $value->agent_name; ?></td>
					        <td><?php if(isset($value->shipper_name)) echo $value->shipper_name; ?></td>
					        <td><?php if(isset($value->document_date)) echo date("d/m/Y", strtotime($value->document_date)); ?></td>

					      </tr>
					    <?php } ?>
					    </tbody>
					</table>
				</div>
		    	</div>
		   <?php } ?>

</div>

<div class="row">

		   <?php if(!empty($documentComlete)){?>
		            <div class="box-header  col-md-12">
		            	<div class="box box-primary">
		            	<table class="table">
		            		<caption>Shipment Released</caption>
						    <thead>
						      <tr>
						        <th>Document No</th>
						        <th>PI & FPI</th>
						        <th>Agent Name</th>
						        <th>Shipper Name</th>
						        <th>Progress Name</th>
						        <th>Progress Date</th>
						      </tr>
						    </thead>
						    <tbody>
						   	<?php foreach ($documentComlete as $key => $value) {
						   		# code...
						   	?>
						      <tr <?php if(($key % 2) == 0) {echo $success;}else{ echo $info;} ?>>
						         <td><?php if(isset($value->document_no)) echo $value->document_no; ?></td>
						         <td><?php if(isset($value->pi_name)) echo $value->pi_name; ?></td>
						         <td><?php if(isset($value->agent_name)) echo $value->agent_name; ?></td>
						         <td><?php if(isset($value->shipper_name)) echo $value->shipper_name; ?></td>
						         <td><?php if(isset($value->pro_name)) echo $value->pro_name; ?></td>
					        	 <td><?php echo date("d/m/Y", strtotime($value->document_date)); ?></td>

						      </tr>
						    <?php } ?>
						    </tbody>
						  </table>
						</div>
			    	</div>
		   <?php } ?>
</div>


<div class="row">
	<?php if(!empty($closeDocument)){?>
	                <div class="box-header  col-md-12">
	                	<div class="box box-primary">
	                	<table class="table">
	            		<caption>Closed Document</caption>
					    <thead>
					      <tr>
					        <th>Document</th>
					        <th>PI & FPI</th>
					        <th>Agent Name</th>
					        <th>Shipper Name</th>
					        <th>Date</th>
					      </tr>
					    </thead>
					    <tbody>
					   	<?php  foreach ($closeDocument as $key => $value) { ?>
					      <tr <?php if(($key % 2) == 0) {echo $success;}else{ echo $info;} ?>>
					        <td><?php if(isset($value->document_no)) echo $value->document_no; ?></td>
					        <td><?php if(isset($value->pi_name)) echo $value->pi_name; ?></td>
					        <td><?php if(isset($value->agent_name)) echo $value->agent_name; ?></td>
					        <td><?php if(isset($value->shipper_name)) echo $value->shipper_name; ?></td>
					        <td><?php if(isset($value->document_date)) echo date("d/m/Y", strtotime($value->document_date)); ?></td>
					      </tr>
					    <?php } ?>
					    </tbody>
					  </table>
					</div>
		    		</div>
		<?php }  ?>
</div>

<div class="row">
	<?php if(!empty($proDocument)){?>
		            <div class="box-header  col-md-12">
		            	<div class="box box-primary">
		            	<table class="table">
		            		<caption>Document in Progress</caption>
						    <thead>
						      <tr>
						        <th>Document</th>
						        <th>PI & FPI</th>
						        <th>Agent Name</th>
						        <th>Shipper Name</th>
						        <th>Progress Name</th>
						        <th>Progress Date</th>
						      </tr>
						    </thead>
						    <tbody>
						   	<?php foreach ($proDocument as $key => $value) {
						   		# code...
						   	?>
						      <tr <?php if(($key % 2) == 0) {echo $success;}else{ echo $info;} ?>>
						        <td><?php if(isset($value->document_no)) echo $value->document_no; ?></td>
						        <td><?php if(isset($value->pi_name)) echo $value->pi_name; ?></td>
						        <td><?php if(isset($value->agent_name)) echo $value->agent_name; ?></td>
						        <td><?php if(isset($value->shipper_name)) echo $value->shipper_name; ?></td>
						        <td><?php if(isset($value->progress)) echo (!empty($value->progress))?$value->progress:'Document Arrived'; ?></td>
						        <td><?php if(isset($value->document_date)) echo date("d/m/Y", strtotime($value->document_date)); ?></td>

						      </tr>
						    <?php } ?>
						    </tbody>
						  </table>
						</div>
			    	</div>
		   <?php } ?>
</div>
<caption><h4>Billing Info</h4></caption>

<div class="row">
		<?php if(!empty($pendingBilling)){?>
	        <div class="box-header  col-md-12">
				<div class="box box-primary">
	            	<table class="table">
	            		<caption>Pending Bill Create</caption>
					    <thead>
					      <tr>
					        <th>Document</th>
					        <th>PI & FPI</th>
					        <th>Agent Name</th>
					        <th>Shipper Name</th>
					        <th>Date</th>
					      </tr>
					    </thead>
					    <tbody>
					   	<?php foreach ($pendingBilling as $key => $value) {
					   		# code...
					   	?>
					      <tr <?php if(($key % 2) == 0) {echo $success;}else{ echo $info;} ?>>
					        <td><?php if(isset($value->document_no)) echo $value->document_no; ?></td>
					        <td><?php if(isset($value->pi_name)) echo $value->pi_name; ?></td>
					        <td><?php if(isset($value->agent_name)) echo $value->agent_name; ?></td>
					        <td><?php if(isset($value->shipper_name)) echo $value->shipper_name; ?></td>
					        <td><?php if(isset($value->document_no)) echo date("d/m/Y", strtotime($value->document_date)); ?></td>

					      </tr>
					    <?php } ?>
					    </tbody>
					</table>
		    	</div>
		    </div>
		   <?php } ?>
</div>

<div class="row">
	<?php if(!empty($pendingIcddrbBilling)){?>
		            <div class="box-header  col-md-12">
		            	<div class="box box-primary">
		            	<table class="table">
		            		<caption>Bill Pending at ecommerce</caption>
						    <thead>
						      <tr>
						        <th>Document</th>
						        <th>PI & FPI</th>
						        <th>Agent name</th>
						        <th>Shipper name</th>
						        <th>Date</th>
						      </tr>
						    </thead>
						    <tbody>
						   	<?php foreach ($pendingIcddrbBilling as $key => $value) {
						   		# code...
						   	?>
						      <tr <?php if(($key % 2) == 0) {echo $success;}else{ echo $info;} ?>>
						        <td><?php if(isset($value->document_no)) echo $value->document_no; ?></td>
						        <td><?php if(isset($value->pi_name)) echo $value->pi_name; ?></td>
						        <td><?php if(isset($value->agent_name)) echo $value->agent_name; ?></td>
						        <td><?php if(isset($value->shipper_name)) echo $value->shipper_name; ?></td>
					       		<td><?php if(isset($value->document_no)) echo date("d/m/Y", strtotime($value->document_date)); ?></td>

						      </tr>
						    <?php } ?>
						    </tbody>
						  </table>
						</div>
			    	</div>
		   <?php } ?>
</div>


<div class="row">
	<?php if(!empty($rejectBilling)){?>
	                <div class="box-header  col-md-12">
	                	<div class="box box-primary">
	                	<table class="table">
	            		<caption>Bill Reject</caption>
					    <thead>
					      <tr>
					        <th>Document</th>
					        <th>PI & FPI</th>
					        <th>Agent name</th>
					        <th>Shipper name</th>
					        <th>Date</th>
					      </tr>
					    </thead>
					    <tbody>
					   	<?php  foreach ($rejectBilling as $key => $value) { ?>
					      <tr <?php if(($key % 2) == 0) {echo $success;}else{ echo $info;} ?>>
					        <td><?php if(isset($value->document_no)) echo $value->document_no; ?></td>
					        <td><?php if(isset($value->pi_name)) echo $value->pi_name; ?></td>
					        <td><?php if(isset($value->agent_name)) echo $value->agent_name; ?></td>
					        <td><?php if(isset($value->shipper_name)) echo $value->shipper_name; ?></td>
					       	<td><?php if(isset($value->document_date)) echo date("d/m/Y", strtotime($value->document_date)); ?></td>
					      </tr>
					    <?php } ?>
					    </tbody>
					  </table>
					</div>
		    		</div>
		<?php }  ?>
</div>


<div class="row">
		<?php if(!empty($approvedBilling)){?>
	                <div class="box-header  col-md-12">
	                	<div class="box box-primary">
	                	<table class="table">
	            		<caption>Bill Approved</caption>
					    <thead>
					      <tr>
					        <th>Document</th>
					        <th>PI & FPI</th>
					        <th>Agent name</th>
					        <th>Shipper name</th>
					        <th>Date</th>
					      </tr>
					    </thead>
					    <tbody>
					   	<?php  foreach ($approvedBilling as $key => $value) { ?>
					      <tr <?php if(($key % 2) == 0) {echo $success;}else{ echo $info;} ?>>
					        <td><?php if(isset($value->document_no)) echo $value->document_no; ?></td>
					        <td><?php if(isset($value->pi_name)) echo $value->pi_name; ?></td>
					        <td><?php if(isset($value->agent_name)) echo $value->agent_name; ?></td>
					        <td><?php if(isset($value->shipper_name)) echo $value->shipper_name; ?></td>
					       	<td><?php if(isset($value->document_date)) echo date("d/m/Y", strtotime($value->document_date)); ?></td>
					      </tr>
					    <?php } ?>
					    </tbody>
					  </table>
					</div>
		    		</div>
		<?php }  ?>

</div>

    </section>
</div>