

    <footer class="main-footer">
        <div class="pull-right">
          Develop & Maintain by IT, <a href="http://localhost/ecommerce/">ecommerce</a>.
         <!-- <b>Developed</b> by | ecommerce  -->
        </div>
        <div class="hidden-xs">
          <strong>Copyright &copy; <?php echo DATE('Y') ?> <a class="no-print" href="<?php echo base_url(); ?>">ecommerce</a>.</strong> All rights reserved.
        </div>
    </footer>

    <!-- jQuery UI 1.11.2 -->
    <!-- <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script> -->
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 3.3.2 JS -->

    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/validation.js" type="text/javascript"></script>


    <script src="<?php echo base_url(); ?>assets/js/num-to-words.js" type="text/javascript"></script>


    <!-- DataTables -->
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>




    <script src="<?php echo base_url(); ?>assets/datatables/v1.10.16/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/v1.10.16/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/jszip/v3.1.4/dist/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/pdfmake/v0.1.33/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/pdfmake/v0.1.33/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/v1.10.16/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/v1.10.16/extensions/Buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/v1.10.16/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/v1.10.16/extensions/FixedHeader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/v1.10.16/extensions/FixedColumns/js/dataTables.fixedColumns.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/v1.10.16/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/v1.10.16/plugins/features/searchHighlight/dataTables.searchHighlight.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/v1.10.16/plugins/pagination/select.js"></script>




    <script src="<?php echo base_url(); ?>assets/sweetalert/v1.1.0/dist/sweetalert.min.js" type="text/javascript"></script>


    <script>

        var windowURL = window.location.href;
        pageURL = windowURL.substring(0, windowURL.lastIndexOf('/'));
        var x= $('a[href="'+pageURL+'"]');
            x.addClass('active');
            x.parent().addClass('active');
        var y= $('a[href="'+windowURL+'"]');
            y.addClass('active');
            y.parent().addClass('active');


       // Prevent string
       $('body').on('keyup', '.numericOnly', function () {
            var val = $(this).val();
            $(this).val(val.replace(/[^\d]/g, ''));
        });

       $('body').on('keyup', '.numericPoint', function () {
            var val = $(this).val();
            $(this).val(val.replace(/[^0-9\.]/g,""));

        });


       //    for dynamic modal
        $('.dynamicFormModal').on('click',function(){
            var urlAction =  $(this).attr('data-action');
            var modalTitle = $(this).attr('modal-head');
            var headerCon = '<div class="modal fade" id="myModalAddItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content" style="margin: 0px; padding: 0px"><div class="modal-header" style="background: #345D78; color: white "><h4 class="modal-title">Modal Heading</h4></div><div class="modal-body">';
            
            var footerCon = '</div><div class="modal-footer"></div></div></div></div></div>';
            //alert(urlAction);
            
            $.ajax({
                type : 'POST',
                dataType : 'HTML',
                url : urlAction, 
                success : function(data){
                    $('#modal_target').html(headerCon+data+footerCon);
                    $('#myModalAddItem').modal();
                    $('.modal-title').html(modalTitle);
                }
               
            });
           
        });

    </script>
  </body>
</html>