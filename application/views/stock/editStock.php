<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <div class="row">
		     <div class="col-xs-6 text-left header-margin ">
                   <h3>
					<?php echo $pageTitle ?>
					<small>(Add, Edit)</small>
					<?php $baseID = $this->input->get('baseID',TRUE); ?>
				  </h3>
               
            </div>
            <div class="col-xs-6 text-right">
                <div class="form-group">
                     <a class="btn btn-primary" href="<?php echo base_url().$controller.'?baseID='.$baseID ?>"><?php echo $shortName ?> List</a>
                </div>
            </div>
        </div>
    </section>
    
    <section class="content content-margin">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $boxTitle ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
						<?php
							$this->load->helper('form');
							$error = $this->session->flashdata('error');
							if($error)
							{
						?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<?php echo $this->session->flashdata('error'); ?>                    
						</div>
						<?php } ?>
						<?php  
							$success = $this->session->flashdata('success');
							if($success)
							{
						?>
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } ?>
						
						<div class="row">
							<div class="col-md-12">
								<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
							</div>
						</div>
            
                    <form role="form" id="addUser" action="<?php echo base_url().$controller.'/'.$action.'?baseID='.$baseID?>" method="post" role="form">
                        <div class="box-body">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Item Name">Batch</label>

                                        <input type="hidden" value="<?php echo $selectAll->id;?>" name="existId" />
                                        <select class="form-control required" name="batch_id" id="product">
                                        
                                        
                                        <?php foreach($batch as $value): ?>
                                        <option <?php if($selectAll->batch_id == $value->id){echo 'selected=selected';} ?> value="<?php echo $value->id;?>"><?php echo $value->batch_id;?></option>
                                        <?php endforeach; ?>
                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Item Name">Quantity</label>
                                        <input type="text" class="form-control required numericPoint" id="quantity" value="<?php echo $selectAll->quantity; ?>"  name="quantity" maxlength="255" required="required">
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Item Name">Quantity Less</label>
                                        <input value="<?php echo $selectAll->qnt_less; ?>" type="text" class="form-control required numericPoint inputAllDamage" id="qnt_less"  name="qnt_less" maxlength="255" >
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Item Name">Quantity Damage</label>
                                        <input value="<?php echo $selectAll->qnt_damage; ?>" type="text" class="form-control required numericPoint inputAllDamage" id="qnt_damage"  name="qnt_damage" maxlength="255" >
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Item Name">Quantity Return</label>
                                        <input value="<?php echo $selectAll->qnt_return; ?>" type="text" class="form-control numericPoint inputAllDamage" id="qnt_return"  name="qnt_return" maxlength="255">
                                    </div>
                                </div>



                                <div class="col-md-2">
                                    <div class="form-group">
                                            <label for="Active">Active</label>
                                            <select class="form-control required" id="role" name="active">
                                                <option value="1" <?php if($selectAll->active==1) {echo "selected=selected" ;}?>>Yes</option>
                                                <option value="0" <?php if($selectAll->active==0) {echo "selected=selected" ;}?> >No</option>

                                            </select>
                                        </div>
                                </div> 


							</div>
							
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Save" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            
        </div>    
    </section>
    
</div>


<script>
// function name check exit value
$(function () {
	$('.inputAllDamage').keyup(function () {
        var _valueInput = Number($('#qnt_less').val()) + Number($('#qnt_damage').val()) + Number($('#qnt_return').val());
        var _quantity = Number($('#quantity').val());
		
        if (_quantity < _valueInput) {
			$(this).val(0);
		}
	});
})

</script>
