<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <div class="row">
		     <div class="col-xs-6 text-left header-margin ">
                   <h3>
					<?php echo $pageTitle ?>
					<small>(Add, Edit)</small>
					<?php $baseID = $this->input->get('baseID',TRUE); ?>
				  </h3>
               
            </div>
            <div class="col-xs-6 text-right">
                <div class="form-group">
                     <a class="btn btn-primary" href="<?php echo base_url().$controller.'?baseID='.$baseID ?>"><?php echo $shortName ?> List</a>
                </div>
            </div>
        </div>
    </section>
    
    <section class="content content-margin">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $boxTitle ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
						<?php
							$this->load->helper('form');
							$error = $this->session->flashdata('error');
							if($error)
							{
						?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<?php echo $this->session->flashdata('error'); ?>                    
						</div>
						<?php } ?>
						<?php  
							$success = $this->session->flashdata('success');
							if($success)
							{
						?>
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } ?>
						
						<div class="row">
							<div class="col-md-12">
								<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
							</div>
						</div>
            
                    <form role="form" id="addUser" enctype="multipart/form-data" action="<?php echo base_url().$controller.'/'.$action.'?baseID='.$baseID?>" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="Item Name">Category</label>
                                        <?php
                                        echo form_dropdown('category', $category, $selectAll->cat_id, 'id="cat" class="form-control"');
                                        
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="Item Name">Color </label>
                                        <?php 
                                        echo form_dropdown('color', $color, $selectAll->color_id, 'id="color" class="form-control"');
                                        
                                        ?>
                                    </div>
                                </div>
 
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="Item Name">Size</label>
                                        <?php
                                        echo form_dropdown('size', $size, $selectAll->size_id, 'id="size" class="form-control"');
                                        
                                        ?>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="Active">Image Upload</label>
                                        <input type="hidden" name="existId" value="<?php echo $selectAll->id; ?>" />
                                        <input type="hidden" name="existImage" value="<?php echo $selectAll->path; ?>" />
                                        <input type="file" name="fileUpload" class="form-control" />
                                    </div>
                                </div> 


                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="Active">Active</label>
                                        <select class="form-control required" id="role" name="active">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                            
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="Active">Image</label>
                                        <img src="<?php echo base_url()?>assets/images/upload/thrum/<?php echo $selectAll->path; ?>" height="70px" width="70px" />
                                    </div>
                                </div> 

                            </div>
							
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Save" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                    </form>
                </div>
            </div>
            
        </div>    
    </section>
    
</div>
