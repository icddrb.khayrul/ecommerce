<div class="content-wrapper">
   
    <section class="content-header">
      <div class="row">
		     <div class="col-xs-6 text-left header-margin ">
                   <h3>
					<?php echo $pageTitle ?>
					<small>(Add, Edit)</small>
					<?php $baseID = $this->input->get('baseID',TRUE); ?>
				  </h3>
               
            </div>
            <div class="col-xs-6 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url().$controller.'/addNew?baseID='.$baseID ?>">Add New</a>
                </div>
            </div>
        </div>
    </section>


    <section class="content content-margin">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            <?php echo $shortName ?>
                            <?php echo $boxTitle ?>
                        </h3>

                        <?php
                        $this->load->helper('form');
                        $error = $this->session->flashdata('error');
                        if($error)
                        {
                    ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                        <?php } ?>
                        <?php
                        $success = $this->session->flashdata('success');
                        if($success)
                        {
                    ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                        <?php } ?>

                        <div class="row">
                            <div class="col-md-12">
                                <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                        <table id="UnitList" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Batch Number</th>
                                    <th>Category Name</th>
                                    <th>Base Price</th>
                                    <th>Selling Price</th>
                                    <th>Discount Price</th>
                                    <th>Active</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if(!empty($result))
                            {
                                foreach($result as $record)
                                {
                            ?>
                                    <tr>
                                        <td>
                                            <?php echo $record->batch_id ?>
                                        </td>
                                        <td>
                                            <?php echo $record->category_name ?>
                                        </td>
                                        <td>
                                            <?php echo $record->base_price ?>
                                        </td>
                                        <td>
                                            <?php echo $record->selling_price ?>
                                        </td>
                                        <td>
                                            <?php echo $record->discount_price ?>
                                        </td>
                                        <td>
                                            <?php if ($record->active == 1 ) { echo "Yes"; } else { echo "No"; } ?>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" href="<?php echo base_url().$controller.'/editOld/'.$record->id . '?baseID='.$baseID ?>"><i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;</a>

                                            <a table="tbl_batch_list" pk_row="id" pk_id="<?php echo $record->id; ?>" class="btn btn-sm btn-danger deleteRow" ><i class="fa fa-trash"></i>&nbsp;&nbsp;&nbsp;</a>

                                        </td>
                                    </tr>
                                    
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>




 <script>
  $(function () {
    $("#UnitList").DataTable();
  });



        //delete function
        $(document).on("click", ".deleteRow", function (e) {
            var self = $(this);
            var table = self.attr('table');
            var pk_row = self.attr('pk_id');
            var pk_id = self.attr('pk_row');

            
                //confirm('Are you sure ?');
                
                
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url()?>base_setup/deleteRecourt?baseID=<?php $baseID; ?>",
                    data: {
                        table: table,
                        pk_row: pk_row,
                        pk_id: pk_id
                    },
                    success: function (data) {
                        
                    }
                });
            
            
        });

  	
 


</script>                             
