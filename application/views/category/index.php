<div class="content-wrapper">
   
    <section class="content-header">
      <div class="row">
		     <div class="col-xs-6 text-left header-margin ">
                   <h3>
					<?php echo $pageTitle ?>
					<small>(Add, Edit)</small>
					<?php $baseID = $this->input->get('baseID',TRUE); ?>
				  </h3>
               
            </div>
            <div class="col-xs-6 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url().$controller.'/addNew?baseID='.$baseID ?>">Add New</a>
                </div>
            </div>
        </div>
    </section>
    <section class="content content-margin">
        
        <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $shortName ?> <?php echo $boxTitle ?></h3>
					
					<?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
                <?php
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                        <div id="treeview"></div>      
                
                        <?php
                       foreach($result as $row){

                            $addhref = ($row['level'] < 2)?'<a href="'.$this->controller.'/'.$row["url"].'/'.$row["id"].'?baseID='.$baseID.'" class="btn btn-info pull-right">Add</a>':'';

                            $edithref = '<a href="'.$this->controller.'/editcat/'.$row["id"].'?baseID='.$baseID.'" class="btn btn-warning pull-right">Edit</a>';


                            $deletehref = '<a href="'.$this->controller.'/deletecat/'.$row["id"].'?baseID='.$baseID.'" class="btn btn-danger pull-right">Delete</a>';

                            $sub_data["id"] = $row["id"];
                            $sub_data["name"] = $row["name"];
                            $sub_data["text"] = $row["name"]. $deletehref .'&nbsp;&nbsp; ' . $addhref . ' &nbsp;&nbsp;' .$edithref;
                            $sub_data["parent_id"] = $row["parent_id"];
                            $data[] = $sub_data;
                        }
                       
                        

                            if(isset($data)):
                            foreach($data as $key => & $value) {
                                $output[$value["id"]] = & $value;
                            }

                            foreach($data as $key => & $value) {
                                if ($value["parent_id"] && isset($output[$value["parent_id"]])) {
                                    $output[$value["parent_id"]]["nodes"][] = & $value;
                                }
                            }
                            foreach($data as $key => & $value) {
                                if ($value["parent_id"] && isset($output[$value["parent_id"]])) {
                                    unset($data[$key]);
                                }
                            }
                        endif;

                        
                       
                       ?>    
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>




<script>
        $('#treeview').treeview({
        color: "#428bca",
        data: <?php echo json_encode($data);?>
        });
</script>                             
