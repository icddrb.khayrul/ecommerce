<?php

$name = '';
$id = 0;
$active = 0;
$address = '';
$registration_no = '';
$mobile = '';
$phone = '';
$email = '';
$remarks = '';

if(!empty($userInfo))
{
    foreach ($userInfo as $uf)
    {
    
        $name = $uf->name;
        $id = $uf->id;
        $active = $uf->active;
        $address = $uf->address;
		
		$registration_no = $uf->registration_no;
		$mobile = $uf->mobile;
		$phone = $uf->phone;
		$email = $uf->email;
		$remarks = $uf->remarks;
		
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <div class="row">
		     <div class="col-xs-6 text-left header-margin ">
                   <h3>
					<?php echo $pageTitle ?>
					<small>(Add, Edit)</small>
					<?php $baseID = $this->input->get('baseID',TRUE); ?>
				  </h3>
               
            </div>
            <div class="col-xs-6 text-right">
                <div class="form-group">
                     <a class="btn btn-primary" href="<?php echo base_url().$controller.'?baseID='.$baseID ?>"><?php echo $shortName ?> List</a>
                </div>
            </div>
        </div>
    </section>
    </section>
    
    <section class="content content-margin">
	        
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $boxTitle ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
					<?php
						$this->load->helper('form');
						$error = $this->session->flashdata('error');
						if($error)
						{
					?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<?php echo $this->session->flashdata('error'); ?>                    
					</div>
					<?php } ?>
					<?php  
						$success = $this->session->flashdata('success');
						if($success)
						{
					?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<?php echo $this->session->flashdata('success'); ?>
					</div>
					<?php } ?>
					
					<div class="row">
						<div class="col-md-12">
							<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
						</div>
					</div>
                    
                    <form role="form" action="<?php echo base_url().$controller.'/'.$action.'?baseID='.$baseID ?>" method="post" id="editUser" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Item Name">Name</label>
										<input type="hidden"  name="id" value="<?php echo $id ?>">
                                        <input type="text" class="form-control required" id="name"  name="name" value="<?php echo $name ?>" maxlength="255">
                                    </div>
                                </div>
								<div class="col-md-6">
										<div class="form-group">
											<label for="Item Name">Registration Number</label>
											<input type="text" class="form-control required" id="registration_no" value="<?php echo $registration_no ?>"  name="registration_no" maxlength="255">
										</div>
									</div>
								</div>
							
							<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="Item Name">Address</label>
                                        <input type="text" class="form-control required" id="address" value="<?php echo $address ?>" name="address">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Item Name">Mobile Number</label>
                                        <input type="text" class="form-control required" id="mobile" value="<?php echo $mobile ?>" name="mobile" maxlength="255">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Item Name">Phone Number</label>
                                        <input type="text" class="form-control required" id="phone" value="<?php echo $phone ?>" name="phone" maxlength="255">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Item Name">Email</label>
                                        <input type="text" class="form-control required" id="email" value="<?php echo $email ?>" name="email" maxlength="255">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Item Name">Remarks</label>
                                        <input type="text" class="form-control required" id="remarks" value="<?php echo $remarks ?>" name="remarks">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="Active">Active</label>
                                        <select class="form-control required" id="role" name="active">
                                            <option value="1" <?php if($active == 1) {echo "selected=selected";}?>>Yes</option>
											<option value="0"  <?php if($active == 0) {echo "selected=selected";}?> >No</option>
                                            
                                        </select>
                                    </div>
                                </div>    
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Save" />
                          
                        </div>
                    </form>
                </div>
            </div>
            
        </div>    
    </section>
</div>
