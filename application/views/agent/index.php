<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
		     <div class="col-xs-6 text-left header-margin ">
                   <h3>
					<?php echo $pageTitle ?>
					<small>(Add, Edit)</small>
					<?php $baseID = $this->input->get('baseID',TRUE); ?>
				  </h3>
               
            </div>
            <div class="col-xs-6 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url().$controller.'/addNew?baseID='.$baseID ?>">Add New</a>
                </div>
            </div>
        </div>
    </section>
    <section class="content content-margin">
        
        <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $shortName ?> <?php echo $boxTitle ?></h3>
					
					<?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
                <?php
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                   <table id="UnitList" class="table table-bordered table-striped">
					<thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Registration</th>
                      <th>Address</th>
                      <th>Mobile</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Remarks</th>
					  <th>Active</th>
                      <th>Actions</th>
                    </tr>
					</thead>
					<tbody>
                    <?php 
                    if(!empty($userRecords))
                    {
                        foreach($userRecords as $record)
                        {
                    ?>
                    <tr>
                      <td><?php echo $record->id ?></td>
                      <td><?php echo $record->name ?></td>
					  <td><?php echo $record->registration_no ?></td>
                      <td><?php echo $record->address ?></td>
                      <td><?php echo $record->mobile ?></td>
                      <td><?php echo $record->phone ?></td>
                      <td>
					  <?php 
					  
					   $email =  $record->email;
					   
					   $array_email = explode( ',', $email );
					   $loopCount = count($array_email);
					   
					   for ($i=0; $i < $loopCount; $i++)
					   {
						 				   
						   echo $array_email[$i].'</br>';
					   }
					  ?></td>
                      <td><?php echo $record->remarks ?></td>
                      <td>  <?php if ($record->active == 1 ) { echo "Yes"; } else { echo "No"; } ?></td>
                      <td>
                          <a href="<?php echo base_url().$controller.'/editOld/'.$record->id . '?baseID='.$baseID ?>"><i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;</a>
                        
					  </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
					</tbody>
					<tfoot>
					<tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Registration</th>
                      <th>Address</th>
                      <th>Mobile</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Remarks</th>
					  <th>Active</th>
                      <th>Actions</th>
                    </tr>
					</tfoot>
                  </table>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
 <script>
  $(function () {
        $("#UnitList").DataTable({
        "order": [[ 0, "asc" ]],
		dom: 'lBfrtip',
		'lengthMenu': [[10, 25, 50,100, -1], [10, 25, 50,100, 'All']],
		"scrollX": true,
		"columnDefs": [
      { "width": "10px", "targets": 0 },
      { "width": "130px", "targets": 1 },
	  { "width": "100px", "targets": 2 },
	  { "width": "120px", "targets": 3 },
	  { "width": "120px", "targets": 4 },
	  { "width": "100px", "targets": 5 },
	  { "width": "120px", "targets": 6 },
	  { "width": "100px", "targets": 7 },
	  { "width": "120px", "targets": 8},
	  { "width": "140px", "targets": 9}
	  ]
    });
      });
  </script>                                
