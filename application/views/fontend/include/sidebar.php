<div class="container-fluid" id="main-container">
      <div class="row">
 
 <!-- Sidebar -->
    <div class="col" id="main-sidebar">
          <div class="list-group list-group-flush">
            <a href="<?php echo base_url()?>fontend" class="list-group-item list-group-item-action active"><i class="fa fa-home fa-lg fa-fw"></i> Home</a>
            <a href="<?php echo base_url()?>fontend/grid" class="list-group-item list-group-item-action"><i class="fa fa-star fa-lg fa-fw text-warning"></i> Editor's Choice</a>
            <a href="<?php echo base_url()?>fontend/category" class="list-group-item list-group-item-action"><i class="fa fa-th fa-lg fa-fw"></i> Categories</a>

            <?php foreach($sidebar_content['categoryTop'] as $value): ?>
            <a href="<?php echo base_url()?>fontend/product_with_category/<?php echo $value->id; ?>" class="list-group-item list-group-item-action sub"><?php echo $value->name; ?></a>
            <?php endforeach; ?>
            
            <div class="collapse" id="categories">
              <?php foreach($sidebar_content['categoryMore'] as $value): ?>
              <a href="<?php echo base_url()?>fontend/product_with_category/<?php echo $value->id; ?>" class="list-group-item list-group-item-action sub"><?php echo $value->name; ?></a>
              <?php endforeach; ?>
            </div>

            <a href="#categories" class="list-group-item list-group-item-action sub toggle" data-toggle="collapse" aria-expanded="false">MORE &#9662;</a>
            <a href="about.html" class="list-group-item list-group-item-action"><i class="fa fa-list fa-lg fa-fw"></i> Other Pages</a>
            <a href="<?php echo base_url()?>fontend/aboutus" class="list-group-item list-group-item-action sub">About Us</a>
            <a href="<?php echo base_url()?>fontend/cart" class="list-group-item list-group-item-action sub">Cart</a>
            <a href="<?php echo base_url()?>fontend/checkout" class="list-group-item list-group-item-action sub">Checkout</a>
            <a href="<?php echo base_url()?>fontend/compare" class="list-group-item list-group-item-action sub">Compare</a>
            <a href="<?php echo base_url()?>fontend/contractus" class="list-group-item list-group-item-action sub">Contact Us</a>
            <a href="<?php echo base_url()?>fontend/error" class="list-group-item list-group-item-action sub">Error 404</a>
            <a href="<?php echo base_url()?>fontend/faq" class="list-group-item list-group-item-action sub">FAQ</a>
            <a href="<?php echo base_url()?>fontend/login" class="list-group-item list-group-item-action sub">Login / Register</a>
            
            <div class="collapse" id="pages">
            
              <a href="<?php echo base_url()?>fontend/profile" class="list-group-item list-group-item-action sub">My Profile</a>
              <a href="<?php echo base_url()?>fontend/order" class="list-group-item list-group-item-action sub">My Orders</a>
              <a href="<?php echo base_url()?>fontend/address" class="list-group-item list-group-item-action sub">My Address</a>
              <a href="<?php echo base_url()?>fontend/wishlist" class="list-group-item list-group-item-action sub">My Wishlist</a>
            </div>
            
            <a href="#pages" class="list-group-item list-group-item-action sub toggle" data-toggle="collapse" aria-expanded="false">MORE &#9662;</a>
            
            <a href="#" class="list-group-item list-group-item-action"><i class="fa fa-question-circle fa-lg fa-fw"></i> Help</a>
            <a href="#" class="list-group-item list-group-item-action"><i class="fa fa-plus-circle fa-lg fa-fw"></i> Start Selling</a>
          </div>
          <div class="small p-3">Copyright © 2018 Mimity All right reserved</div>
        </div>
        <!-- /Sidebar -->