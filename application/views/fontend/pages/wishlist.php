<div class="col" id="main-content">

<div class="card user-card">
  <div class="card-body">
    <div class="media">
      <img src="<?php echo base_url(); ?>assets/assets-fontend/img/user.svg" width="100" height="100" class="img-thumbnail rounded-circle" alt="John Thor">
      <div class="media-body ml-3 pt-4">
        <h4>John Thor</h4>
        <div class="small text-muted">Joined Dec 31, 2017</div>
        <div class="small text-muted">Points: 100</div>
      </div>
    </div>
    <hr>
    <ul class="nav nav-pills">
      <li class="nav-item">
        <a class="nav-link" href="account-profile.html">Profile</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="account-order.html">Orders</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="account-address.html">Address</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="account-wishlist.html">Wishlist</a>
      </li>
    </ul>
    <hr>
    <table class="table table-cart">
      <tbody>
        
        <tr>
          <td>
            <button class="btn btn-sm btn-outline-warning rounded-circle" title="Remove"><i class="fa fa-close"></i></button>
          </td>
          <td>
            <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/2.jpg" width="50" height="50" alt="Apple 15.4&quot; MacBook Pro Laptop Space Gray"></a>
            <button class="btn btn-sm btn-outline-warning rounded">Remove</button>
          </td>
          <td>
            <h6><a href="detail.html" class="text-body">Apple 15.4&quot; MacBook Pro Laptop Space Gray</a></h6>
            <h6 class="text-muted">$2,720.38</h6>
            
              <span class="badge badge-success font-weight-light">In Stock</span>
            
          </td>
          <td>
            <button class="btn btn-info btn-sm">BUY NOW</button>
          </td>
        </tr>
        
        <tr>
          <td>
            <button class="btn btn-sm btn-outline-warning rounded-circle" title="Remove"><i class="fa fa-close"></i></button>
          </td>
          <td>
            <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/4.jpg" width="50" height="50" alt="Sony Alpha a6500 Mirrorless Digital Camera"></a>
            <button class="btn btn-sm btn-outline-warning rounded">Remove</button>
          </td>
          <td>
            <h6><a href="detail.html" class="text-body">Sony Alpha a6500 Mirrorless Digital Camera</a></h6>
            <h6 class="text-muted">$1,098.00</h6>
            
              <span class="badge badge-dark font-weight-light">Out of Stock</span>
            
          </td>
          <td>
            <button class="btn btn-info btn-sm">BUY NOW</button>
          </td>
        </tr>
        
        <tr>
          <td>
            <button class="btn btn-sm btn-outline-warning rounded-circle" title="Remove"><i class="fa fa-close"></i></button>
          </td>
          <td>
            <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/6.jpg" width="50" height="50" alt="Xbox One X 1TB Console - PUBG Bundle"></a>
            <button class="btn btn-sm btn-outline-warning rounded">Remove</button>
          </td>
          <td>
            <h6><a href="detail.html" class="text-body">Xbox One X 1TB Console - PUBG Bundle</a></h6>
            <h6 class="text-muted">$499.99</h6>
            
              <span class="badge badge-success font-weight-light">In Stock</span>
            
          </td>
          <td>
            <button class="btn btn-info btn-sm">BUY NOW</button>
          </td>
        </tr>
        
        <tr>
          <td colspan="4">
            <button class="btn btn-outline-secondary">CLEAR ALL</button>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<!-- Footer -->
<?php $this->load->view('fontend/pages/footer-content'); ?>
<!-- /Footer -->

</div>