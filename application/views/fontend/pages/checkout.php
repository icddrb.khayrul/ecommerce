<?php
$name = '';
$mobile = '';
$address = '';
$email = '';

if(isset($customer_data)):



  $name = $customer_data->name;
  $mobile = $customer_data->mobile;
  $address = $customer_data->address;
  $email = $customer_data->email;

 
endif;

?>

<style>
label.error{
  color:red
}
</style>

<div class="col" id="main-content">

          <div class="row">
            <div class="col-sm-7 col-md-8">
              <h3 class="title"><i class="fa fa-map-marker"></i> Delivery address</h3>
              <span class="text-muted">Step 1 of 3</span>
              <hr>
              <form id="form" method="post" action="<?php echo base_url()?>fontend/save_customer" class="bg-light p-3 border shadow-sm">
                <div class="form-group">
                  <label for="checkoutName">Name <span style="color:red">*</span></label>
                  <input type="text" name="name" value="<?php echo $name; ?>" class="form-control required" id="checkoutName">
                </div>

                <div class="form-group">
                  <label for="checkoutPhone">Phone <span style="color:red">*</span> <span id="exitPhone"></span></label>
                  <input type="text" name="phone" value="<?php echo $mobile; ?>" path="<?php echo base_url();?>" class="form-control required number checkMobile" id="mobile">
                </div>
                <div class="form-group">
                  <label for="checkoutCity">Email <span style="color:red">*</span></label>
                  <input type="text" name="email" value="<?php echo $email; ?>" class="form-control required email" id="checkoutCity">
                </div>
                
                <div class="form-group">
                  <label for="checkoutAddr2">Address <span style="color:red">*</span></label>
                  <input type="text" name="address" value="<?php echo $address; ?>" class="form-control required" id="checkoutAddr2" aria-describedby="addr2Help">
                  <small id="addr2Help" class="form-text text-muted">Apartment, unit, floor, etc. Also Care of, Leave at, etc.</small>
                </div>
                
              
                <div class="form-group">
                  <label>SELECT SHIPPING METHOD</label>
                  <div class="custom-control custom-radio">
                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked>
                    <label class="custom-control-label" for="customRadio1">Standard Delivery</label>
                    <span class="float-right">TK&nbsp;&nbsp; 50</span>
                    <small class="form-text text-muted">(3 - 6 business days)</small>
                  </div>
                  <div class="custom-control custom-radio">
                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                    <label class="custom-control-label" for="customRadio2">Premium Delivery</label>
                    <span class="float-right">TK 100</span>
                    <small class="form-text text-muted">(2 - 3 business days)</small>
                  </div>
                  <div class="custom-control custom-radio">
                    <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                    <label class="custom-control-label" for="customRadio3">Express Delivery</label>
                    <span class="float-right">TK 150</span>
                    <small class="form-text text-muted">(1 - 2 business days)</small>
                  </div>
                </div>
                <!-- href="<?php echo base_url()?>fontend/checkoutone" -->
                <button type="submit"  class="btn btn-primary customer_info_save">NEXT <i class="fa fa-angle-right "></i></button>
              </form>
            </div>
            <div class="col-sm-5 col-md-4 pt-5">
              <h4 class="title mb-3">Order summary</h4>
              
              <?php 
              $subtotal = 0;
              foreach($cart_item_list as $value):
                      $subtotal += $value->quantity*$value->base_price; 
              ?>
              <div class="media border-bottom mb-3">
                <img src="<?php echo base_url(); ?>assets/images/upload/thrum/<?php echo $value->path; ?>" width="50" height="50" alt="<?php echo $value->prod_name?>">
                <div class="media-body ml-3">
                  <h6><?php echo $value->prod_name?></h6>
                  <div><?php echo $value->quantity?> x <span class="price">TK <?php echo $value->base_price*$value->quantity; ?></span></div>
                </div>
              </div>
              <?php endforeach; ?>
              
              
              <div class="d-flex justify-content-between">
                
                
                <table width="100%">
                  <tr>
                    <td class="pull-left" style="width:200px"><span>Items</span></td>
                    <td><span>TK</span></td>
                    <td class="pull-right"><span><?php echo number_format($subtotal ,2); ?></span></td>
                  </tr>
                  <tr>
                    <td class="pull-left"><span>Shipping</span></td>
                    <td><span>TK</span></td>
                    <td class="pull-right"><span><?php echo number_format(50 ,2); ?></span></span></td>
                  </tr>
                </table>
              </div>
              <hr>
              <div class="box-total">
                  <h4>TOTAL</h4>
                  <h4><span class="price">TK <?php echo number_format($subtotal+50 ,2); ?></span></h4>
              </div>
            </div>
          </div>

          <!-- Footer -->
          <?php $this->load->view('fontend/pages/footer-content'); ?>
          <!-- /Footer -->

        </div>

        <script>
            $(document).ready(function(){

                  $.validator.addMethod("email", 
                      function(value, element) {
                          return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
                      }, 
                      "Sorry, I've enabled very strict email validation"
                  );

            });
        
        </script>