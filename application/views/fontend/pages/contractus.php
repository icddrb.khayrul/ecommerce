<div class="col" id="main-content">

<div class="row">
  <div class="col-sm-6 mb-3 mb-sm-0">
    <div class="img-thumbnail">
      <div class="embed-responsive embed-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3021.9264094650316!2d-73.97488578459351!3d40.763643279326224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c258f00eb25f59%3A0x75ddbee78904e799!2s767+5th+Ave%2C+New+York%2C+NY+10153%2C+USA!5e0!3m2!1sen!2sid!4v1532319134271" width="600" height="450" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <h3>CONTACT US</h3>
    <form class="mt-3">
      <div class="form-row">
        <div class="form-group col-md-6">
          <div class="media">
            <span><i class="fa fa-map-marker fa-fw text-info"></i></span>
            <div class="media-body ml-1">
              <div>767 Fifth Avenue</div>
              <div>New York</div>
              <div>NY 10153</div>
            </div>
          </div>
        </div>
        <div class="form-group col-md-6">
          <div class="media mb-3 mb-md-0">
            <span><i class="fa fa-phone fa-fw text-info"></i></span>
            <div class="media-body ml-1">212 123 456 789</div>
          </div>
          <div class="media">
            <span><i class="fa fa-envelope fa-fw text-info"></i></span>
            <div class="media-body ml-1">support@example.com</div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Name">
      </div>
      <div class="form-group">
        <input type="email" class="form-control" placeholder="Email">
      </div>
      <div class="form-group">
        <textarea class="form-control" rows="3" placeholder="Message"></textarea>
      </div>
      <button type="submit" class="btn btn-info">SEND</button>
    </form>
  </div>
</div>

<!-- Footer -->
<?php $this->load->view('fontend/pages/footer-content'); ?>
<!-- /Footer -->

</div>