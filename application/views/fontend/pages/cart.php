<div class="col" id="main-content">

<h3 class="title mb-3">You have <?php echo count($cart_item_list); ?> items in your cart</h3>

<!-- Shopping Cart Table -->
<table class="table table-cart">
  <tbody>
    <?php 
    $subtotal = 0;
    foreach($cart_item_list as $value):
            $subtotal += $value->quantity*$value->base_price; 
    ?>
    <tr id="media<?php echo $value->id?>">
      <td><button class="btn btn-sm btn-outline-warning rounded-circle delete_item" path="<?php echo base_url(); ?>" id="<?php echo $value->id?>" quantity="<?php echo $value->quantity?>" title="Remove"><i class="fa fa-close"></i></button></td>
      <td>
        <a href="<?php echo base_url();?>fontend/details/<?php echo $value->id?>"><img src="<?php echo base_url(); ?>assets/images/upload/thrum/<?php echo $value->path; ?>" width="50" height="50" alt="NEW Microsoft Surface Go"></a>
        <button class="btn btn-sm btn-outline-warning rounded">Remove</button>
      </td>
      <td>
        <h6><a href="<?php echo base_url();?>fontend/details/<?php echo $value->id?>" class="text-body"><?php echo $value->prod_name?></a></h6>
        <h6 class="text-muted">TK <?php echo number_format($value->base_price,2)?></h6>
        
        <span class="badge badge-success font-weight-light"><?php echo $value->color; ?></span>
        <span class="badge badge-dark font-weight-light"><?php echo $value->size?></span>
        
      </td>
      <td>
        <div class="input-spinner">
          <input type="number" readonly="readonly" style="background:none" class="form-control" value="<?php echo $value->quantity?>" min="1" max="999">
          <div class="btn-group-vertical">
            <button type="button" plusMinus="plus" path="<?php echo base_url()?>addtocart/addProductToTemp" data-id="<?php echo $value->id; ?>" class="btn btn-light add-to-cart"><i class="fa fa-chevron-up"></i></button>
            <button type="button" path="<?php echo base_url()?>addtocart/addProductToTemp" data-id="<?php echo $value->id; ?>" plusMinus="minus" type="button" class="btn btn-light add-to-cart"><i class="fa fa-chevron-down"></i></button>
          </div>
        </div>
        <span class="price">TK <?php echo number_format($value->base_price*$value->quantity,2); ?></span>
      </td>
    </tr>
    <?php endforeach; ?>
    
    
    <tr>
      <td colspan="4">
        <div class="box-total">
          <h4>Subotal: <span class="price">TK <?php echo number_format($subtotal,2); ?></span></h4>
          <a href="<?php echo base_url()?>fontend/checkout" class="btn btn-success">CHECKOUT</a>
        </div>
      </td>
    </tr>
    
  </tbody>
</table>
<!-- /Shopping Cart Table -->

<!-- Recently viewed-->
<h4>Recently viewed items</h4>
<div class="content-slider">
  <div class="swiper-container" id="popular-slider">
    <div class="swiper-wrapper">
      
      <div class="swiper-slide">
        <div class="row no-gutters gutters-2">
          <div class="col-6 col-md-3 mb-2">
            <div class="card card-product">
              <div class="badge badge-success badge-pill">save $89.01</div>
              <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
              <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/9.jpg" alt="ASUS VivoBook F510UA FHD Laptop" class="card-img-top"></a>
              <div class="card-body">
                <span class="price"><del class="small text-muted">$599.00</del> $509.99</span>
                <a href="detail.html" class="card-title h6">ASUS VivoBook F510UA FHD Laptop</a>
                <div class="d-flex justify-content-between align-items-center">
                  <span class="rating" data-value="4"></span>
                  <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-3 mb-2">
            <div class="card card-product">
              <div class="ribbon"><span class="bg-pink text-white">Hot</span></div>
              <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
              <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/10.jpg" alt="Nikon D7200 DX-format DSLR Body (Black)" class="card-img-top"></a>
              <div class="card-body">
                <span class="price">$996.95</span>
                <a href="detail.html" class="card-title h6">Nikon D7200 DX-format DSLR Body (Black)</a>
                <div class="d-flex justify-content-between align-items-center">
                  <span class="rating" data-value="5"></span>
                  <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-3 mb-2">
            <div class="card card-product">
              <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
              <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/11.jpg" alt="Polk Audio PSW10 10-Inch Powered Subwoofer" class="card-img-top"></a>
              <div class="card-body">
                <span class="price">$99.99</span>
                <a href="detail.html" class="card-title h6">Polk Audio PSW10 10-Inch Powered Subwoofer</a>
                <div class="d-flex justify-content-between align-items-center">
                  <span class="rating" data-value="4"></span>
                  <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-3 mb-2">
            <div class="card card-product">
              <div class="badge badge-danger badge-pill">Only 1 left in stock</div>
              <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
              <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/12.jpg" alt="Essential Phone in Halo Gray – 128 GB Unlocked" class="card-img-top"></a>
              <div class="card-body">
                <span class="price"><del class="small text-muted">$499.99</del> $435.00</span>
                <a href="detail.html" class="card-title h6">Essential Phone in Halo Gray – 128 GB Unlocked</a>
                <div class="d-flex justify-content-between align-items-center">
                  <span class="rating" data-value="4"></span>
                  <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="swiper-slide">
        <div class="row no-gutters gutters-2">
          <div class="col-6 col-md-3 mb-2">
            <div class="card card-product">
              <div class="badge badge-success badge-pill">save $89.01</div>
              <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
              <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/9.jpg" alt="ASUS VivoBook F510UA FHD Laptop" class="card-img-top"></a>
              <div class="card-body">
                <span class="price"><del class="small text-muted">$599.00</del> $509.99</span>
                <a href="detail.html" class="card-title h6">ASUS VivoBook F510UA FHD Laptop</a>
                <div class="d-flex justify-content-between align-items-center">
                  <span class="rating" data-value="4"></span>
                  <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-3 mb-2">
            <div class="card card-product">
              <div class="ribbon"><span class="bg-pink text-white">Hot</span></div>
              <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
              <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/10.jpg" alt="Nikon D7200 DX-format DSLR Body (Black)" class="card-img-top"></a>
              <div class="card-body">
                <span class="price">$996.95</span>
                <a href="detail.html" class="card-title h6">Nikon D7200 DX-format DSLR Body (Black)</a>
                <div class="d-flex justify-content-between align-items-center">
                  <span class="rating" data-value="5"></span>
                  <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-3 mb-2">
            <div class="card card-product">
              <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
              <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/11.jpg" alt="Polk Audio PSW10 10-Inch Powered Subwoofer" class="card-img-top"></a>
              <div class="card-body">
                <span class="price">$99.99</span>
                <a href="detail.html" class="card-title h6">Polk Audio PSW10 10-Inch Powered Subwoofer</a>
                <div class="d-flex justify-content-between align-items-center">
                  <span class="rating" data-value="4"></span>
                  <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-3 mb-2">
            <div class="card card-product">
              <div class="badge badge-danger badge-pill">Only 1 left in stock</div>
              <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
              <a href="detail.html"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/12.jpg" alt="Essential Phone in Halo Gray – 128 GB Unlocked" class="card-img-top"></a>
              <div class="card-body">
                <span class="price"><del class="small text-muted">$499.99</del> $435.00</span>
                <a href="detail.html" class="card-title h6">Essential Phone in Halo Gray – 128 GB Unlocked</a>
                <div class="d-flex justify-content-between align-items-center">
                  <span class="rating" data-value="4"></span>
                  <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <a href="#" role="button" class="carousel-control-prev" id="popular-slider-prev"><i class="fa fa-angle-left fa-lg"></i></a>
  <a href="#" role="button" class="carousel-control-next" id="popular-slider-next"><i class="fa fa-angle-right fa-lg"></i></a>
</div>
<!-- /Recently viewed-->

<!-- Footer -->

<?php $this->load->view('fontend/pages/footer-content'); ?>

<!-- /Footer -->

</div>