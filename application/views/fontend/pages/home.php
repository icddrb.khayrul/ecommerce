    
    <div class="col" id="main-content">

          <!-- Home Slider -->
          <div class="swiper-container" id="home-slider">
            <div class="swiper-wrapper">
              <a href="grid.html" class="swiper-slide" data-cover="<?php echo base_url(); ?>assets/assets-fontend/img/slider/1.jpg" data-xs-height="150px" data-sm-height="265px" data-md-height="300px" data-lg-height="300px" data-xl-height="300px"></a>
              <a href="grid.html" class="swiper-slide" data-cover="<?php echo base_url(); ?>assets/assets-fontend/img/slider/2.jpg" data-xs-height="150px" data-sm-height="265px" data-md-height="300px" data-lg-height="300px" data-xl-height="300px"></a>
              <a href="grid.html" class="swiper-slide" data-cover="<?php echo base_url(); ?>assets/assets-fontend/img/slider/3.jpg" data-xs-height="150px" data-sm-height="265px" data-md-height="300px" data-lg-height="300px" data-xl-height="300px"></a>
            </div>
            <a href="#" role="button" class="carousel-control-prev d-none d-sm-flex" id="home-slider-prev"><i class="fa fa-angle-left fa-lg"></i></a>
            <a href="#" role="button" class="carousel-control-next d-none d-sm-flex" id="home-slider-next"><i class="fa fa-angle-right fa-lg"></i></a>
          </div>
          <!-- /Home Slider -->

          <!-- Services -->
          <div class="row services-box">
            <div class="col-6 col-md-3">
              <div class="media">
                <i class="fa fa-truck" style="font-size:35px; padding-right:5px"></i>
                <div class="media-body">
                  <h6>FREE SHIPPING</h6>
                  <span class="text-muted d-none d-md-block">Get free shipping for all orders $99 or more</span>
                </div>
              </div>
            </div>
            <div class="col-6 col-md-3">
              <div class="media">
                <i class="fa fa-refresh" style="font-size:35px; padding-right:5px"></i>
                <div class="media-body">
                  <h6>MONEY BACK GUARANTEE</h6>
                  <span class="text-muted d-none d-md-block">Get the item you ordered, or your money back</span>
                </div>
              </div>
            </div>
            <div class="col-6 col-md-3">
              <div class="media">
                <i class="fa user-shield" style="font-size:35px; padding-right:5px"></i>
                <div class="media-body">
                  <h6>100% SECURE PAYMENT</h6>
                  <span class="text-muted d-none d-md-block">Your transaction are secure with SSL Encryption</span>
                </div>
              </div>
            </div>
            <div class="col-6 col-md-3">
              <div class="media">
                <i class="fa fa-phone" style="font-size:35px; padding-right:5px"></i>
                <div class="media-body">
                  <h6>ONLINE SUPPORT 24/7</h6>
                  <span class="text-muted d-none d-md-block">Chat with experts or have us call you right away</span>
                </div>
              </div>
            </div>
          </div>
          <!-- /Services -->

          <!-- Categories Slider -->
          <h3 class="title mt-4">Shop by Categories</h3>
          <div class="content-slider">
            <div class="swiper-container categories-slider" id="categories-slider">
              <div class="swiper-wrapper">

                <div class="swiper-slide">
                  <div class="row no-gutters gutters-1">
                  <?php foreach($category as $value): ?>  
                    <div class="col-6 col-md-3 mb-1">
                      <a href="<?php echo base_url()?>fontend/product_with_category/<?php echo $value->parent_id; ?>" class="card progressive">
                        <img class="card-img-top" src="<?php echo base_url(); ?>assets/images/upload/thrum/<?php echo $value->path; ?>" alt="">
                        <div class="card-img-overlay card-img-overlay-bottom p-2 pink">
                          <h5><?php echo $value->cat_name; ?></h5>
                        </div>
                      </a>
                    </div>
                    <?php endforeach; ?>  
                  </div>
                </div>

                <div class="swiper-slide">
                  <div class="row no-gutters gutters-1">
                    <div class="col-6 col-md-3 mb-1">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/categories/5.jpg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                        <div class="card-img-overlay card-img-overlay-bottom p-2 success">
                          <h5>Video Games</h5>
                        </div>
                      </a>
                    </div>
                    <div class="col-6 col-md-3 mb-1">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/categories/6.jpg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                        <div class="card-img-overlay card-img-overlay-bottom p-2 warning">
                          <h5>Headphones</h5>
                        </div>
                      </a>
                    </div>
                    <div class="col-6 col-md-3 mb-1">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/categories/8.jpg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                        <div class="card-img-overlay card-img-overlay-bottom p-2 danger">
                          <h5>Office Supplies</h5>
                        </div>
                      </a>
                    </div>
                    <div class="col-6 col-md-3 mb-1">
                      <a href="categories.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/more.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <a href="#" role="button" class="carousel-control-prev" id="categories-slider-prev"><i class="fa fa-angle-left fa-lg"></i></a>
            <a href="#" role="button" class="carousel-control-next" id="categories-slider-next"><i class="fa fa-angle-right fa-lg"></i></a>
          </div>
          <!-- /Categories Slider -->

          <!-- Hot new releases -->
          <h3 class="title mt-4">Hot New Releases</h3>
          <div class="row no-gutters gutters-2">

          <?php foreach($product as $value):?>
            <div class="col-6 col-md-3 mb-2">
              <div class="card card-product">
                <div class="ribbon"><span class="bg-info text-white">New</span></div>
                <button class="wishlist" title="Add to wishlist"><i class="fa fa-heart"></i></button>
                <a href="<?php echo base_url()?>fontend/details/<?php echo $value->id; ?>"><img data-progressive="<?php echo base_url(); ?>assets/images/upload/thrum/<?php echo $value->path; ?>" alt="NEW Microsoft Surface Go" class="card-img-top progressive__img progressive--not-loaded"></a>
                <div class="card-body">
                  <span class="price">TK <?php echo $value->base_price; ?></span>
                  <a href="<?php echo base_url()?>fontend/details/<?php echo $value->id; ?>" class="card-title h6"><?php echo $value->prod_name; ?></a>
                  <div class="d-flex justify-content-between align-items-center">
                    <button type="button" plusminus="plus" path="<?php echo base_url()?>addtocart/addProductToTemp" data-id="<?php echo $value->id; ?>" class="btn btn-outline-info btn-sm btn-block add-to-cart">Add to cart</button>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>  
          </div>
          <!-- /Hot new releases -->

          <!-- Popular Brands -->
          <h3 class="title mt-4">Popular Brands</h3>
          <div class="content-slider">
            <div class="swiper-container brands-slider" id="brands-slider">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div class="row no-gutters gutters-2">
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card">
                        <img class="card-img-top progressive__img progressive--not-loaded" data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/samsung.svg" alt="" >
                      </a>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/lenovo.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/sony.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/canon.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/xerox.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/lg.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="row no-gutters gutters-2">
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/xbox.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/dell.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/nikon.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/sandisk.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/gopro.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 mb-2">
                      <a href="grid.html" class="card progressive">
                        <img data-progressive="<?php echo base_url(); ?>assets/assets-fontend/img/brands/logitech.svg" alt="" class="card-img-top progressive__img progressive--not-loaded">
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <a href="#" role="button" class="carousel-control-prev" id="brands-slider-prev"><i class="fa fa-angle-left fa-lg"></i></a>
            <a href="#" role="button" class="carousel-control-next" id="brands-slider-next"><i class="fa fa-angle-right fa-lg"></i></a>
          </div>
          <!-- /Popular Brands -->

          <!-- Popular -->
          <h3 class="title mt-4">Popular this week</h3>

         
          <div class="content-slider">
            <div class="swiper-container" id="popular-slider">
              <div class="swiper-wrapper">
                
                <div class="swiper-slide">
                  <div class="row no-gutters gutters-2">

                  <?php foreach($product as $value):?>
                    <div class="col-6 col-md-3 mb-2">
                      <div class="card card-product">
                        <div class="badge badge-success badge-pill">&nbsp;</div>
                        <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
                        <a href="<?php echo base_url()?>fontend/details/<?php echo $value->id; ?>"><img data-progressive="<?php echo base_url(); ?>assets/images/upload/thrum/<?php echo $value->path; ?>" alt="ASUS VivoBook F510UA FHD Laptop" class="card-img-top progressive__img progressive--not-loaded"></a>
                        <div class="card-body">
                          <span class="price"><del class="small text-muted">TK 599.00</del> TK <?php echo $value->base_price; ?></span>
                          <a href="<?php echo base_url()?>fontend/details/<?php echo $value->id; ?>" class="card-title h6"><?php echo $value->prod_name; ?></a>
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="rating" data-value="4"></span>
                            <button type="button" plusminus="plus" path="<?php echo base_url()?>addtocart/addProductToTemp" data-id="<?php echo $value->id; ?>" class="btn btn-outline-info btn-sm add-to-cart">Add to cart</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endforeach; ?>  
                    
                  </div>
                </div>
                
              
                
              </div>
            </div>
            <a href="#" role="button" class="carousel-control-prev" id="popular-slider-prev"><i class="fa fa-angle-left fa-lg"></i></a>
            <a href="#" role="button" class="carousel-control-next" id="popular-slider-next"><i class="fa fa-angle-right fa-lg"></i></a>
          </div>
          <!-- /Popular -->

          <!-- Footer -->
          <?php $this->load->view('fontend/pages/footer-content'); ?>
          <!-- /Footer -->

        </div>

        