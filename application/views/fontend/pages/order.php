<div class="col" id="main-content">

          <div class="card user-card">
            <div class="card-body">
              <div class="media">
                <img src="<?php echo base_url(); ?>assets/assets-fontend/img/user.svg" width="100" height="100" class="img-thumbnail rounded-circle" alt="John Thor">
                <div class="media-body ml-3 pt-4">
                  <h4>John Thor</h4>
                  <div class="small text-muted">Joined Dec 31, 2017</div>
                  <div class="small text-muted">Points: 100</div>
                </div>
              </div>
              <hr>
              <ul class="nav nav-pills">
                <li class="nav-item">
                  <a class="nav-link" href="account-profile.html">Profile</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="account-order.html">Orders</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="account-address.html">Address</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="account-wishlist.html">Wishlist</a>
                </li>
              </ul>
              <hr>
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Order ID</th>
                      <th scope="col">Date</th>
                      <th scope="col">Total</th>
                      <th scope="col">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row"><a href="#" class="text-info">1712697HPKINV</a></th>
                      <td>Dec 19, 2017</td>
                      <td>$74.00</td>
                      <td><span class="badge badge-warning">In Progress</span></td>
                    </tr>
                    <tr>
                      <th scope="row"><a href="#" class="text-info">171264CK6FINV</a></th>
                      <td>Dec 10, 2017</td>
                      <td>$100.00</td>
                      <td><span class="badge badge-danger">Canceled</span></td>
                    </tr>
                    <tr>
                      <th scope="row"><a href="#" class="text-info">171266PC4AINV</a></th>
                      <td>Dec 01, 2017</td>
                      <td>$20.00</td>
                      <td><span class="badge badge-success">Finished</span></td>
                    </tr>
                    <tr>
                      <th scope="row"><a href="#" class="text-info">1711697HPKINV</a></th>
                      <td>Nov 19, 2017</td>
                      <td>$74.00</td>
                      <td><span class="badge badge-success">Finished</span></td>
                    </tr>
                    <tr>
                      <th scope="row"><a href="#" class="text-info">171164CK6FINV</a></th>
                      <td>Nov 10, 2017</td>
                      <td>$100.00</td>
                      <td><span class="badge badge-success">Finished</span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <!-- Footer -->
          <?php $this->load->view('fontend/pages/footer-content'); ?>
          <!-- /Footer -->

        </div>