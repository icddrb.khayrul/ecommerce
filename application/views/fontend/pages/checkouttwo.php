<?php
$customer_id = 0;
$delivery_charge = 50;
$name = '';
$mobile = '';
$address = '';
$email = '';

if(isset($customer_info)):


  $customer_id = $customer_info->customer_id; 
  $name = $customer_info->name;
  $mobile = $customer_info->mobile;
  $address = $customer_info->address;
  $email = $customer_info->email;

 
endif;

?>


<div class="col" id="main-content">

<div class="row">
  <div class="col-sm-7 col-md-8">
    <h3 class="title"><i class="fa fa-list"></i> Review your order</h3>
    <span class="text-muted">Step 3 of 3</span>
    <hr>
     <?php 
        $subtotal = 0;
        foreach($cart_item_list as $value):
                $subtotal += $value->quantity*$value->base_price; 
      ?>
    <div class="media border-bottom mb-3">
      <img src="<?php echo base_url(); ?>assets/images/upload/thrum/<?php echo $value->path; ?>" width="50" height="50" alt="NEW Microsoft Surface Go">
      <div class="media-body ml-3">
        <h6><?php echo $value->prod_name?></h6>
        <div><?php echo $value->quantity; ?> x <span class="price">TK <?php echo $value->base_price?></span></div>
      </div>
    </div>
    <?php endforeach; ?>      
    
   
    
    <a href="<?php echo site_url('fontend/cart')?>" class="btn btn-outline-secondary btn-sm">Edit Cart</a>
  </div>
  <div class="col-sm-5 col-md-4 pt-5">
    <h4>Shipping to</h4>
    <div><?php echo $name; ?></div>
    <div><?php echo $address; ?></div>
    <div><?php echo $email; ?></div>
    <div><?php echo $mobile; ?></div>
    <hr>
    <h4>Payment Method</h4>
    <p>Cash on delivery</p>
    <hr>
    <table width="100%">
      <tr>
        <td class="pull-left" style="width:200px"><span>Items</span></td>
        <td><span>TK</span></td>
        <td class="pull-right"><span><?php echo number_format($subtotal ,2); ?></span></td>
      </tr>
      <tr>
        <td class="pull-left"><span>Shipping</span></td>
        <td><span>TK</span></td>
        <td class="pull-right"><span><?php echo number_format(50 ,2); ?></span></span></td>
      </tr>
    </table>
    <hr>
    <div class="box-total">
        <h4>TOTAL</h4>
        <h4><span class="price">TK <?php echo number_format($subtotal+50 ,2); ?></span></h4>
    </div>
    <hr>
    <form action="<?php echo base_url(); ?>fontend/save_order" method="post">
      <?php foreach($cart_item_list as $value): ?>
        <input type="hidden" name="prod_id[]" value="<?php echo $value->prod_id; ?>">
        <input type="hidden" name="quantity[]" value="<?php echo $value->quantity; ?>">
      <?php endforeach; ?>
      <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>">
      <input type="hidden" name="total" value="<?php echo $subtotal; ?>">
      <input type="hidden" name="delivery_charge" value="<?php echo $delivery_charge; ?>">
      <button type="submit" class="btn btn-primary btn-block">COMPLETE MY ORDER</button>
    </form>
  </div>
</div>

<!-- Footer -->
<?php $this->load->view('fontend/pages/footer-content'); ?>
<!-- /Footer -->

</div>