

        

        <div class="col" id="main-content">

         

        <br/>
          

          <!-- Grid -->
          
          <div class="row no-gutters gutters-2">

            <?php foreach($product as $val): 
            // echo '<pre>';
            // print_r($value);
                  
            ?>
            <div class="col-6 col-md-3 mb-2">
              <div class="card card-product">
                <div class="ribbon"><span class="bg-info text-white">New</span></div>
                <button class="wishlist" title="Add to wishlist"><i class="fa fa-heart"></i></button>
                <a href="<?php echo base_url(); ?>/fontend/details/<?php echo $val->id; ?>"><img src="<?php echo base_url(); ?>assets/images/upload/small/<?php echo $val->path; ?>" data-progressive="<?php echo base_url(); ?>assets/images/upload/thrum/<?php echo $val->path; ?>" alt="NEW Microsoft Surface Go" class="card-img-top  progressive__img progressive--not-loaded"></a>
                <div class="card-body">
                  <span class="price">TK. <?php echo $val->base_price; ?></span>
                  <a href="<?php echo base_url(); ?>/fontend/details/<?php echo $val->id; ?>" class="card-title h6"><?php echo $val->prod_name; ?></a>
                  <div class="d-flex justify-content-between align-items-center">
                    <button type="button" plusminus="plus" path="<?php echo base_url()?>addtocart/addProductToTemp" data-id="<?php echo $val->id; ?>" class="btn btn-outline-info btn-sm btn-block add-to-cart">Add to cart</button>
                  </div>
                </div>
              </div>
            </div>
            <?php 
            endforeach;
            ?>              



          </div>
          
          <!-- /Grid -->

          <!-- Pagination -->
          
          <!-- /Pagination -->

          <!-- Footer -->
          <?php $this->load->view('fontend/pages/footer-content'); ?>
          <!-- /Footer -->

        </div>
     