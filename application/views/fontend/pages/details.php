<?php
$ip = (isset($ip['ip']))?$ip['ip']:'';

$image_path = (isset($product->path))?$product->path:'';
$product_name = (isset($product->product_name))?$product->product_name:'';  
$product_price = (isset($product->base_price))?$product->base_price:''; 
$product_color = (isset($product->color))?$product->color:''; 
$product_size = (isset($product->size))?$product->size:''; 
$product_id = (isset($product->id))?$product->id:''; 

?>


<div class="col" id="main-content">

          <!-- Breadcrumb -->
          <div class="btn-tags">
              <a  style="cursor:pointer" onclick="window.history.back()" class="btn btn-light btn-sm">Back</a>
           </div>
          <!-- /Breadcrumb -->
          <h3 class="title mt-4">Product Detail:</h3>
          <div class="row">
            <div class="col-md-7">
              <div class="img-detail-wrapper">
                <img src="<?php echo base_url(); ?>assets/images/upload/<?php echo $image_path; ?>" class="img-fluid px-5" id="img-detail" alt="Responsive image" data-index="0">
                <div class="img-detail-list">
                  <a href="#" class="active"><img src="<?php echo base_url(); ?>assets/images/upload/<?php echo $image_path; ?>" data-large-src="<?php echo base_url(); ?>assets/images/upload/<?php echo $image_path; ?>" alt="Product" data-index="0"></a>
                </div>
              </div>
            </div>
            <div class="col-md-5">
              <div class="detail-header">
                <h3><?php echo $product_name; ?></h3>
                <!-- <h6><span class="rating" data-value="4.5"></span> <a class="ml-1" href="#reviews">2 reviews</a></h6> -->
                <h3 class="price">TK. <?php echo $product_price; ?></h3>
              </div>
              <form>
                <div class="form-group">
                  <label for="quantity">Quantity</label>
                  <div class="input-spinner">
                    <input type="number" class="form-control" id="quantity" value="1" min="1" max="999">
                    <div class="btn-group-vertical">
                      <button type="button" class="btn btn-light"><i class="fa fa-chevron-up"></i></button>
                      <button type="button" class="btn btn-light"><i class="fa fa-chevron-down"></i></button>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="d-block">Color</label>
                  <div class="btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-outline-success active">
                      <input type="radio" name="ram" checked><?php echo $product_color; ?>
                    </label>
                  </div>
                </div>
                <div class="form-group mb-4">
                  <label class="d-block">Size</label>
                  <div class="btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-outline-dark active">
                      <input type="radio" name="storage" checked><?php echo $product_size; ?>
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <button type="button" plusMinus="plus" path="<?php echo base_url()?>addtocart/addProductToTemp" data-id="<?php echo $product_id; ?>" class="btn btn-info btn-block add-to-cart">ADD TO CART</button>
                </div>
              </form>
            </div>
          </div>
          <hr>
          <div class="row mt-4">
            <div class="col">
              <h3>Description</h3>
              <p>New 10” Surface Go is perfect for all your daily tasks, giving you laptop performance with tablet portability, a stunning touchscreen, and the Windows and Office experience you know. From email, browsing, and home projects to unwinding with a favorite TV show, Surface Lingo is by your side wherever you are — with up to 9 hours1 of battery life, built-in HD cameras, hassle-free connectivity, and all the ports you need, including multi-tasking USB-C.</p>
              <p>Disclaimers:</p>
              <ul>
                <li>
                  Battery
                  <p>life: Up to 9 hours of video playback. Testing conducted by Microsoft in June 2018] using preproduction Intel Pentium Gold 4415Y Processor, 128GB, 8GB RAM device. Testing consisted of full battery discharge during video playback. All settings were default except: Wi-Fi was associated with a network and Auto-Brightness disabled. Battery life varies significantly with settings, usage, and other factors.</p>
                </li>
                <li>
                  Windows
                  <p>10 in S Mode works exclusively with apps from the Microsoft Store within Windows. Certain default settings, features, and apps cannot be changed. Some accessories and apps compatible with Windows 10 may not work (including some antivirus and accessibility apps), and performance may vary. If you switch to Windows 10 Pro configuration (fee may apply), you can’t switch back to Windows 10 in S Mode. Learn more at Windows.com/Windows10SFAQ.</p>
                </li>
              </ul>
              <p>Tech Specs</p>
              <ul>
                <li>10” PixelSense Display</li>
                <li>Intel Pentium Gold</li>
                <li>4GB or 8GB RAM</li>
                <li>64GB or 128GB Storage</li>
                <li>Weight starting at 1.15 lbs</li>
                <li>Up to 9 hours battery life</li>
              </ul>
              <hr>

              <!-- Similar Items -->
              <h3>Similar Items</h3>
              <div class="content-slider">
            <div class="swiper-container" id="popular-slider">
              <div class="swiper-wrapper">
                
                <div class="swiper-slide">
                  <div class="row no-gutters gutters-2">
                    <div class="col-6 col-md-3 mb-2">
                      <div class="card card-product">
                        <div class="badge badge-success badge-pill">save $89.01</div>
                        <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
                        <a href="detail.php"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/9.jpg" alt="ASUS VivoBook F510UA FHD Laptop" class="card-img-top"></a>
                        <div class="card-body">
                          <span class="price"><del class="small text-muted">$599.00</del> $509.99</span>
                          <a href="detail.php" class="card-title h6">ASUS VivoBook F510UA FHD Laptop</a>
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="rating" data-value="4"></span>
                            <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2">
                      <div class="card card-product">
                        <div class="ribbon"><span class="bg-pink text-white">Hot</span></div>
                        <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
                        <a href="detail.php"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/10.jpg" alt="Nikon D7200 DX-format DSLR Body (Black)" class="card-img-top"></a>
                        <div class="card-body">
                          <span class="price">$996.95</span>
                          <a href="detail.php" class="card-title h6">Nikon D7200 DX-format DSLR Body (Black)</a>
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="rating" data-value="5"></span>
                            <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2">
                      <div class="card card-product">
                        <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
                        <a href="detail.php"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/11.jpg" alt="Polk Audio PSW10 10-Inch Powered Subwoofer" class="card-img-top"></a>
                        <div class="card-body">
                          <span class="price">$99.99</span>
                          <a href="detail.php" class="card-title h6">Polk Audio PSW10 10-Inch Powered Subwoofer</a>
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="rating" data-value="4"></span>
                            <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2">
                      <div class="card card-product">
                        <div class="badge badge-danger badge-pill">Only 1 left in stock</div>
                        <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
                        <a href="detail.php"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/12.jpg" alt="Essential Phone in Halo Gray – 128 GB Unlocked" class="card-img-top"></a>
                        <div class="card-body">
                          <span class="price"><del class="small text-muted">$499.99</del> $435.00</span>
                          <a href="detail.php" class="card-title h6">Essential Phone in Halo Gray – 128 GB Unlocked</a>
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="rating" data-value="4"></span>
                            <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="swiper-slide">
                  <div class="row no-gutters gutters-2">
                    <div class="col-6 col-md-3 mb-2">
                      <div class="card card-product">
                        <div class="badge badge-success badge-pill">save $89.01</div>
                        <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
                        <a href="detail.php"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/9.jpg" alt="ASUS VivoBook F510UA FHD Laptop" class="card-img-top"></a>
                        <div class="card-body">
                          <span class="price"><del class="small text-muted">$599.00</del> $509.99</span>
                          <a href="detail.php" class="card-title h6">ASUS VivoBook F510UA FHD Laptop</a>
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="rating" data-value="4"></span>
                            <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2">
                      <div class="card card-product">
                        <div class="ribbon"><span class="bg-pink text-white">Hot</span></div>
                        <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
                        <a href="detail.php"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/10.jpg" alt="Nikon D7200 DX-format DSLR Body (Black)" class="card-img-top"></a>
                        <div class="card-body">
                          <span class="price">$996.95</span>
                          <a href="detail.php" class="card-title h6">Nikon D7200 DX-format DSLR Body (Black)</a>
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="rating" data-value="5"></span>
                            <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2">
                      <div class="card card-product">
                        <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
                        <a href="detail.php"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/11.jpg" alt="Polk Audio PSW10 10-Inch Powered Subwoofer" class="card-img-top"></a>
                        <div class="card-body">
                          <span class="price">$99.99</span>
                          <a href="detail.php" class="card-title h6">Polk Audio PSW10 10-Inch Powered Subwoofer</a>
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="rating" data-value="4"></span>
                            <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2">
                      <div class="card card-product">
                        <div class="badge badge-danger badge-pill">Only 1 left in stock</div>
                        <button class="wishlist" title="Add tot wishlist"><i class="fa fa-heart"></i></button>
                        <a href="detail.php"><img src="<?php echo base_url(); ?>assets/assets-fontend/img/product/12.jpg" alt="Essential Phone in Halo Gray – 128 GB Unlocked" class="card-img-top"></a>
                        <div class="card-body">
                          <span class="price"><del class="small text-muted">$499.99</del> $435.00</span>
                          <a href="detail.php" class="card-title h6">Essential Phone in Halo Gray – 128 GB Unlocked</a>
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="rating" data-value="4"></span>
                            <button type="button" class="btn btn-outline-info btn-sm">Add to cart</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
            <a href="#" role="button" class="carousel-control-prev" id="popular-slider-prev"><i class="fa fa-angle-left fa-lg"></i></a>
            <a href="#" role="button" class="carousel-control-next" id="popular-slider-next"><i class="fa fa-angle-right fa-lg"></i></a>
          </div>
              <!-- /Similar Items -->

              <hr>
              <h3 id="reviews">Reviews</h3>
              <div class="media align-items-center mb-3">
                <h1 class="mb-0">4.5</h1>
                <div class="media-body ml-2">
                  <div class="rating" data-value="4.5"></div>
                  <div>(2 reviews)</div>
                </div>
              </div>
              <div class="media">
                <img src="<?php echo base_url(); ?>assets/assets-fontend/img/user.svg" width="50" height="50" alt="John Thor" class="rounded-circle">
                <div class="media-body ml-3">
                  <h5 class="mb-0">John Thor</h5>
                  <span class="rating text-secondary" data-value="4"></span>
                  <small class="ml-2">15/07/2018</small>
                  <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi</p>
                </div>
              </div>
              <div class="media">
                <img src="<?php echo base_url(); ?>assets/assets-fontend/img/user2.svg" width="50" height="50" alt="Michael Lelep" class="rounded-circle">
                <div class="media-body ml-3">
                  <h5 class="mb-0">Michael Lelep</h5>
                  <span class="rating text-secondary" data-value="5"></span>
                  <small class="ml-2">15/07/2018</small>
                  <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi</p>
                </div>
              </div>
              <div class="text-center">
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#reviewFormModal">Write a review</button>
              </div>
            </div>
          </div>

          <!-- Footer -->
          <?php $this->load->view('fontend/pages/footer-content'); ?>
          <!-- /Footer -->

        </div>