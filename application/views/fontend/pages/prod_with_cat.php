<?php
$ip = (isset($ip['ip']))?$ip['ip']:'';
$product_id = (isset($product->id))?$product->id:''; 

?>

        

        <div class="col" id="main-content">

         


          <div class="d-flex justify-content-between">
            <!-- Tags -->
            <div class="btn-tags">
            <?php foreach($sub_category as $value): ?>
              <a href="<?php echo base_url()?>fontend/product/<?php echo $this->uri->segment(3).'/'.$value->id; ?>" class="btn btn-light btn-sm active"><?php echo $value->name; ?></a>
            <?php endforeach; ?>  
            </div>
            <!-- /Tags -->


            <!-- Filter Modal Toggler -->
            <span>
              <button class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#filterModal"><i class="fa fa-filter"></i> FILTER</button>
            </span>
          </div>

          <!-- Grid -->
          
            <?php foreach($sub_category as $key => $value): if($key === 0): ?>
              <h3 class="title mt-4"><?php echo $value->name; ?></h3>
              <div class="row no-gutters gutters-2">
            <?php endif; endforeach; ?>

            <?php 
            $count_row = 0; 
            foreach($product as $value): 
                  foreach($value as $key=> $val): 
                  $count_row ++;  
            ?>
            <div class="col-6 col-md-3 mb-2">
              <div class="card card-product">
                <div class="ribbon"><span class="bg-info text-white">New</span></div>
                <button class="wishlist" title="Add to wishlist"><i class="fa fa-heart"></i></button>
                <a href="<?php echo base_url(); ?>/fontend/details/<?php echo $val->id; ?>"><img src="<?php echo base_url(); ?>assets/images/upload/small/<?php echo $val->path; ?>" data-progressive="<?php echo base_url(); ?>assets/images/upload/thrum/<?php echo $val->path; ?>" alt="NEW Microsoft Surface Go" class="card-img-top  progressive__img progressive--not-loaded"></a>
                <div class="card-body">
                  <span class="price">TK. <?php echo $val->base_price; ?></span>
                  <a href="<?php echo base_url(); ?>/fontend/details/<?php echo $val->id; ?>" class="card-title h6"><?php echo $val->prod_name; ?></a>
                  <div class="d-flex justify-content-between align-items-center">
                    <button plusMinus="plus" path="<?php echo base_url()?>addtocart/addProductToTemp" data-id="<?php echo $val->id; ?>" type="button" class="btn btn-outline-info btn-sm btn-block add-to-cart">Add to cart</button>
                  </div>
                </div>
              </div>
            </div>
            <?php 
            endforeach; 
            endforeach;
            
            ?>              



          </div>

            

          
          <!-- /Grid -->

          <!-- Pagination -->
          <?php 
            $page_number = ceil($count_row/$limit);
          ?>
          <br/> 
          <nav aria-label="Page navigation Shop Grid">
            <ul class="pagination justify-content-center">
              <li class="page-item disabled"><a class="page-link" href="<?php echo base_url(); ?>/fontend/details" tabindex="-1">Previous</a></li>
              <?php for($i=1; $i<=$page_number; $i++):?>
               <li class="page-item"><a class="page-link"
                href="<?php echo base_url()?>fontend/product_with_category/<?php echo $id;?>?page_id=<?php echo $i; ?>">
                <?php echo $i; ?></a></li>
              <?php endfor; ?>
              <li class="page-item">
                <a class="page-link" href="grid.html">Next</a>
              </li>
            </ul>
          </nav>

          <!-- /Pagination -->

          <!-- Footer -->
          <?php $this->load->view('fontend/pages/footer-content'); ?>
          <!-- /Footer -->

        </div>
     