<div class="col" id="main-content">

          <div class="card user-card">
            <div class="card-body">
              <div class="media">
                <img src="<?php echo base_url(); ?>assets/assets-fontend/img/user.svg" width="100" height="100" class="img-thumbnail rounded-circle" alt="John Thor">
                <div class="media-body ml-3 pt-4">
                  <h4>John Thor</h4>
                  <div class="small text-muted">Joined Dec 31, 2017</div>
                  <div class="small text-muted">Points: 100</div>
                </div>
              </div>
              <hr>
              <ul class="nav nav-pills">
                <li class="nav-item">
                  <a class="nav-link active" href="account-profile.html">Profile</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="account-order.html">Orders</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="account-address.html">Address</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="account-wishlist.html">Wishlist</a>
                </li>
              </ul>
              <hr>
              <form>
                <div class="form-row">
                  <div class="form-group col-sm-6">
                    <label for="profileFirstName">First Name</label>
                    <input type="text" class="form-control" id="profileFirstName" value="John">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="profileLastName">Last Name</label>
                    <input type="text" class="form-control" id="profileLastName" value="Thor">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="profileEmail">Email address</label>
                    <input type="email" class="form-control" id="profileEmail" value="john.thor@example.com">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="profilePhone">Phone Number</label>
                    <input type="number" class="form-control" id="profilePhone" value="123456789">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="profilePassword">Password</label>
                    <input type="password" class="form-control" id="profilePassword">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="profileConfirmPassword">Confirm Password</label>
                    <input type="password" class="form-control" id="profileConfirmPassword">
                  </div>
                  <div class="form-group col-12">
                    <button type="submit" class="btn btn-success btn-block">SAVE</button>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <!-- Footer -->
          <?php $this->load->view('fontend/pages/footer-content'); ?>
          <!-- /Footer -->

        </div>