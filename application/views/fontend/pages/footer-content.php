
<div class="navbar navbar-expand navbar-light navbar-footer">
  <a class="navbar-brand" href="#">Soroon</a>
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="#">About</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Privacy</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Terms</a>
    </li>
  </ul>
  <ul class="navbar-nav ml-auto">
    <li class="nav-item">
      <a class="nav-link" href="#"><i class="fa fa-question-circle"></i> Help</a>
    </li>
  </ul>
</div>