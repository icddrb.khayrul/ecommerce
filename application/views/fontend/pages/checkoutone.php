<div class="col" id="main-content">

          <div class="row">
            <div class="col-md-8">
              <h3 class="title"><i class="fa fa-credit-card"></i> Payment</h3>
              <span class="text-muted">Step 2 of 3</span>
              <hr>
              <div class="bg-light p-3 border shadow-sm">
                <ul class="nav nav-pills payment-nav mb-4" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="cc-tab" data-toggle="pill" href="#cc" role="tab" aria-controls="cc" aria-selected="true">
                      <i class="fa fa-fw fa-credit-card"></i> Cash on delivery
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="paypal-tab" data-toggle="pill" href="#paypal" role="tab" aria-controls="paypal" aria-selected="false">
                      <i class="fa fa-fw fa-paypal"></i> Pay with PayPal
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="redeem-tab" data-toggle="pill" href="#redeem" role="tab" aria-controls="redeem" aria-selected="false">
                      <i class="fa fa-fw fa-database"></i> Redeem Points
                    </a>
                  </li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane fade show active" id="cc" role="tabpanel" aria-labelledby="cc-tab">
                    <div class='card-wrapper mb-3'></div>
                    <form id="card">
                      <div class="form-row">
                        <img src="<?php echo base_url();?>assets/images/cash.jpg" alt="" width="100%" height="200px" srcset="">
                      </div>
                      <br/><br/>
                      <div class="form-row">
                        <div class="form-group col-sm-6">
                          <a href="<?php echo base_url()?>fontend/checkouttwo" class="btn btn-primary btn-block">NEXT <i class="fa fa-angle-right"></i></a>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="tab-pane fade" id="paypal" role="tabpanel" aria-labelledby="paypal-tab">
                    <form id="card" class="p-sm-5 mx-sm-5">
                      <h5>Log in to your PayPal account</h5>
                      <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                      </div>
                      <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                      </div>
                      <div class="form-group">
                        <a href="#" class="btn btn-primary">NEXT <i class="fa fa-angle-right"></i></a>
                      </div>
                    </form>
                  </div>
                  <div class="tab-pane fade" id="redeem" role="tabpanel" aria-labelledby="redeem-tab">
                    <div class="text-center">
                      <div class="alert alert-primary" role="alert">
                        You currently have <strong>9,386</strong> Points to spend.
                      </div>
                      <div class="custom-control custom-checkbox mb-3">
                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                        <label class="custom-control-label" for="customCheck1">Use my Points to pay for this order.</label>
                      </div>
                      <a href="#" class="btn btn-primary">NEXT <i class="fa fa-angle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 pt-5">
              <h4 class="title mb-3">Order summary</h4>
              <?php 
              $subtotal = 0;
              foreach($cart_item_list as $value):
                      $subtotal += $value->quantity*$value->base_price; 
              ?>
              <div class="media border-bottom mb-3">
                <img src="<?php echo base_url(); ?>assets/images/upload/thrum/<?php echo $value->path; ?>" width="50" height="50" alt="NEW Microsoft Surface Go">
                <div class="media-body ml-3">
                  <h6><?php echo $value->prod_name?></h6>
                  <div>1 x <span class="price">TK <?php echo $value->base_price?></span></div>
                </div>
              </div>
              <?php endforeach; ?>

              <table width="100%">
                  <tr>
                    <td class="pull-left" style="width:200px"><span>Items</span></td>
                    <td><span>TK</span></td>
                    <td class="pull-right"><span><?php echo number_format($subtotal ,2); ?></span></td>
                  </tr>
                  <tr>
                    <td class="pull-left"><span>Shipping</span></td>
                    <td><span>TK</span></td>
                    <td class="pull-right"><span><?php echo number_format(50 ,2); ?></span></span></td>
                  </tr>
              </table>
              <hr>
              <div class="box-total">
                  <h4>TOTAL</h4>
                  <h4><span class="price">TK <?php echo number_format($subtotal+50 ,2); ?></span></h4>
              </div>
            </div>
          </div>

          <!-- Footer -->
          <?php $this->load->view('fontend/pages/footer-content'); ?>
          <!-- /Footer -->

        </div>