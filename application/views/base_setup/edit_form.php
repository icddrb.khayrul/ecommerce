




                <form class="saveArea" method="POST" action="<?php echo base_url().'base_setup/updateEditForm?baseID='.$baseID ?>">
                    <input type="hidden" name="id" id="" class="form-control" value="<?php echo $id; ?>">

                    <?php if($chdData->mst_id == 6): ?>
                      <div class="row">
                          <div class="col-md-3">
                              <div class="input-group">
                                  <label for="percentage">Air</label>
                                  <input type="text" name="air" id="percentage" class="form-control numericPoint" value="<?php echo $chdData->fld_air; ?>">
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="input-group">
                                  <label for="percentage">Sea</label>


                                  <input type="text" name="sea" id="percentage" class="form-control numericPoint" value="<?php echo $chdData->fld_sea; ?>">
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="input-group">
                                  <label for="minimum">Land</label>
                                  <input type="text" name="land" id="minimum" class="form-control numericPoint" value="<?php echo $chdData->fld_land; ?>">
                              </div>
                          </div>
                       </div>
                    <?php endif;?>

                    <?php if($chdData->mst_id ==4):?>
                    <div class="row">
                          <div class="col-md-3">
                              <div class="input-group">
                                  <label for="Agent">Agent</label>
                                  <?php
                                    echo form_dropdown('agentid', $selectAgent,$chdData->agent_id, 'id=""  class="form-control agentClass"');
                                  ?>
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="input-group">
                                  <label for="percentage">Percentage</label>


                                  <input type="text" name="percentage" id="percentage" class="form-control numericPoint" value="<?php echo $chdData->percentage; ?>">
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="input-group">
                                  <label for="minimum">Minimum</label>
                                  <input type="text" name="minimum" id="minimum" class="form-control numericOnly" value="<?php echo $chdData->minimum; ?>">
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="input-group">
                                  <label for="maximum">Maximum</label>
                                  <input type="text" name="maximum" id="maximum" class="form-control numericOnly" value="<?php echo $chdData->maximum; ?>">
                              </div>
                          </div>
                    </div>
                <?php endif;?>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                              <label for="item">Item</label>
                              <input type="text" name="item" id=""  class="form-control col-md-12" value="<?php echo $chdData->name; ?>">
                          </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                              <label for="status">Status</label>
                              <div class="checkbox">
                                  <label><input type="checkbox" <?php if($chdData->is_active == 1){echo "checked";}?> name="active" value="1"> Is Active ?</label>
                              </div>
                          </div>
                        </div>
                    </div>
                    <br>
                    <input type="submit" class="btn btn-primary" value="Update">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </form>









