<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="row">
			<div class="col-xs-6 text-left header-margin ">
				<h3>
					<?php echo $pageTitle ?>
					<small>(Add, Edit)</small>
					<?php $baseID = $this->input->get('baseID',TRUE); ?>
				</h3>

			</div>
			<div class="col-xs-6 text-right">
				<div class="form-group">
					<a class="btn btn-primary" href="<?php echo base_url().'base_setup/addNew?baseID='.$baseID ?>">Add New</a>
				</div>
			</div>
		</div>
	</section>
	<section class="content content-margin">

		<?php
        foreach ($mstData as $key => $value) {
        ?>
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" href="#collapse<?php echo $key;?>">
							<?php echo $value->name; ?></a>

						<div style="float: right;">
							<button data-action="<?php echo base_url().'base_setup/addBaseForm?mst_id='.$value->mst_id.'&&baseID='.$baseID ?>"
							 data-toggle="tooltip" title="Add" modal-head="Add Base Setup Item" class="dynamicFormModal btn btn-sm btn-info"><span
								 class="glyphicon glyphicon-plus"></span></button>
						</div>
					</h4>
				</div>
				<div id="collapse<?php echo $key;?>" class="panel-collapse collapse">
					<div class="panel-body">
						<ol type="1" class="col-xs-12">

							<?php
                foreach ($chdData as $keys => $chd) {
                  if($chd->mst_id == $value->mst_id){
                ?>
							<li class="list-group-item list<?php echo $keys; ?>">
								<?php echo  $chd->name; ?>
							</li>
							<div style="position: relative; top: -34px; float: right">





								<button data-action="<?php echo base_url().'base_setup/editBaseForm?id='.$chd->chd_id.'&&baseID='.$baseID ?>"
								 data-toggle="tooltip" title="Edit" modal-head="Edit Base Setup Item" class="dynamicFormModal btn btn-sm btn-info"><span
									 class="glyphicon glyphicon-edit"></span></button>





								<div class="btn-group" data-toggle="buttons">
									<label class='btn btn-<?php if($chd->is_active==1){echo "success";}else{echo "warning";}?>'>
										<?php if($chd->is_active==1){echo "Active";}else{echo "Inactivate";}?>
									</label>
								</div>

							</div>

							<?php } } ?>
						</ol>


					</div>

				</div>
			</div>
		</div>

		<?php } ?>

	</section>
</div>


<script>
	//delete function
	$(document).on("click", ".deleteRow", function (e) {
		var self = $(this);
		var table = self.attr('tableName');
		var pk_row = self.attr('fieldName');
		var pk_id = self.attr('fieldId');

		if (typeof table != undefined) {
			var event = e.type;
			var element = $(this);
			var elementTagName = element.prop('tagName');
			var message = element.attr('data-confirm-message');

			swal({
				title: message,
				// text: 'You will not be able to recover this imaginary file!',
				type: 'warning',
				showCancelButton: true,
				// confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				closeOnConfirm: false
			}, function (isConfirm) {
				if (isConfirm != false) {
					//$(this).parent('.li').remove();
					//  console.log(da)
					$.ajax({
						type: "POST",
						url: "<?php echo base_url()?>base_setup/deleteRecourt?baseID=<?php echo $baseID; ?>",
						data: {
							table: table,
							pk_row: pk_row,
							pk_id: pk_id
						},
						success: function (data) {
							if (data) {
								console.log(data)
							} else {
								alert('Data delete failed!');
							}
						}
					});
				} else {
					return false;
				}
			});

		}
	});
</script>
