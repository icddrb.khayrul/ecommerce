<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

/**
 * Class : BaseController
 * Base Class to control over all the classes
 */
class BaseController extends CI_Controller {
	protected $role = '';
	protected $vendorId = '';
	protected $employeeid = '';
	protected $department = '';
	protected $job_title = '';
	protected $name = '';
	protected $email = '';
	protected $roleText = '';
	protected $agentID = 0;
	protected $global = array ();

	/**
	 * Takes mixed data and optionally a status code, then creates the response
	 *
	 * @access public
	 * @param array|NULL $data
	 *        	Data to output to the user
	 *        	running the script; otherwise, exit
	 */
	public function response($data = NULL) {
		$this->output->set_status_header ( 200 )->set_content_type ( 'application/json', 'utf-8' )->set_output ( json_encode ( $data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) )->_display ();
		exit ();
	}

	/**
	 * This function used to check the user is logged in or not
	 */
	function isLoggedIn() {
		$isLoggedIn = $this->session->userdata ('isLoggedIn' );

		if (! isset ( $isLoggedIn ) || $isLoggedIn != TRUE) {
			redirect ( 'login' );
		} else {
			$this->role = $this->session->userdata ('role');
			$this->vendorId = $this->session->userdata ('userId');
			$this->name = $this->session->userdata ('name');
			$this->roleText = $this->session->userdata ('roleText');
			$this->employeeid = $this->session->userdata ('employee_id');
			$this->department = $this->session->userdata ('department');
			$this->job_title = $this->session->userdata ('job_title');
			$this->extension = $this->session->userdata ('extension');
			$this->agentID = $this->session->userdata ('agent_id');
			$this->email = $this->session->userdata ('email');

			$this->global ['name'] = $this->name;
			$this->global ['role'] = $this->role;
			$this->global ['role_text'] = $this->roleText;
		}
	}

	/**
	 * This function is used to check the access
	 */
	function isAdmin() {
		if ($this->role != 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This function is used to check the access
	 */
	function isTicketter() {
		if ($this->role != 1 || $this->role != 2) {
			return true;
		} else {
			return false;
		}
	}

	function isUriString ()
	{
		return uri_string();
	}

	//permission check

	function loadThisForAccess($role_id, $menu_id,$menu_key)
	{


		$get_data = $this->db->get_where('tbl_user_role_menu', ['role_id' => $role_id, 'menu_id' => $menu_id]);
        $count = $get_data->num_rows();

		if ($count == 1)
		{

			$get_data_from_menu = $this->db->get_where('tbl_menu', ['status' => 1, 'id' => $menu_id, 'menu_key' => $menu_key]);
            $count_row = $get_data_from_menu->num_rows();
			if ($count_row == 1)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}


		}
		else
		{
			 return FALSE;
		}


	}

	/**
	 * This function is used to load the set of views
	 */
	function loadThis() {
		$this->global ['pageTitle'] = 'ecommerce : Access Denied';

		$this->load->view ( 'includes/header', $this->global );
		$this->load->view ( 'access' );
		$this->load->view ( 'includes/footer' );
	}

	/**
	 * This function is used to logged out user from system
	 */
	function logout() {
		$this->session->sess_destroy ();

		redirect ( 'login' );
	}

	/**
	 * This function used provide the pagination resources
	 * @param unknown $link
	 * @param number $count
	 * @return string[]|unknown[]
	 */
	function paginationCompress($link, $count, $perPage = 10) {
		$this->load->library ( 'pagination' );

		$config ['base_url'] = base_url () . $link;
		$config ['total_rows'] = $count;
		$config ['uri_segment'] = SEGMENT;
		$config ['per_page'] = $perPage;
		$config ['num_links'] = 5;
		$config ['full_tag_open'] = '<nav><ul class="pagination">';
		$config ['full_tag_close'] = '</ul></nav>';
		$config ['first_tag_open'] = '<li class="arrow">';
		$config ['first_link'] = 'First';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="arrow">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li class="arrow">';
		$config ['next_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li class="arrow">';
		$config ['last_link'] = 'Last';
		$config ['last_tag_close'] = '</li>';

		$this->pagination->initialize ( $config );
		$page = $config ['per_page'];
		$segment = $this->uri->segment ( SEGMENT );

		return array (
				"page" => $page,
				"segment" => $segment
		);
	}
}