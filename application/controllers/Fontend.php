<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

//require APPPATH . '/libraries/BaseController.php';

class Fontend extends ci_controller
{
    /**
     * This is default constructor of the class
     */
	public $controller = "fontend";
	public $pageTitle = 'fontend Management';
	public $pageShortName = 'Fontend';
	
    public function __construct()
    {
        parent::__construct();

                $this->load->model('fontend_model');
                $this->load->model('pagination_model');
                $this->load->model('utilities');
                $this->load->model('ip_model');

                $menu_key = 'fontend';
                
   
    }


    


    public function menu_load()
    {
        $ip = $this->ip_model->identify_to_user();
        $sidebar['prod_count'] = $this->fontend_model->count_product($ip);    
        $sidebar['categoryTop'] = $this->fontend_model->selectCategoryTopThree();  
        $sidebar['categoryMore'] = $this->fontend_model->selectCategoryMore();
        return $sidebar;
    }




    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $limit = 4;    
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['category'] = $this->fontend_model->getAllCategoryWithImage();
            $data['product'] = $this->fontend_model->selectAllChoiceProduct($limit);
            //$this->pr($data);
            $data['main_content'] ='fontend/pages/home'; 
            $this->load->view('fontend/include/index',$data);
    }

    


    public function product_with_category($id){

        $limit = 10;
        $data['sidebar_content'] = $this->menu_load();
        $data['sub_category'] = $this->fontend_model->selectAllSubCategoryById($id);  
        $data['product'] = $this->fontend_model->selectAllProductById($id, $limit);
        $data['limit'] = $limit;
        $data['id'] = $id;
        $data['user_uniq_id'] = $this->ip_model->identify_to_user();
        //$this->pr($data);

        $data['main_content'] ='fontend/pages/prod_with_cat'; 
        $this->load->view('fontend/include/index',$data);
    }


    public function product($cat_id, $id){

        $limit = 10;

        $data['sidebar_content'] = $this->menu_load();
        $data['sub_category'] = $this->fontend_model->selectAllSubCategoryById($cat_id); 
        $data['product'] = $this->fontend_model->selectAllProdByCatId($id, $limit); 
        $data['limit'] = $limit;
        $data['cat_id'] = $cat_id;
        $data['id'] = $id;
        $data['user_uniq_id'] = $this->ip_model->identify_to_user();

        $data['main_content'] ='fontend/pages/prod'; 
        $this->load->view('fontend/include/index',$data);
    }



    
    public function category()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['category'] = $this->fontend_model->getAllCategoryWithImage();
            $data['main_content'] ='fontend/pages/category'; 
            $this->load->view('fontend/include/index',$data);
    }



    public function grid()
    {
            $limit = 10;
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['product'] = $this->fontend_model->selectAllChoiceProduct($limit);

            //$this->pr($data);

            $data['main_content'] ='fontend/pages/grid'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function aboutus()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['main_content'] ='fontend/pages/aboutus'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function contractus()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['main_content'] ='fontend/pages/contractus'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function error()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['main_content'] ='fontend/pages/error'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function checkout()
    {
            $ip = $this->ip_model->identify_to_user();
            $where = array('ip'=>$ip);
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['cart_item_list'] = $this->fontend_model->getCartItemList($ip);
            $data['customer_data'] = $this->utilities->findByAttribute('tbl_customers',$where);
            //$this->pr($data);
            $data['main_content'] ='fontend/pages/checkout'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function checkoutone()
    {
            $ip = $this->ip_model->identify_to_user();
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['cart_item_list'] = $this->fontend_model->getCartItemList($ip);
            $data['main_content'] ='fontend/pages/checkoutone'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function checkouttwo()
    {
            $ip = $this->ip_model->identify_to_user();
            $where = array('ip'=>$ip);
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['cart_item_list'] = $this->fontend_model->getCartItemList($ip);
            $data['customer_info'] = $this->utilities->findByAttribute('tbl_customers',$where);
            //$this->pr($data);
            $data['main_content'] = 'fontend/pages/checkouttwo'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function cart($id = NULL)
    {
            $ip = $this->ip_model->identify_to_user();
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['cart_item_list'] = $this->fontend_model->getCartItemList($ip);
            //$this->pr($data);
            $data['main_content'] ='fontend/pages/cart'; 
            $this->load->view('fontend/include/index',$data);
    }

    



    public function compare()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['main_content'] ='fontend/pages/compare'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function faq()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['main_content'] ='fontend/pages/faq'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function profile()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['main_content'] ='fontend/pages/profile'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function order()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['main_content'] ='fontend/pages/order'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function address()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['main_content'] ='fontend/pages/address'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function wishlist()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['main_content'] ='fontend/pages/wishlist'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function login()
    {
            $data['title'] = 'Home Page';
            $data['sidebar_content'] = $this->menu_load();
            $data['main_content'] ='fontend/pages/login'; 
            $this->load->view('fontend/include/index',$data);
    }


    public function details($id)
    {
        $data['title'] = 'Home Page';
        $data['sidebar_content'] = $this->menu_load();
        $data['main_content'] ='fontend/pages/details'; 
        $data['product'] = $this->fontend_model->selectDetailProduct($id);
        $data['ip'] = $this->ip_model->identify_to_user();
        //$this->pr($data);
        $this->load->view('fontend/include/index',$data);
    }


    public function checkUsernameExist()
    {
            $mobile = $this->input->post('username', TRUE);
            $where = array('mobile'=>$mobile);
            $exist = $this->utilities->findByAttribute('tbl_customers',$where);
            $return = 0;   
            if(!empty($exist)){
                    $return = 2;
            }else{
                    $return = 1;
            }
            echo $return;
    }


    public function save_customer()
    {
        $ip = $this->ip_model->identify_to_user();    
        $name = $this->input->post('name', TRUE);
        $phone = $this->input->post('phone', TRUE);
        $email = $this->input->post('email', TRUE);
        $address = $this->input->post('address', TRUE);

        $data = array(
                'ip' => $ip,
                'name' => $name,
                'mobile' => $phone,
                'email' => $email,
                'address' => $address,
                'createdDtm'=> date('Y-m-d H:i:s'),
                'createdBy' => 1
        );

        $where = array('ip'=>$ip);
        $exist = $this->utilities->findByAttribute('tbl_customers',$where);
        if(!empty($exist)){
            $this->utilities->updateData('tbl_customers', $data, $where);
        }else{
            $this->utilities->insertData($data, 'tbl_customers');
        }

        redirect('fontend/checkoutone');
    }



    public function save_order()
    {
            $quantity = $this->input->post('quantity', TRUE);
            $prod_id = $this->input->post('prod_id', TRUE);
            $customer_id = $this->input->post('customer_id', TRUE);
            $total_price = $this->input->post('total', TRUE);
            $delivery_charge = $this->input->post('delivery_charge', TRUE);

            if(isset($_POST)):

                $mst_data = array(
                        'customer_id' => $customer_id,
                        'total_price' => $total_price,
                        'delivery_charges' => $delivery_charge,
                        'order_status' => 1,
                        'createdDtm' => date('Y-m-d H:i:s'),
                        'createdBy' => 1

                );

               $mst_id = $this->utilities->insert('tbl_order_mst', $mst_data);
               $limitCount = (isset($prod_id))?count($prod_id):0;
               if(isset($mst_id)):
                     for( $i=0; $i< $limitCount; $i++):
                        $chd_data = array(
                                'order_id' => $mst_id,
                                'prod_id' => $prod_id[$i],
                                'quantity' => $quantity[$i],
                                'createdDtm' => date('Y-m-d H:i:s'),
                                'createdBy' => 1
                        );
                        $this->utilities->insertData($chd_data,'tbl_order_chd');
                     endfor;  
               endif;

            endif;  
            
            $ip_addr = $this->ip_model->identify_to_user();
            $where = array('user_uniq_id'=>$ip_addr);
            $user_id = $this->utilities->findByAttribute('tbl_cart_temp_mst',$where)->id;
            $deleteWhereMst = array('id'=>$user_id);
            $deleteWhereChd = array('user_uniq_id'=>$user_id);
            $this->utilities->deleteRowByAttribute('tbl_cart_temp_mst', $deleteWhereMst);
            $this->utilities->deleteRowByAttribute('tbl_cart_temp_chd', $deleteWhereMst);
            
            $cookie_check = $this->unset_cookie();

            if($cookie_check){
                redirect('fontend');
            }


    }


    public function unset_cookie()
    {
           if (isset($_COOKIE['user_id'])) {
                unset($_COOKIE['user_id']);
                setcookie("user_id", "", time()-3600, "/");
                return true;
            } else {
                return false;
            }
    }


    


    public function pr($data)
    {

            echo '<pre>';
            print_r($data);
            exit;
    }

    
    
    
}

