<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Dashboard extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
		$this->load->model('menu_model');
		$this->load->model('utilities');
        $this->isLoggedIn();

         $menu_key = 'home';

		 $baseID = $this->input->get('baseID', TRUE);

		 $baseID = isset($baseID) ?  1 : $baseID  ;


		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true)
		 {
			 redirect('access');
		 }

    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
		// print_r($list); die();
		$baseID = $this->input->get('baseID', TRUE);
		$this->load->model('menu_model');

		$this->role;
        $this->global['menu'] =  $this->menu_model->getMenu($this->role);

        $this->global['pageTitle'] = 'ecommerce : Dashboard';
        //echo CI_VERSION;
        if($this->role == 8){
                
            $data = array();
        	$this->load->view('includes/header', $this->global);
        	$this->load->view('dashboard_agent',$data);
        }else{
            
            $data = array();
        	$this->load->view('includes/header', $this->global);
            $this->load->view('dashboard_user',$data);
        }



        //$this->pr($data);


        $this->load->view('includes/footer');
    }

	function logout() {
		$this->session->sess_destroy ();

		redirect ( 'login' );
	}



	public function pr($data)
    {

    		echo '<pre>';
            print_r($data);
            exit;
    }


}

?>