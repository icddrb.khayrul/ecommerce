<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Location extends BaseController
{
    

    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('location_model');
		$this->load->library('pagination');
        $this->isLoggedIn(); 
		 $menu_key = 'loc';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true) 
		 {
			 redirect('access');
		 }
			
    }
    
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
	        $this->global['pageTitle'] = 'ecommerce : Location List';
            $data['userRecords'] = $this->location_model->locationListing();
		    $this->load->view('includes/header', $this->global);
			$this->load->view('includes/script');
			$this->load->view('location/index', $data);
			$this->load->view('includes/footer');				
    }

    

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
      
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $this->global['pageTitle'] = 'ecommerce : Add New Location';
            $this->load->view('includes/header', $this->global);
            $this->load->view('location/addNew');
            $this->load->view('includes/footer');    
    }

    
    /**
     * This function is used to add new user to the system
     */
    function addNewLocation()
    {
          
			$this->load->library('form_validation');        
            // $this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('id_name','ID Name','trim|required|max_length[255]|xss_clean');
              
            $baseID = $this->input->get('baseID', TRUE);
         
           if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
				
                $id_name = $this->input->post('id_name');
                $active = $this->input->post('active');
				 
				$IdInfo = array('name'=>$id_name, 'active'=>$active, 'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:s'));
					
				$result = $this->location_model->addNewLocation($IdInfo);
				
				if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Location created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Location creation failed');
                }
				
					
                redirect('location?baseID='.$baseID);
            }
        
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($id = NULL)
    {
			$baseID = $this->input->get('baseID', TRUE);
			
            if($id == null)
            {
                redirect('location/index?baseID='.$baseID);
            }
            
            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->location_model->getLocationInfo($id);
			
			//$data['unit'] = $this->item_model->getUnits();
            
            $this->global['pageTitle'] = 'ecommerce : Edit Location';
            $this->load->view('includes/header', $this->global);
            $this->load->view('location/editOld', $data);
            $this->load->view('includes/footer');
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editLocation()
    {
            $this->load->library('form_validation');
            $id = $this->input->post('id');
			$baseID = $this->input->get('baseID', TRUE);
			//print_r($unit_name); die();
            //$this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('id_name','ID Name','trim|required|max_length[255]|xss_clean');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($id);
            }
            else
            {
                
			    $name = $this->input->post('id_name');
                $active = $this->input->post('active');
                $IDInfo = array();
                $IDInfo = array( 'name'=> $name,
										'active'=>$active,'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));
                $result = $this->location_model->editLocation($IDInfo, $id);
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Location updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Location update failed');
                }
                redirect('location?baseID='.$baseID);
            }
        
    }
    
    
}

?>