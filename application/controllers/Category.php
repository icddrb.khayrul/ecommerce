<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';




class Category extends BaseController
{

    /**
     * This is default constructor of the class
     */
	public $controller = "category";
	public $pageTitle = 'Category Management';
	public $pageShortName = 'Category';
	
    public function __construct()
    {
        parent::__construct();
		$this->load->model('agent_model');
        $this->load->library('pagination');
        $this->load->model('utilities');
        $this->isLoggedIn(); 
		 $menu_key = 'category';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true) 
		 {
			 redirect('access');
		 }
		 
			
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
			
	        $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = 'List';
			
            $data['result'] = $this->utilities->treeViewData('tbl_category_list');

            //$this->pr($data);
			
		    $this->load->view('includes/header', $this->global);
			$this->load->view($this->controller.'/index', $data);
			$this->load->view('includes/footer');
			
		
		
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
       
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewCat';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;			
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addNew',$data);
            $this->load->view('includes/footer');
        
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewCat()
    {
          
			$this->load->library('form_validation');
            
             // $this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|   xss_clean');

            $this->form_validation->set_rules('name','Name','trim|required|max_length[255]|xss_clean');
            
			
            $baseID = $this->input->get('baseID', TRUE);
           
                //    if($this->form_validation->run() == FALSE)
                //     {
                //         $this->addNew();
                //     }
                //     else
                //     {
				
                $name = $this->input->post('name');
                $href = $this->input->post('href');
                $parent_id = $this->input->post('parent_id');
                $level = $this->input->post('level');
                
                $active = $this->input->post('active');
				
				
				 
				 
				$IdInfo = array('name'=>$name,'url'=>$href, 'parent_id'=>$parent_id,'level'=>$level, 
				'active'=>$active, 'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:s'));
					
				$result = $this->utilities->insertData($IdInfo,'tbl_category_list');
				
				if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Category created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category creation failed');
                }
				
					
                redirect($this->controller.'?baseID='.$baseID);
                
                    //}
        
    }




    /**
     * This function is used to add new user to the system
     */
    function addSubCat($id)
    {
          
			$baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewCat';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;	
            $data['parent_id']	= $id;	
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addSubCat',$data);
            $this->load->view('includes/footer');
        
    }


    /**
     * This function is used to add new user to the system
     */
    function addSubSubCat($id)
    {
          
			$baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewCat';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;	
            $data['parent_id']	= $id;	
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addSubSubCat',$data);
            $this->load->view('includes/footer');
        
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editcat($id = NULL)
    {
			$baseID = $this->input->get('baseID', TRUE);
			
            if($id == null)
            {
                redirect($this->controller.'?baseID='.$baseID);
            }
            
            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->agent_model->getCatInfo($id);
			
			$this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'updatecat';
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = $this->pageShortName .' Details';
			
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/editcat', $data);
            $this->load->view('includes/footer');
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function updatecat()
    {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
			
			 $baseID = $this->input->get('baseID', TRUE);
			 
			 //print_r($unit_name); die();
            		
			//$this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('name','Agent Name','trim|required|max_length[255]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editcat($id);
            }
            else
            {
                
			    $name = $this->input->post('name');
                
                $active = $this->input->post('active');
                
                $IDInfo = array();
                
               	
                $IDInfo = array( 'name'=>$name,
				'active'=>$active,'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));
                
                $result = $this->utilities->updateData('tbl_category_list',$IDInfo, array('id'=>$id));
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Category updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category  update failed');
                }
                
				
				redirect($this->controller.'?baseID='.$baseID);
            }
        
    }




    public function deletecat($id = NULL){
        $baseID = $this->input->get('baseID', TRUE);
        $getItem  = $this->agent_model->getDeleteList($id);

        if(!empty($getItem)){

            foreach($getItem as $va){
                $this->utilities->deleteRowByAttribute('tbl_category_list', array('id'=>$va->id));
            }
        }

        $this->utilities->deleteRowByAttribute('tbl_category_list', array('id'=>$id));

        redirect($this->controller.'?baseID='.$baseID);
    }


    public function pr($data)
    {

            echo '<pre>';
            print_r($data);
            exit;
    }


    
    
}

?>