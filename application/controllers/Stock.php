<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';




class Stock extends BaseController
{

    /**
     * This is default constructor of the class
     */
	public $controller = "stock";
	public $pageTitle = 'Stock Management';
	public $pageShortName = 'Stock';
	
    public function __construct()
    {
        parent::__construct();
		$this->load->model('utilities');
        $this->load->model('stock_model');
        $this->load->model('product_model');
        $this->isLoggedIn(); 
		 $menu_key = 'stock';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true) 
		 {
			 redirect('access');
		 }
		 
			
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
			
	        $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = 'List';
			
            $data['result'] = $this->stock_model->getAllProduct();

            //$this->pr($data);
			
		    $this->load->view('includes/header', $this->global);
			$this->load->view($this->controller.'/index', $data);
			$this->load->view('includes/footer');
			
		
		
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
            $this->load->helper('form');
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewStock';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;		
            
            
            $data['batch'] = $this->stock_model->dropdownFromBatchWithCondition();

            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addNew',$data);
            $this->load->view('includes/footer');
        
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewStock()
    {
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('batch_id','Batch Number','trim|required|max_length[255]|xss_clean');
        $this->form_validation->set_rules('quantity','Quantity','trim|required|max_length[255]|xss_clean');

        if($this->form_validation->run() == FALSE):

            $this->addNew();    

        else:    
        $baseID = $this->input->get('baseID', TRUE);

        $batch_id = $this->input->post('batch_id');
        $quantity = $this->input->post('quantity');
        $active = $this->input->post('active');
        
        $IdInfo = array('batch_id'=>$batch_id, 'quantity'=>$quantity, 'active'=>$active, 'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:s'));

        $result = $this->utilities->insertData($IdInfo,'tbl_stock_list');

        if($result > 0)
        {
            $this->session->set_flashdata('success', 'New Stock created successfully');
        }
        else
        {
            $this->session->set_flashdata('error', 'Stock creation failed');
        }
        redirect($this->controller.'?baseID='.$baseID);

        endif;
                
    }




    public function editOld($id = NULL){
            $this->load->helper('form');
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'updateStock';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;			

            $data['batch'] = $this->stock_model->dropdownFromBatchWith();

            $data['selectAll'] = $this->utilities->findResultByAttribute('tbl_stock_list', array('id'=>$id));
            //$this->pr($data);
            
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/editStock',$data);
            $this->load->view('includes/footer');
        

    }




    function updateStock()
    {
        
        $baseID = $this->input->get('baseID', TRUE);
        

        $batch_id = $this->input->post('batch_id');
        
        $quantity = $this->input->post('quantity');
        $qnt_less = $this->input->post('qnt_less');
        $qnt_damage = $this->input->post('qnt_damage');
        $qnt_return = $this->input->post('qnt_return');
        $active = $this->input->post('active');

        $id = $this->input->post('existId');

       
        
        
        $IdInfo = array('batch_id'=>$batch_id, 'quantity'=>$quantity, 'qnt_less'=>$qnt_less, 'qnt_damage'=>$qnt_damage, 'qnt_return'=>$qnt_return, 'active'=>$active, 'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));

        $result = $this->utilities->updateData('tbl_stock_list', $IdInfo, array('id'=>$id));


        if($result > 0)
        {
            $this->session->set_flashdata('success', 'Stock Updated successfully');
        }
        else
        {
            $this->session->set_flashdata('error', 'Stock    Updated failed');
        }
        redirect($this->controller.'?baseID='.$baseID);
                
    }





    public function docFileUpload() {
		if(isset($_FILES['fileUpload'])){
			$IMAGE_NAME = '';
			$errors= array();
			$file_name = $_FILES['fileUpload']['name'];
			$file_size =$_FILES['fileUpload']['size'];
			$file_tmp =$_FILES['fileUpload']['tmp_name'];
			$file_type=$_FILES['fileUpload']['type'];
			$tmp = explode('.', $file_name);
			$file_ext = end($tmp);

			$extensions= array("jpeg","jpg","png","pdf","doc","docx");
			
			if(in_array($file_ext,$extensions)=== false){
			   $errors[]="extension not allowed, please choose a JPEG or PNG file.";
			}
			
			if($file_size > 2097152){
			   $errors[]='File size must be extensions 2 MB';
			}
			
			$targetDir = "assets/images/upload";

            $imageNewName = time();

			if(empty($errors)==true){
			   move_uploaded_file($file_tmp, $targetDir.'/'.$imageNewName);
			$IMAGE_NAME =  $imageNewName;
			}else{
			$IMAGE_NAME =  'NOT_FOUND';
			}

			return $IMAGE_NAME;
		 }
		
	 }




    /**
     * This function is used to add new user to the system
     */
    function addSubCat($id)
    {
          
			$baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewCat';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;	
            $data['parent_id']	= $id;	
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addSubCat',$data);
            $this->load->view('includes/footer');
        
    }


    /**
     * This function is used to add new user to the system
     */
    function addSubSubCat($id)
    {
          
			$baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewCat';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;	
            $data['parent_id']	= $id;	
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addSubSubCat',$data);
            $this->load->view('includes/footer');
        
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editcat($id = NULL)
    {
			$baseID = $this->input->get('baseID', TRUE);
			
            if($id == null)
            {
                redirect($this->controller.'?baseID='.$baseID);
            }
            
            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->agent_model->getCatInfo($id);
			
			$this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'updatecat';
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = $this->pageShortName .' Details';
			
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/editcat', $data);
            $this->load->view('includes/footer');
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function updatecat()
    {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
			
			 $baseID = $this->input->get('baseID', TRUE);
			 
			 //print_r($unit_name); die();
            		
			//$this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('name','Agent Name','trim|required|max_length[255]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editcat($id);
            }
            else
            {
                
			    $name = $this->input->post('name');
                
                $active = $this->input->post('active');
                
                $IDInfo = array();
                
               	
                $IDInfo = array( 'name'=>$name,
				'active'=>$active,'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));
                
                $result = $this->utilities->updateData('tbl_category_list',$IDInfo, array('id'=>$id));
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Category updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category  update failed');
                }
                
				
				redirect($this->controller.'?baseID='.$baseID);
            }
        
    }




    public function deletecat($id = NULL){
        $baseID = $this->input->get('baseID', TRUE);
        $getItem  = $this->agent_model->getDeleteList($id);

        if(!empty($getItem)){

            foreach($getItem as $va){
                $this->utilities->deleteRowByAttribute('tbl_category_list', array('id'=>$va->id));
            }
        }

        $this->utilities->deleteRowByAttribute('tbl_category_list', array('id'=>$id));

        redirect($this->controller.'?baseID='.$baseID);
    }


    public function pr($data)
    {

            echo '<pre>';
            print_r($data);
            exit;
    }


    
    
}

