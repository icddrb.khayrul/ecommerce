<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';




class Order extends BaseController
{

    /**
     * This is default constructor of the class
     */
	public $controller = "order";
	public $pageTitle = 'Order Management';
	public $pageShortName = 'Order';
	
    public function __construct()
    {
        parent::__construct();
		$this->load->model('order_model');
        $this->load->library('pagination');
        $this->load->model('utilities');
        $this->isLoggedIn(); 
		 $menu_key = 'order';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true) 
		 {
			 redirect('access');
		 }
		 
			
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
			
	        $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = 'List';
            $data['result'] = $this->order_model->getAllOrder();
			$data['baseID'] = $baseID;

            //$this->pr($data);
			
		    $this->load->view('includes/header', $this->global);
			$this->load->view($this->controller.'/index', $data);
			$this->load->view('includes/footer');
			
		
		
    }

    public function view_order($id)
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
            $data['approved'] = 'Approved';
            $data['reject'] = 'Reject';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['customer'] = $this->order_model->getOrderById($id);
            
            $data['cart_item_list'] = $this->order_model->getOrderItemList($id);
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/view_order',$data);
            $this->load->view('includes/footer');
    }

    public function pr($data)
    {

            echo '<pre>';
            print_r($data);
            exit;
    }


    
    
}

?>