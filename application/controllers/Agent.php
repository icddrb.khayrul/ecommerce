<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Agent extends BaseController
{
    /**
     * This is default constructor of the class
     */
	public $controller = "agent";
	public $pageTitle = 'Agent Management';
	public $pageShortName = 'Agent';
	
    public function __construct()
    {
        parent::__construct();
		$this->load->model('agent_model');
		$this->load->library('pagination');
        $this->isLoggedIn(); 
		 $menu_key = 'agnt';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true) 
		 {
			 redirect('access');
		 }
		 
			
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
			
	        $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = 'List';
			
            $data['userRecords'] = $this->agent_model->agentListing();
			
		    $this->load->view('includes/header', $this->global);
			$this->load->view($this->controller.'/index', $data);
			$this->load->view('includes/footer');
			
		
		
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
       
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addAgent';
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = $this->pageShortName .' Details';
			
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addNew',$data);
            $this->load->view('includes/footer');
        
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addAgent()
    {
          
			$this->load->library('form_validation');
            
           // $this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('name','Agent Name','trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('registration_no','registration_no','trim|xss_clean');
            $this->form_validation->set_rules('address','address','trim|xss_clean');
            $this->form_validation->set_rules('mobile','mobile','trim|xss_clean');
            $this->form_validation->set_rules('phone','phone','trim|xss_clean');
            $this->form_validation->set_rules('email','email','trim|xss_clean');
            $this->form_validation->set_rules('remarks','remarks','trim|xss_clean');
			
			//registration_no, address, mobile, phone, email, remarks
              
            $baseID = $this->input->get('baseID', TRUE);
           
		   if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
				
                $name = $this->input->post('name');
                $address = $this->input->post('address');
                $registration_no = $this->input->post('registration_no');
                $mobile = $this->input->post('mobile');
                $phone = $this->input->post('phone');
                $email = $this->input->post('email');
                $remarks = $this->input->post('remarks');
				
                $active = $this->input->post('active');
				
				
				 
				 
				$IdInfo = array('name'=>$name,'address'=>$address, 'registration_no'=>$registration_no,'mobile'=>$mobile, 
				'phone'=>$phone, 'email'=>$email,'remarks'=>$remarks,
				'active'=>$active, 'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:s'));
					
				$result = $this->agent_model->addNewAgent($IdInfo);
				
				if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Agent created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Agent creation failed');
                }
				
					
                redirect($this->controller.'?baseID='.$baseID);
            }
        
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($id = NULL)
    {
			$baseID = $this->input->get('baseID', TRUE);
			
            if($id == null)
            {
                redirect($this->controller.'?baseID='.$baseID);
            }
            
            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->agent_model->getAgentInfo($id);
			
			$this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'editAgent';
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = $this->pageShortName .' Details';
			
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/editOld', $data);
            $this->load->view('includes/footer');
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editAgent()
    {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
			
			 $baseID = $this->input->get('baseID', TRUE);
			 
			 //print_r($unit_name); die();
            		
			//$this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('name','Agent Name','trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('registration_no','registration_no','trim|xss_clean');
            $this->form_validation->set_rules('address','address','trim|xss_clean');
            $this->form_validation->set_rules('mobile','mobile','trim|xss_clean');
            $this->form_validation->set_rules('phone','phone','trim|xss_clean');
            $this->form_validation->set_rules('email','email','trim|xss_clean');
            $this->form_validation->set_rules('remarks','remarks','trim|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($id);
            }
            else
            {
                
			    $name = $this->input->post('name');
                $address = $this->input->post('address');
                $registration_no = $this->input->post('registration_no');
                $mobile = $this->input->post('mobile');
                $phone = $this->input->post('phone');
                $email = $this->input->post('email');
                $remarks = $this->input->post('remarks');
				
                $active = $this->input->post('active');
                
                $IDInfo = array();
                
               	
                $IDInfo = array( 'name'=>$name,'address'=>$address, 'registration_no'=>$registration_no,'mobile'=>$mobile, 
				'phone'=>$phone, 'email'=>$email,'remarks'=>$remarks,
				'active'=>$active,'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));
                
                $result = $this->agent_model->editAgent($IDInfo, $id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Agent updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Agent  update failed');
                }
                
				
				redirect($this->controller.'?baseID='.$baseID);
            }
        
    }


    
    
}

?>