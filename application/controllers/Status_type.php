<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Status_type extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('status_model');
		$this->load->library('pagination');
        $this->isLoggedIn(); 
		 $menu_key = 'st';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true) 
		 {
			 redirect('access');
		 }
			
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
	        $this->global['pageTitle'] = 'ecommerce : Status Type List';
            $data['userRecords'] = $this->status_model->statusListing();
			
		    $this->load->view('includes/header', $this->global);
			$this->load->view('status_type/index', $data);
			$this->load->view('includes/footer');
			
		
		
    }
    

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
      
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $this->global['pageTitle'] = 'ecommerce : Add New Status Type';
            $this->load->view('includes/header', $this->global);
            $this->load->view('status_type/addNew');
            $this->load->view('includes/footer');
        
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewStatus()
    {
          
			$this->load->library('form_validation');
            
           // $this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('id_name','ID Name','trim|required|max_length[255]|xss_clean');
              
            $baseID = $this->input->get('baseID', TRUE);
           
		   if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
				
                $id_name = $this->input->post('id_name');
                $active = $this->input->post('active');
				
				
				 
				 
				$IdInfo = array('name'=>$id_name, 'active'=>$active, 'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:sa'));
					
				$result = $this->status_model->addNewStatus($IdInfo);
				
				if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Status Type created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Status Type creation failed');
                }
				
					
                redirect('status_type?baseID='.$baseID);
            }
        
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($id = NULL)
    {
			$baseID = $this->input->get('baseID', TRUE);
			
            if($id == null)
            {
                redirect('status_type/index?baseID='.$baseID);
            }
            
            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->status_model->getStatusInfo($id);
			
			//$data['unit'] = $this->item_model->getUnits();
            
            $this->global['pageTitle'] = 'ecommerce : Edit Status Type';
            $this->load->view('includes/header', $this->global);
            $this->load->view('status_type/editOld', $data);
            $this->load->view('includes/footer');
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editStatus()
    {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
			
			 $baseID = $this->input->get('baseID', TRUE);
			 
			 //print_r($unit_name); die();
            		
			//$this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('id_name','ID Name','trim|required|max_length[255]|xss_clean');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($id);
            }
            else
            {
                
			    $name = $this->input->post('id_name');
                $active = $this->input->post('active');
                
                $IDInfo = array();
                
               		
                $IDInfo = array( 'name'=> $name,
										'active'=>$active,'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:sa'));
                
                $result = $this->status_model->editStatus($IDInfo, $id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Status Type updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Status Type  update failed');
                }
                
				
				redirect('status_type?baseID='.$baseID);
            }
        
    }
    
    
}

?>