<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Api extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('variance_model');
        $this->isLoggedIn();
    }



	public function getVariance($id) {
       $result = $this->db->where("progress_id",$id)->get("tbl_progress_variance_type")->result();
       echo json_encode($result);
   }


  public function getDocument($id)
  {
      $result = $this->db->query("SELECT a.shipment_type, a.document_date,
      (SELECT b.name FROM tbl_pi_list b WHERE b.id = a.pi_id) pi_name,
      (SELECT c.name FROM tbl_agent_list c WHERE c.id = a.agent_id) agent_name
      FROM tbl_documents a WHERE a.id = $id")->row();

      echo json_encode($result);
  }

}