<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Frowarder extends BaseController
{
    /**
     * This is default constructor of the class
     */
	public $controller = "frowarder";
	public $pageTitle = 'Frowarder Management';
	public $pageShortName = 'Frowarder';
	
    public function __construct()
    {
        parent::__construct();
		$this->load->model('frowarder_model');
		$this->load->library('pagination');
        $this->isLoggedIn(); 
		 $menu_key = 'frowar';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true) 
		 {
			 redirect('access');
		 }
		 
			
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
			
	        $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = 'List';
			
            $data['userRecords'] = $this->frowarder_model->frowarderListing();
			
		    $this->load->view('includes/header', $this->global);
			$this->load->view($this->controller.'/index', $data);
			$this->load->view('includes/footer');
			
		
		
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
       
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addFrowarder';
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = $this->pageShortName .' Details';
			
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addNew',$data);
            $this->load->view('includes/footer');
        
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addFrowarder()
    {
          
			$this->load->library('form_validation');
            
           // $this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('id_name','Shipper Name','trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('address','address','trim|xss_clean');
              
            $baseID = $this->input->get('baseID', TRUE);
           
		   if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
				
                $id_name = $this->input->post('id_name');
                $address = $this->input->post('address');
                $active = $this->input->post('active');
				$shipment_mode = $this->input->post('shipment_mode', true);
				
				
				 
				 
				$IdInfo = array('name'=>$id_name,'address'=>$address, 'active'=>$active, 'shipment_mode'=>$shipment_mode, 'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:s'));
					
				$result = $this->frowarder_model->addNewFrowarder($IdInfo);
				
				if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Frowarder created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Frowarder creation failed');
                }
				
					
                redirect($this->controller.'?baseID='.$baseID);
            }
        
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($id = NULL)
    {
			$baseID = $this->input->get('baseID', TRUE);
			
            if($id == null)
            {
                redirect($this->controller.'?baseID='.$baseID);
            }
            
            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->frowarder_model->getFrowarderInfo($id);
			
			$this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'editFrowarder';
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = $this->pageShortName .' Details';
			
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/editOld', $data);
            $this->load->view('includes/footer');
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editFrowarder()
    {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
			
			 $baseID = $this->input->get('baseID', TRUE);
			 
			 //print_r($unit_name); die();
            		
			//$this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('id_name','Shipper Name','trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('address','address','trim|xss_clean');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($id);
            }
            else
            {
                
			    $name = $this->input->post('id_name');
                $active = $this->input->post('active');
                $address = $this->input->post('address');
				$shipment_mode = $this->input->post('shipment_mode', true);
                
                $IDInfo = array();
                
               	
                $IDInfo = array( 'name'=> $name, 'address'=> $address, 'shipment_mode'=>$shipment_mode,
										'active'=>$active,'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));
                
                $result = $this->frowarder_model->editFrowarder($IDInfo, $id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Frowarder updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Frowarder  update failed');
                }
                
				
				redirect($this->controller.'?baseID='.$baseID);
            }
        
    }


    
    
}

?>