<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Currency extends BaseController
{
    /**
     * This is default constructor of the class
     */
	public $controller = "currency";
	public $pageTitle = 'Currency Management';
	public $pageShortName = 'Currency';

    public function __construct()
    {
        parent::__construct();
		$this->load->model('currency_model');
		$this->load->library('pagination');
        $this->isLoggedIn();
		 $menu_key = 'cur';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true)
		 {
			 redirect('access');
		 }


    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);



	        $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = 'List';

            $data['userRecords'] = $this->currency_model->currencyListing();

		    $this->load->view('includes/header', $this->global);
			$this->load->view($this->controller.'/index', $data);
			$this->load->view('includes/footer');



    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {

            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);


            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addCurrency';
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = $this->pageShortName .' Details';


            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addNew',$data);
            $this->load->view('includes/footer');

    }

    /**
     * This function is used to add new user to the system
     */
    function addCurrency()
    {

			$this->load->library('form_validation');

           // $this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('id_name','Currency Name','trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('short_name','Short Name','trim|required|max_length[255]|xss_clean');

            $baseID = $this->input->get('baseID', TRUE);

		   if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {

                $id_name = $this->input->post('id_name');
                $short_name = $this->input->post('short_name');
                $bdt = $this->input->post('bdt');
                $active = $this->input->post('active');




				$IdInfo = array('name'=>$id_name,'short_name'=>$short_name, 'bdt'=>$bdt, 'active'=>$active, 'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:s'));

				$result = $this->currency_model->addNewCurrency($IdInfo);

				if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Currency created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Currency creation failed');
                }


                redirect($this->controller.'?baseID='.$baseID);
            }

    }


    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($id = NULL)
    {
			$baseID = $this->input->get('baseID', TRUE);

            if($id == null)
            {
                redirect($this->controller.'?baseID='.$baseID);
            }

            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->currency_model->getCurrencyInfo($id);

			$this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'editCurrency';
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = $this->pageShortName .' Details';


            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/editOld', $data);
            $this->load->view('includes/footer');
    }


    /**
     * This function is used to edit the user information
     */
    function editCurrency()
    {
            $this->load->library('form_validation');

            $id = $this->input->post('id');

			 $baseID = $this->input->get('baseID', TRUE);

			 //print_r($unit_name); die();

			//$this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('id_name','Currency Name','trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('short_name','Short Name','trim|required|max_length[255]|xss_clean');


            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($id);
            }
            else
            {

			    $name = $this->input->post('id_name');
                $active = $this->input->post('active');
                $short_name = $this->input->post('short_name');
                $bdt = $this->input->post('bdt');

                $IDInfo = array();


                $IDInfo = array( 'name'=> $name, 'short_name'=> $short_name, 'bdt'=>$bdt,
										'active'=>$active,'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));

                $result = $this->currency_model->editCurrency($IDInfo, $id);

                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Currency updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Currency  update failed');
                }


				redirect($this->controller.'?baseID='.$baseID);
            }

    }




}

?>