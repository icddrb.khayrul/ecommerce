<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Image extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();

        $this->isLoggedIn();
    }

    public function show()
    {
        $filename =  time() . '.jpg';
		$filepath =  'http://localhost:81/nvims/'.'assets/upload/';

		//read the raw POST data and save the file with file_put_contents()
		$result = file_put_contents( "assets/upload/".$filename, file_get_contents('php://input') );
		if (!$result) {
			print "ERROR: Failed to write data to $filename, check permissions\n";
			exit();
		}

		echo $filepath.$filename;
    }

}