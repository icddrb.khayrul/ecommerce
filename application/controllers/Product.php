<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';




class Product extends BaseController
{

    /**
     * This is default constructor of the class
     */
	public $controller = "product";
	public $pageTitle = 'Product Management';
	public $pageShortName = 'Product';
	
    public function __construct()
    {
        parent::__construct();
		$this->load->model('agent_model');
        $this->load->library('pagination');
        $this->load->model('utilities');
        $this->load->model('product_model');
        $this->isLoggedIn(); 
		 $menu_key = 'product';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true) 
		 {
			 redirect('access');
		 }
		 
			
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
			
	        $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = 'List';
			
            $data['result'] = $this->product_model->getAllProducts();

            //$this->pr($data);
			
		    $this->load->view('includes/header', $this->global);
			$this->load->view($this->controller.'/index', $data);
			$this->load->view('includes/footer');
			
		
		
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
            $this->load->helper('form');
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewProd';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;			

            $data['category'] = $this->utilities->dropdownFromTableWithCondition('tbl_category_list','Select Category','id', 'name', array('level'=>'2'));

            $data['color'] = $this->utilities->dropdownFromTableWithCondition('tbl_base_setup_chd','Select color','chd_id', 'name', array('mst_id'=>'12'));

            $data['size'] = $this->utilities->dropdownFromTableWithCondition('tbl_base_setup_chd','Select size','chd_id', 'name', array('mst_id'=>'13'));
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addNew',$data);
            $this->load->view('includes/footer');
        
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewProd()
    {
        
        $baseID = $this->input->get('baseID', TRUE);
        $category = $this->input->post('category');
        $color = $this->input->post('color');
        $size = $this->input->post('size');
        $active = $this->input->post('active');
        $imageName = $this->docFileUpload();
        $IdInfo = array('cat_id'=>$category,'color_id'=>$color, 'size_id'=>$size,'active'=>$active, 'path'=> $imageName, 'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:s'));
        $result = $this->utilities->insertData($IdInfo,'tbl_products');
        if($result > 0)
        {
            $this->session->set_flashdata('success', 'New Category created successfully');
        }
        else
        {
            $this->session->set_flashdata('error', 'Category creation failed');
        }
        redirect($this->controller.'?baseID='.$baseID);
                
    }




    public function editOld($id = NULL){
            $this->load->helper('form');
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'updateProd';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;			

            $data['category'] = $this->utilities->dropdownFromTableWithCondition('tbl_category_list','Select Category','id', 'name', array('level'=>'2'));

            $data['color'] = $this->utilities->dropdownFromTableWithCondition('tbl_base_setup_chd','Select color','chd_id', 'name', array('mst_id'=>'12'));

            $data['size'] = $this->utilities->dropdownFromTableWithCondition('tbl_base_setup_chd','Select size','chd_id', 'name', array('mst_id'=>'13'));

            $data['selectAll'] = $this->utilities->findResultByAttribute('tbl_products', array('id'=>$id));

            
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/editProd',$data);
            $this->load->view('includes/footer');
        

    }




    function updateProd()
    {
        
        $baseID = $this->input->get('baseID', TRUE);
        $category = $this->input->post('category');
        $color = $this->input->post('color');
        $size = $this->input->post('size');
        $active = $this->input->post('active');
        $existImage = $this->input->post('existImage');

        $id = $this->input->post('existId');

       
        if (!file_exists($_FILES['fileUpload']['tmp_name']) || !is_uploaded_file($_FILES['fileUpload']['tmp_name'])) 
        {
            $imageName = $existImage;
        }
        else
        {
            $imageName = $this->docFileUpload();
        }
        
        $IdInfo = array('cat_id'=>$category,'color_id'=>$color, 'size_id'=>$size,'active'=>$active, 'path'=> $imageName, 'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));

        $result = $this->utilities->updateData('tbl_products', $IdInfo, array('id'=>$id));


        if($result > 0)
        {
            $this->session->set_flashdata('success', 'Category Updated successfully');
        }
        else
        {
            $this->session->set_flashdata('error', 'Category Updated failed');
        }
        redirect($this->controller.'?baseID='.$baseID);
                
    }





    public function docFileUpload() {
		if(isset($_FILES['fileUpload'])){
			$IMAGE_NAME = '';
			$errors= array();
			$file_name = $_FILES['fileUpload']['name'];
			$file_size =$_FILES['fileUpload']['size'];
			$file_tmp =$_FILES['fileUpload']['tmp_name'];
			$file_type=$_FILES['fileUpload']['type'];
			$tmp = explode('.', $file_name);
			$file_ext = end($tmp); // .png or other Image's ext 

			$extensions= array("jpeg","jpg","png","pdf","doc","docx");
			
			if(in_array($file_ext,$extensions)=== false){
			   $errors[]="extension not allowed, please choose a JPEG or PNG file.";
			}
			
			if($file_size > 2097152){
			   $errors[]='File size must be extensions 2 MB';
			}
			
            $targetDir = "assets/images/upload"; // Original image upload location
            
            $targetDirThrum = "assets/images/upload/thrum/"; // upload thrum image location

            $imageNewName = time(); // Rename image 

            

			if(empty($errors)==true){
             
               move_uploaded_file($file_tmp, $targetDir.'/'.$imageNewName.'.'.$file_ext);
               $newImg = $targetDir.'/'.$imageNewName.'.'.$file_ext;
               
               $this->make_thumb($newImg, $targetDirThrum); // call thrum function

               $IMAGE_NAME =  $imageNewName.'.'.$file_ext;

			}else{
			   $IMAGE_NAME =  'NOT_FOUND';
			}

			return $IMAGE_NAME;
		 }
		
     }
     



     function make_thumb($file, $pathToSave) {

        $tmp = explode('.', $file);
		$file_ext = end($tmp); // .png or other Image's ext 
        
        list($origWidth, $origHeight) = getimagesize($file);

        //echo $origHeight; exit;
        
        //$ratio = min($origWidth, $origHeight);
         
        $sourceWidth = 300;
        $sourceHeight = 300;

        $what = getimagesize($file);
        $file_name = basename($file);/* Name of the Image File*/
        $ext   = pathinfo($file_name, PATHINFO_EXTENSION);
    
        /* Adding image name _thumb for thumbnail image */
        $file_name = basename($file_name, ".$ext");
    
        switch(strtolower($what['mime']))
        {
            case 'image/png':
                $img = imagecreatefrompng($file);
                $new = imagecreatetruecolor($sourceWidth,$sourceHeight);
                imagecopyresampled($new,$img, 0, 0, 0, 0,$sourceWidth,$sourceHeight, $origWidth,$origHeight);
                header('Content-Type: image/png');           
            break;
            case 'image/jpeg':
                $img = imagecreatefromjpeg($file);
                $new = imagecreatetruecolor($sourceWidth,$sourceHeight);
                imagecopyresampled($new,$img, 0, 0, 0, 0,$sourceWidth,$sourceHeight, $origWidth,$origHeight);
                header('Content-Type: image/jpeg');
            break;
            case 'image/gif':
                $img = imagecreatefromgif($file);
                $new = imagecreatetruecolor($sourceWidth,$sourceHeight);
                imagecopyresampled($new,$img, 0, 0, 0, 0,$sourceWidth,$sourceHeight, $origWidth,$origHeight);
                header('Content-Type: image/gif');
            break;
            default: die();
        }
    
            imagejpeg($new,$pathToSave.$file_name.'.'.$file_ext);
            imagedestroy($new);
    }
        
       




    /**
     * This function is used to add new user to the system
     */
    function addSubCat($id)
    {
          
			$baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewCat';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;	
            $data['parent_id']	= $id;	
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addSubCat',$data);
            $this->load->view('includes/footer');
        
    }


    /**
     * This function is used to add new user to the system
     */
    function addSubSubCat($id)
    {
          
			$baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewCat';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;	
            $data['parent_id']	= $id;	
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addSubSubCat',$data);
            $this->load->view('includes/footer');
        
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editcat($id = NULL)
    {
			$baseID = $this->input->get('baseID', TRUE);
			
            if($id == null)
            {
                redirect($this->controller.'?baseID='.$baseID);
            }
            
            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->agent_model->getCatInfo($id);
			
			$this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'updatecat';
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = $this->pageShortName .' Details';
			
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/editcat', $data);
            $this->load->view('includes/footer');
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function updatecat()
    {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
			
			 $baseID = $this->input->get('baseID', TRUE);
			 
			 //print_r($unit_name); die();
            		
			//$this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('name','Agent Name','trim|required|max_length[255]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editcat($id);
            }
            else
            {
                
			    $name = $this->input->post('name');
                
                $active = $this->input->post('active');
                
                $IDInfo = array();
                
               	
                $IDInfo = array( 'name'=>$name,
				'active'=>$active,'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));
                
                $result = $this->utilities->updateData('tbl_category_list',$IDInfo, array('id'=>$id));
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Category updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category  update failed');
                }
                
				
				redirect($this->controller.'?baseID='.$baseID);
            }
        
    }




    public function deletecat($id = NULL){
        $baseID = $this->input->get('baseID', TRUE);
        $getItem  = $this->agent_model->getDeleteList($id);

        if(!empty($getItem)){

            foreach($getItem as $va){
                $this->utilities->deleteRowByAttribute('tbl_category_list', array('id'=>$va->id));
            }
        }

        $this->utilities->deleteRowByAttribute('tbl_category_list', array('id'=>$id));

        redirect($this->controller.'?baseID='.$baseID);
    }


    public function pr($data)
    {

            echo '<pre>';
            print_r($data);
            exit;
    }


    
    
}

?>