<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Visitor extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('visitor_model');
		$this->load->model('visitorType_model');
		$this->load->model('id_model');
		$this->load->library('pagination');
        $this->isLoggedIn(); 
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID);
		 if ($result != true) 
		 {
			 redirect('access');
		 }
			
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			$data['userRecords'] = array();
			
	        $this->global['pageTitle'] = 'ecommerce : Visitor List';
			
			$searchStart = '';
			$searchEnd = '';
			
			if($this->input->post('Clear'))
			{
				$this->session->unset_userdata('searchStart'); 
				$this->session->unset_userdata('searchEnd'); 
				$data['searchStart'] = '';
				$data['searchEnd'] = '';
			}
			
			$searchStart = $this->input->post('searchStart');
			$searchEnd = $this->input->post('searchEnd');
			
			$data['searchStart'] = $this->session->userdata('searchStart');
			$data['searchEnd'] = $this->session->userdata('searchEnd');
			
			if($this->input->post('search'))
			{
			
			$this->session->set_userdata('searchStart', $searchStart);
			$this->session->set_userdata('searchEnd', $searchEnd);
			
		    $data['searchStart'] = $this->session->userdata('searchStart');
		    $data['searchEnd'] = $this->session->userdata('searchEnd');
	        }
			
		    if (!empty($this->employeeid))
			{
				 $data['userRecords'] = $this->visitor_model->visitorListIndividual($this->employeeid);
			}
			else  
			{
				  $data['userRecords'] = $this->visitor_model->visitorList();
			}
			
			if ((!empty($data['searchStart'])) && (!empty($data['searchEnd'])))
			{
				 $data['userRecords'] = $this->visitor_model->visitorListSearch($data['searchStart'],  $data['searchEnd']);
			}
			
			
			 
			$self_service = 0;
		    $system_user = $this->session->userdata('system_user');
			if($system_user == 0)
			{
             $self_service = 1;
			}
			
		    $this->load->view('includes/header', $this->global);
			if ($self_service == 1)	{
				$this->load->view('visitor/indexSelf', $data);
			}
			else {
			  $this->load->view('visitor/index', $data);
			}
			$this->load->view('includes/footer');
			
		
		
    }
    
	
    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
       
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $this->global['pageTitle'] = 'ecommerce : Add New Visitor';
			$data['id_type'] =  $this->id_model->getIDType();
			$data['visitor_type'] =  $this->visitorType_model->getVisitorType();
			
			$data['employeeId'] = '';
			$self_service = 0;
			$data['department'] = '';
			$data['job_title'] = '';
			$data['name'] = '';
			
			$system_user = $this->session->userdata('system_user');
			
			if($system_user == 0)
			{
			 $data['employeeId'] = $this->employeeid;
             $self_service = 1;
			 $data['job_title'] = $this->job_title;
			 $data['department'] =  $this->department;
			 $data['name'] = $this->name;
			 $data['extension'] = $this->extension;
			
			}
			
			
			//$data['employee'] = $this->visitor_model->getEmployees();
            $this->load->view('includes/header', $this->global);
			if ( $self_service == 1){
             $this->load->view('visitor/addNewSelf',$data);
			}
			else{$this->load->view('visitor/addNew',$data);}
            $this->load->view('includes/footer');
        
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewVisitor()
    {
        	
           // print_r($this->input->post()); die();
			$this->load->library('form_validation');
			
			//fullname, id_type, id_number, to_meet, designation, department, visitor_type_id, address, mobile_phone,
			//check_in, check_out, batch_number, visitor_photo, insertedOn, insertedBy,
			//visiting_purpose, parking_exempt,license_plate_no, comments
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('id_type','ID Type','trim|required|numeric');
			$this->form_validation->set_rules('id_number','ID Number','trim|required|xss_clean');
			$this->form_validation->set_rules('visitor_type','Visitor Type','trim|required|numeric');
			$this->form_validation->set_rules('employee_select','Visiting Person','trim|required|xss_clean');
			//$this->form_validation->set_rules('department','Department','trim|required|max_length[255]|xss_clean');
			$this->form_validation->set_rules('address','Address','trim|required|xss_clean');
			$this->form_validation->set_rules('mobile','Mobile','trim|required|xss_clean');
			//$this->form_validation->set_rules('check_in','Check in Time','trim|required|xss_clean');
			$this->form_validation->set_rules('batch_number','Batch Number','trim|required|xss_clean');
              
            $baseID = $this->input->get('baseID', TRUE);
           
		   if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
				//redirect('unit/addNew?baseID='.$baseID);
            }
            else
            {
				
                $fullname = $this->input->post('fname', TRUE);
                $id_type = $this->input->post('id_type', TRUE);
				$id_number = $this->input->post('id_number', TRUE);
                $to_meet = $this->input->post('employee_select', TRUE);
				$designation = $this->input->post('designation', TRUE);
                $department = $this->input->post('department', TRUE);
				$visitor_type_id = $this->input->post('visitor_type', TRUE);
                $address = $this->input->post('address', TRUE);
				$mobile_phone = $this->input->post('mobile', TRUE);
               // $check_in = $this->input->post('check_in', TRUE);
				$batch_number = $this->input->post('batch_number', TRUE);
				$time_expected = date('h:i:sa');
                $date_expected = date('Y-m-d');				
				$visiting_purpose = $this->input->post('visiting_purpose', TRUE);
                $parking_exempt = $this->input->post('parking_need', TRUE);
                $comments = $this->input->post('comments', TRUE);
				
				$extension = $this->input->post('extension', TRUE);
				$check_in = date('Y-m-d H:i:sa');
				
				$insertedBy = $this->vendorId;
				$insertedOn = date('Y-m-d H:i:sa');
				//$image_location = $this->input->post('image_location', TRUE);
				
				//print_r($image_location); die();
				
				//$pos = strrpos($image_location, '/');
                //$visitor_photo = $pos === false ? $image_location : substr($image_location, $pos + 1);
				
				//$visitor_photo  = '';
				if (!empty($parking_exempt))
				{
				  $parking_exempt = 1;
				  $license_plate_no = $this->input->post('license_no', TRUE);
				}
				else
				{
				  $parking_exempt = 0;
				  $license_plate_no = '';
				}
				
				//print_r($this->input->post($parking_exempt)); die();
				
				 //'visitor_photo'=>$visitor_photo,
					
				$VisirotInfo = array('fullname'=>$fullname, 'id_type' =>$id_type, 'extension' => $extension,
				'id_number'=>$id_number, 'to_meet'=>$to_meet, 'designation'=>$designation, 'department'=>$department, 
				'visitor_type_id'=>$visitor_type_id, 'address'=>$address, 'mobile_phone'=>$mobile_phone, 'check_in'=>$check_in, 
				'batch_number'=>$batch_number, 'time_expected'=>$time_expected, 'date_expected'=>$date_expected, 'visiting_purpose'=>$visiting_purpose, 
				'parking_exempt'=>$parking_exempt, 'comments'=>$comments, 'license_plate_no'=>$license_plate_no, 
				'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:sa'));
				
					
				$result = $this->visitor_model->addNewVisitor($VisirotInfo);
				
				if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Visitor created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Visitor creation failed');
                }
				
					
                redirect('visitor?baseID='.$baseID);
            }
        
    }
	
	/**
     * This function is used to add new user to the system
     */
    function addNewVisitorSelf()
    {
        	
           // print_r($this->input->post()); die();
			$this->load->library('form_validation');
			   
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('id_type','ID Type','trim|required|numeric');
			$this->form_validation->set_rules('id_number','ID Number','trim|required|xss_clean');
			$this->form_validation->set_rules('visitor_type','Visitor Type','trim|required|numeric');
			$this->form_validation->set_rules('employeeid','To Meet','trim|required|xss_clean');
			//$this->form_validation->set_rules('department','Department','trim|required|max_length[255]|xss_clean');
			$this->form_validation->set_rules('address','Address','trim|required|xss_clean');
			$this->form_validation->set_rules('mobile','Mobile','trim|required|xss_clean');
			$this->form_validation->set_rules('date_expected','Excepted Check in Date','trim|required|xss_clean');
			$this->form_validation->set_rules('time_expected','Excepted Check in Time','trim|required|xss_clean');
			        
            $baseID = $this->input->get('baseID', TRUE);
           
		   if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
				//redirect('unit/addNew?baseID='.$baseID);
            }
            else
            {
				
                $fullname = $this->input->post('fname', TRUE);
                $id_type = $this->input->post('id_type', TRUE);
				$id_number = $this->input->post('id_number', TRUE);
                $to_meet = $this->input->post('employeeid', TRUE);
				$designation = $this->input->post('designation', TRUE);
                $department = $this->input->post('department', TRUE);
				$visitor_type_id = $this->input->post('visitor_type', TRUE);
                $address = $this->input->post('address', TRUE);
				$mobile_phone = $this->input->post('mobile', TRUE);
				$time_expected = $this->input->post('time_expected', TRUE);
                $date_expected =$this->input->post('date_expected', TRUE);				
				$visiting_purpose = $this->input->post('visiting_purpose', TRUE);
                $parking_exempt = $this->input->post('parking_need', TRUE);
                $comments = $this->input->post('comments', TRUE);
				
				$extension = $this->input->post('extension', TRUE);
				
				 // Change Date Format for mysql
				$parts = explode('/', $date_expected);
				$new_date_expected = $parts[2].'-'.$parts[0].'-'.$parts[1];
				
				$insertedBy = $this->vendorId;
				$insertedOn = date('Y-m-d H:i:sa');
				$self_service = 1;
		
				if (!empty($parking_exempt))
				{
				  $parking_exempt = 1;
				  $license_plate_no = $this->input->post('license_no', TRUE);
				}
				else
				{
				  $parking_exempt = 0;
				  $license_plate_no = '';
				}
				//echo $parking_exempt; die();
					
				$VisirotInfo = array('fullname'=>$fullname, 'id_type' =>$id_type,'extension' =>$extension,'self_service' =>$self_service,
				'id_number'=>$id_number, 'to_meet'=>$to_meet, 'designation'=>$designation, 'department'=>$department, 
				'visitor_type_id'=>$visitor_type_id, 'address'=>$address, 'mobile_phone'=>$mobile_phone,  
				'time_expected'=>$time_expected, 'date_expected'=>$new_date_expected, 'visiting_purpose'=>$visiting_purpose, 
				'parking_exempt'=>$parking_exempt, 'comments'=>$comments, 'license_plate_no'=>$license_plate_no, 
				'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:sa'));
				
					
				$result = $this->visitor_model->addNewVisitor($VisirotInfo);
				
				if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Visitor created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Visitor creation failed');
                }
				
					
                redirect('visitor?baseID='.$baseID);
            }
        
    }
	
	function check_out($id)
    {
             
	        
             $baseID = $this->input->get('baseID', TRUE);
			 
			 $check_out = date('Y-m-d H:i:sa');
			 
			 $userInfoCheck = $this->visitor_model->getVisitorInfo($id);
			 $check_in_check = '';
			 if (!empty($userInfoCheck ))
			 {
				   foreach ($userInfoCheck as $row):
				   
				   $check_in_check= $row->check_in;
				   
				   endforeach;
			 }
			 
			 if (empty($check_in_check))
			 {
				 $this->session->set_flashdata('error', "You can't checked in before check out");
			     redirect('visitor?baseID='.$baseID);
			 }
			 
			 $visitorInfo = array('check_out'=>$check_out, 'modifiedBy'=>$this->vendorId, 'modifiedOn'=>date('Y-m-d H:i:sa'));
			 
             $result = $this->visitor_model->checkOutVisitor($id, $visitorInfo);
            
             if($result == true)
                {
                    $this->session->set_flashdata('success', 'Visitor Check Out successfully');
					redirect('visitor?baseID='.$baseID);
                }
                else
                {
                    $this->session->set_flashdata('error', 'Visitor Check Out failed');
					redirect('visitor?baseID='.$baseID);
                }
			
        
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($id = NULL)
    {
            
			$baseID = $this->input->get('baseID', TRUE);
			
            if($id == null)
            {
                redirect('visitor/index?baseID='.$baseID);
            }
            
            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->visitor_model->getVisitorInfo($id);
			
			
            
            $this->global['pageTitle'] = 'ecommerce : Edit Visitor';
			
			
			
			$data['visitor_type'] =  $this->visitorType_model->getVisitorType();
			$data['employee'] = $this->visitor_model->getEmployees();
			$data['id_type'] =  $this->id_model->getIDType();
			
            $this->load->view('includes/header', $this->global);
            $this->load->view('visitor/editOld', $data);
            $this->load->view('includes/footer');
    }
	
	 function editOldSelf($id = NULL)
    {
            
			$baseID = $this->input->get('baseID', TRUE);
			
            if($id == null)
            {
                redirect('visitor/index?baseID='.$baseID);
            }
            
            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->visitor_model->getVisitorInfo($id);
			
			
            
            $this->global['pageTitle'] = 'ecommerce : Edit Self Visitor';
			
			
			
			$data['visitor_type'] =  $this->visitorType_model->getVisitorType();
			$data['id_type'] =  $this->id_model->getIDType();
			
			$self_service = 0;
			//$data['name'] = '';
			$system_user = $this->session->userdata('system_user');
			if($system_user == 0)
			{
             $self_service = 1;
			 //$data['name'] = $this->name;

			}
			
            $this->load->view('includes/header', $this->global);
            $this->load->view('visitor/editOldSelf', $data);
            $this->load->view('includes/footer');
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editVisitor()
    {
        
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
			
			$this->form_validation->set_rules('fname','Full Name','trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('id_type','ID Type','trim|required|numeric');
			$this->form_validation->set_rules('id_number','ID Number','trim|required|xss_clean');
			$this->form_validation->set_rules('visitor_type','Visitor Type','trim|required|numeric');
			$this->form_validation->set_rules('employee_select','Visiting Person','trim|required|xss_clean');
			$this->form_validation->set_rules('address','Address','trim|required|xss_clean');
			$this->form_validation->set_rules('mobile','Mobile','trim|required|xss_clean');
			$this->form_validation->set_rules('batch_number','Badge Number','trim|required|xss_clean');
              
            $baseID = $this->input->get('baseID', TRUE);
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($id);
            }
            else
            {
                
                $fullname = $this->input->post('fname', TRUE);
                $id_type = $this->input->post('id_type', TRUE);
				$id_number = $this->input->post('id_number', TRUE);
                $to_meet = $this->input->post('employee_select', TRUE);
				$visitor_type_id = $this->input->post('visitor_type', TRUE);
                $address = $this->input->post('address', TRUE);
				$mobile_phone = $this->input->post('mobile', TRUE);
				$batch_number = $this->input->post('batch_number', TRUE);				
                $parking_exempt = $this->input->post('parking_need', TRUE);
                $comments = $this->input->post('comments', TRUE);
				
				$modifiedBy = $this->vendorId;
				$modifiedOn = date('Y-m-d H:i:sa');
				
				$check_in = date('Y-m-d H:i:sa');
				
				
				if (!empty($parking_exempt))
				{
				  $parking_exempt = 1;
				  $license_plate_no = $this->input->post('license_no', TRUE);
				}
				else
				{
				  $parking_exempt = 0;
				  $license_plate_no = '';
				}
                
				$VisirotInfo = array('fullname'=>$fullname, 'id_type' =>$id_type, 'check_in' =>$check_in,
				'id_number'=>$id_number,'visitor_type_id'=>$visitor_type_id, 'address'=>$address, 'mobile_phone'=>$mobile_phone, 
				'batch_number'=>$batch_number,'parking_exempt'=>$parking_exempt, 'comments'=>$comments, 'license_plate_no'=>$license_plate_no, 
				'modifiedBy'=>$this->vendorId, 'modifiedOn'=>date('Y-m-d H:i:sa'));
				
				$result = $this->visitor_model->editVisitorWithoutPhoto($VisirotInfo, $id);
				
				
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Visitor updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Visitor update failed');
                }
                
				
				redirect('visitor?baseID='.$baseID);
            }
        
    }
	
	    /**
     * This function is used to edit the user information
     */
    function editVisitorSelf()
    {
        
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
			
			$this->form_validation->set_rules('fname','Full Name','trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('id_type','ID Type','trim|required|numeric');
			$this->form_validation->set_rules('id_number','ID Number','trim|required|xss_clean');
			$this->form_validation->set_rules('visitor_type','Visitor Type','trim|required|numeric');
			$this->form_validation->set_rules('to_meet','Visiting Person','trim|required|xss_clean');
			$this->form_validation->set_rules('address','Address','trim|required|xss_clean');
			$this->form_validation->set_rules('mobile','Mobile','trim|required|xss_clean');
              
            $baseID = $this->input->get('baseID', TRUE);
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldSelf($id);
            }
            else
            {
				$to_meet = $this->input->post('to_meet', TRUE);
				
				$this->employeeId = $this->session->userdata('employee_id');
				
				if ($to_meet != $this->employeeId)
				{
					$this->session->set_flashdata('error', 'You are not authorized to change this information');
					redirect('visitor/editOldSelf/'.$id.'?baseID='.$baseID); 
				}
                
                $fullname = $this->input->post('fname', TRUE);
                $id_type = $this->input->post('id_type', TRUE);
				$id_number = $this->input->post('id_number', TRUE);
                $to_meet = $this->input->post('employeeid_final', TRUE);
				$visitor_type_id = $this->input->post('visitor_type', TRUE);
                $address = $this->input->post('address', TRUE);
				$mobile_phone = $this->input->post('mobile', TRUE);			
				$visiting_purpose = $this->input->post('visiting_purpose', TRUE);
                $parking_exempt = $this->input->post('parking_need', TRUE);
				$date_expected = $this->input->post('date_expected', TRUE);
				$time_expected = $this->input->post('time_expected', TRUE);
                $comments = $this->input->post('comments', TRUE);
				$modifiedBy = $this->vendorId;
				$modifiedOn = date('Y-m-d H:i:sa');
				
				// Change Date Format for mysql
				$parts = explode('/', $date_expected);
				$new_date_expected = $parts[2].'-'.$parts[0].'-'.$parts[1];
			
				if (!empty($parking_exempt))
				{
				  $parking_exempt = 1;
				  $license_plate_no = $this->input->post('license_no', TRUE);
				}
				else
				{
				  $parking_exempt = 0;
				  $license_plate_no = '';
				}
                
				$VisirotInfo = array('fullname'=>$fullname, 'id_type' =>$id_type, 
				'id_number'=>$id_number,'visitor_type_id'=>$visitor_type_id, 'address'=>$address, 'mobile_phone'=>$mobile_phone, 
				'date_expected'=>$new_date_expected, 'time_expected'=>$time_expected, 
				'parking_exempt'=>$parking_exempt, 'comments'=>$comments, 'license_plate_no'=>$license_plate_no, 
				'modifiedBy'=>$this->vendorId, 'modifiedOn'=>date('Y-m-d H:i:sa'));
				
				$result = $this->visitor_model->editVisitorWithoutPhoto($VisirotInfo, $id);
				
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Information updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Visitor update failed');
                }
                
				
				redirect('visitor?baseID='.$baseID);
            }
        
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteVisitor($id = null)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            
             $baseID = $this->input->get('baseID', TRUE);
		     $array = array ('is_deleted' => 1, 'modifiedOn' => date('Y-m-d H:i:sa'), 'modifiedBy' => $this->vendorId);
             $result = $this->visitor_model->deleteVisitorAdmin($id, $array);
            
             if($result == true)
                {
                    $this->session->set_flashdata('success', 'Visitor Deleted successfully');
					redirect('visitor/index?baseID='.$baseID);
                }
                else
                {
                    $this->session->set_flashdata('error', 'Visitor deletion failed');
					redirect('visitor/index?baseID='.$baseID);
                }
				
			
        }
    }
	
	function deleteVisitorSelf($id = null)
    {
            
	 $baseID = $this->input->get('baseID', TRUE);
	 $result = $this->visitor_model->deleteVisitor($id);

	 if($result == true)
		{
			$this->session->set_flashdata('success', 'Visitor Deleted successfully');
			redirect('visitor/index?baseID='.$baseID);
		}
		else
		{
			$this->session->set_flashdata('error', 'Visitor deletion failed');
			redirect('visitor/index?baseID='.$baseID);
		}
				
    }
	
	function print_badge($id,$badge) {


            
           
		$data = [];
        $data['userInfo'] = $this->visitor_model->getVisitorInfoPDF($id);
		
        $html = $this->load->view('visitor/badge', $data, true);
        $pdfFilePath = "visitor_".$badge.".pdf";
        $this->load->library('m_pdf');
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "D");   
       
        
	}
	
    
    
}

?>