<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Base_setup extends BaseController
{
    /**
     * This is default constructor of the class
     */
	public $controller = "base_setup";
	public $pageTitle = 'Base Setup';
	public $pageShortName = 'base_setup';

    public function __construct()
    {
        parent::__construct();
		$this->load->model('frowarder_model');
		$this->load->model('currency_model');
		$this->load->model('progress_model');
		$this->load->model('variance_model');
		$this->load->model('agent_model');
		$this->load->model('pi_model');
		$this->load->model('utilities');
		$this->load->library('pagination');
		$this->load->helper(array('form'));
        $this->isLoggedIn();
		 $menu_key = 'bs';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true)
		 {
			 redirect('access');
		 }


    }



    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {

    		$baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);



	        $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = 'List';


			$data['mstData'] = $this->utilities->findAllFromView('tbl_base_setup_mst');
            $data['chdData'] = $this->utilities->findAllFromView('tbl_base_setup_chd');


            $data['selectAgent'] = $this->utilities->dropdownFromTableWithCondition('tbl_agent_list', 'Select Agent', 'id', 'name');



            //$this->pr($data);

		    $this->load->view('includes/header', $this->global);
			$this->load->view('includes/script');
			$this->load->view($this->controller.'/index', $data);
			$this->load->view('includes/footer');

    }

    

    /**
     * Add function
     *
     * @param Place   where  Where something interesting takes place
     * @param integer repeat How many times something interesting should happen
     *
     * @throws Some_Exception_Class If something interesting cannot happen
     * @author Md. Khayrul Hasan <khayrul.web@gmail.com>
     * @return Status
     */

    public function addNew()
    {

    		if($this->input->post()){


    			$data = array(
    				'name' => $this->input->post('name'),
    				'is_active' => $this->input->post('active'),
    				'insertBy'=>$this->vendorId,
    				'insertOn'=>date('Y-m-d H:i:s')
    			);
    			$this->utilities->insertData($data,'tbl_base_setup_mst');

    			$baseID = $this->input->get('baseID', TRUE);
    			redirect($this->controller.'/addNew?baseID='.$baseID);

    		}else{
	    		$baseID = $this->input->get('baseID', TRUE);
				$this->load->model('menu_model');
			    $this->global['menu'] =  $this->menu_model->getMenu($this->role);

		        $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
		        $data['pageTitle'] = $this->pageTitle;
				$data['controller'] = $this->controller;
				$data['shortName'] = $this->pageShortName;
				$data['boxTitle'] = 'List';

			    $this->load->view('includes/header', $this->global);
				$this->load->view('includes/script');
				$this->load->view($this->controller.'/addNew', $data);
				$this->load->view('includes/footer');
    		}


    }


    public function saveData()
    {


    	$data = array(
    				'name' => $this->input->post('name'),
    				'mst_id'=>$this->input->post('mst_id'),
    				'agent_id'=>$this->input->post('agent_id'),
    				'percentage'=>$this->input->post('percentage'),
    				'minimum'=>$this->input->post('minimum'),
    				'maximum'=>$this->input->post('maximum'),
    				'insertBy'=>$this->vendorId,
    				'insertOn'=>date('Y-m-d H:i:s')
    			);
		$id = $this->utilities->insert('tbl_base_setup_chd', $data);

    }


    public function updateData()
    {

    	$agent_id = $this->input->post('agent_id');
    	$mst_id = $this->input->post('mst_id');
    	$chd_id = $this->input->post('chd_id');
    	$name = $this->input->post('name');

    	$data = array(
    				'name' => $name,
    				'agent_id' => $agent_id,
    				'percentage'=>$this->input->post('percentage'),
    				'minimum'=>$this->input->post('minimum'),
    				'maximum'=>$this->input->post('maximum'),
    				'updateBy'=>$this->vendorId,
    				'updateOn'=>date('Y-m-d H:i:s')
    			);
		$this->utilities->updateData('tbl_base_setup_chd', $data, array('mst_id' => $mst_id, 'chd_id'=> $chd_id));
		echo $name;
    }



    function getBaseSetupData()
    {
    	$data['mstData'] = $this->utilities->findAllFromView('tbl_base_setup_mst');
        $data['chdData'] = $this->utilities->findAllFromView('tbl_base_setup_chd');
        return $data;
    }



     public function deleteRecourt(){

        $this->pr($_POST);

    	$table = $this->input->post('table');
    	$id = (int) $this->input->post('pk_id');
    	$pk_row = $this->input->post('pk_row');
        $idd = $this->utilities->deleteRowByAttribute($table,array($pk_row => $id ));

        if($idd){
            echo 'TRUE';
        }else{
            echo 'FALSE';
        }
    }



    public function addBaseForm()
    {

         $data['id'] = $id = $this->input->get('mst_id', TRUE);
         $data['baseID'] = $this->input->get('baseID', TRUE);


         $data['selectAgent'] = $this->utilities->dropdownFromTableWithCondition('tbl_agent_list', 'Select Agent', 'id', 'name');



         //$this->pr($data);
         $this->load->view('base_setup/add_form', $data);
    }



    public function saveAddForm()
    {

        $baseID = $this->input->get('baseID', TRUE);
        $agent_id = $this->input->post('agentid');
        $mst_id = $this->input->post('id');
        $name = $this->input->post('item');
        $active = $this->input->post('active');

        $data = array(
                    'mst_id' => $mst_id,
                    'name' => $name,
                    'agent_id' => $agent_id,
                    'fld_air'=>$this->input->post('air'),
                    'fld_sea'=>$this->input->post('sea'),
                    'fld_land'=>$this->input->post('land'),
                    'percentage'=>$this->input->post('percentage'),
                    'minimum'=>$this->input->post('minimum'),
                    'maximum'=>$this->input->post('maximum'),
                    'is_active'=>(!empty($active))?1:0,
                    'updateBy'=>$this->vendorId,
                    'updateOn'=>date('Y-m-d H:i:s')
                );
        //$this->pr($data);
        $this->utilities->insertData($data,'tbl_base_setup_chd');

        redirect($this->controller.'?baseID='.$baseID);
    }



    public function editBaseForm()
    {

         $data['id'] = $id = $this->input->get('id', TRUE);
         $data['baseID'] = $this->input->get('baseID', TRUE);
         $data['chdData'] = $this->utilities->findByAttribute('tbl_base_setup_chd', array('chd_id' =>$id));


         $data['selectAgent'] = $this->utilities->dropdownFromTableWithCondition('tbl_agent_list', 'Select Agent', 'id', 'name');


         //$this->pr($data);
         $this->load->view('base_setup/edit_form', $data);
    }


    public function updateEditForm()
    {

        $baseID = $this->input->get('baseID', TRUE);
        $agent_id = $this->input->post('agentid');
        $chd_id = $this->input->post('id');
        $name = $this->input->post('item');
        $active = $this->input->post('active');
        $air = $this->input->post('air');
        $sea = $this->input->post('sea');
        $land = $this->input->post('land');

        $data = array(
                    'name' => $name,
                    'agent_id' => $agent_id,
                    'fld_air'=>$air,
                    'fld_sea'=>$sea,
                    'fld_land'=>$land,
                    'percentage'=>$this->input->post('percentage'),
                    'minimum'=>$this->input->post('minimum'),
                    'maximum'=>$this->input->post('maximum'),
                    'is_active'=>(!empty($active))?1:0,
                    'updateBy'=>$this->vendorId,
                    'updateOn'=>date('Y-m-d H:i:s')
                );
        //$this->pr($data);
        $this->utilities->updateData('tbl_base_setup_chd', $data, array('chd_id'=> $chd_id));

        redirect($this->controller.'?baseID='.$baseID);
    }






    public function pr($data)
    {

    		echo '<pre>';
            print_r($data);
            exit;
    }






}

