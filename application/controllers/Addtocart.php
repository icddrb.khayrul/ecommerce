<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

//require APPPATH . '/libraries/BaseController.php';

class Addtocart extends ci_controller
{
    /**
     * This is default constructor of the class
     */
	public $controller = "addtocart";
	public $pageTitle = 'addtocart Management';
	public $pageShortName = 'Addtocart';
	
    public function __construct()
    {
        parent::__construct();

        $this->load->model('fontend_model');
        $this->load->model('ip_model');
        $this->load->model('utilities');
        //$menu_key = 'addtocart';
    }

    

    public function addProductToTemp()
    {
        $ip_addr = $this->ip_model->identify_to_user();
        $id = $this->input->post('id', TRUE);
        $qnt = $this->input->post('qnt', TRUE);
        $plusMinus = $this->input->post('plus_minus');

        
      
        if(!empty($qnt)){
            $quantity = $qnt;
        }else{
            $quantity = 1;
        }

        $return_qnt = 0;

        $user_id = $this->utilities->findByAttribute('tbl_cart_temp_mst',array('user_uniq_id'=>$ip_addr))->id;


        $check_exist_prod = $this->utilities->findByAttribute('tbl_cart_temp_chd', array('user_uniq_id'=>$user_id, 'prod_id'=>$id));

        if(empty($check_exist_prod)){
             $this->utilities->insert('tbl_cart_temp_chd', array('user_uniq_id'=>$user_id, 'prod_id'=>$id, 'quantity'=>$quantity));
             $return_qnt = $quantity; 
        }else{
              
             $total_quantity = ($plusMinus === 'plus')? $check_exist_prod->quantity + $quantity:$check_exist_prod->quantity - $quantity;

             $this->utilities->updateData('tbl_cart_temp_chd', array('quantity'=>$total_quantity),array('user_uniq_id'=>$user_id, 'prod_id'=>$id));
             $return_qnt = $quantity;
        }
        $returnArray = array(
          'quantity' => $quantity,
          'status' => $plusMinus
        );

        echo json_encode($returnArray);

    }



    public function get_cart_bag()
    {
        
            $ip_addr = $this->ip_model->identify_to_user();
            $cart_item_list = $this->fontend_model->getCartItemList($ip_addr);

            $data['header'] = '<h5 class="modal-title" id="cartModalLabel">You have '.count($cart_item_list) .' items in your bag</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>';

            // echo '<pre>';
            // print_r($cart_item_list); exit;
            $baseurl = base_url();
            $data['body'] = '';
            $subtotal = 0;
            foreach($cart_item_list as $value):
            $subtotal += $value->quantity*$value->base_price;    
            $data['body'] .= '<div class="media" id="media'.$value->id.'">
              <a href="'.$baseurl.'fontend/details/'.$value->id.'"><img src="'.$baseurl.'assets/images/upload/thrum/'.$value->path.'" alt="NEW Microsoft Surface Go" width="50" height="50"></a>
              <div class="media-body">
                <a href="'.$baseurl.'fontend/details/'.$value->id.'" title="'.$value->prod_name.'">'.$value->prod_name.'</a>
                <div class="input-spinner input-spinner-sm">
                  <input type="number" class="form-control form-control-sm" value="'.$value->quantity.'" min="1" max="999">
                  <div class="btn-group-vertical">
                    <button type="button" class="btn btn-light"><i class="fa fa-chevron-up"></i></button>
                    <button type="button" class="btn btn-light"><i class="fa fa-chevron-down"></i></button>
                  </div>
                </div>
                x <span class="price">TK '.$value->base_price.'</span>
                <button type="button" quantity="'.$value->quantity.'" path="'.$baseurl.'" id="'.$value->id.'" class="close delete_item" aria-label="Close"><i class="fa fa-trash"></i></button>
              </div>
            </div>';
            endforeach;
            

            $data['footer'] = '<div class="box-total">
            <h6>Subotal: <span class="price">TK '.number_format($subtotal,2).'</span></h6>
            <a href="'.base_url().'fontend/cart" class="btn btn-success">VIEW CART</a>
          </div>';
          
          echo json_encode($data);
    }



    public function delete_item(){
      
      $ip_addr = $this->ip_model->identify_to_user();
      $where = array('user_uniq_id'=>$ip_addr);
      
      $prod_id = $this->input->post('id', TRUE);
      $user_id = $this->utilities->findByAttribute('tbl_cart_temp_mst',$where)->id;

      $deleteWhere = array('prod_id'=>$prod_id, 'user_uniq_id'=>$user_id);


      $status = $this->utilities->deleteRowByAttribute('tbl_cart_temp_chd', $deleteWhere);
      $returnStatus = FALSE;
      if($status){
        
        $returnStatus = TRUE;
      }
      echo $returnStatus;
      

    }


    public function pr($data)
    {

            echo '<pre>';
            print_r($data);
            exit;
    }

    





    

   

    
    
}

?>