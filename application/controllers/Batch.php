<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';




class Batch extends BaseController
{

    /**
     * This is default constructor of the class
     */
	public $controller = "batch";
	public $pageTitle = 'Batch Management';
	public $pageShortName = 'Batch';
	
    public function __construct()
    {
        parent::__construct();
		$this->load->model('utilities');
        $this->load->model('batch_model');
        $this->load->model('product_model');
        $this->isLoggedIn(); 
		 $menu_key = 'batch';
         $baseID = $this->input->get('baseID',TRUE);
		 $result = $this->loadThisForAccess($this->role,$baseID,$menu_key);
		 if ($result != true) 
		 {
			 redirect('access');
		 }
		 
			
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
            
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
			
	        $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = 'List';
			
            $data['result'] = $this->batch_model->getAllBatches();

            //$this->pr($data);
			
		    $this->load->view('includes/header', $this->global);
			$this->load->view($this->controller.'/index', $data);
			$this->load->view('includes/footer');
			
		
		
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
            $this->load->helper('form');
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewBatch';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;		
            
            $getSerialNumber = $this->utilities->generateSerialNum('tbl_batch_list', 'batch_id');

            $data['serial_num'] = $getSerialNumber;

            $data['category'] = $this->batch_model->dropdownFromProductWithCondition();

            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addNew',$data);
            $this->load->view('includes/footer');
        
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewBatch()
    {
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('product','Product','trim|required|max_length[255]|xss_clean');
        $this->form_validation->set_rules('batch_id','Batch ID','trim|required|max_length[255]|xss_clean');
        $this->form_validation->set_rules('base_price','Base Price','trim|required|max_length[255]|xss_clean');
        $this->form_validation->set_rules('selling_price','Selling Price','trim|required|max_length[255]|xss_clean');

        if($this->form_validation->run() == FALSE):

            $this->addNew();    

        else:    
        $baseID = $this->input->get('baseID', TRUE);

        $product = $this->input->post('product');
        $batch_id = $this->input->post('batch_id');
        $base_price = $this->input->post('base_price');
        $selling_price = $this->input->post('selling_price');
        $discount_price = $this->input->post('discount_price');
        $active = $this->input->post('active');
        
        $IdInfo = array('prod_id'=>$product,'batch_id'=>$batch_id, 'base_price'=>$base_price, 'selling_price'=>$selling_price, 'discount_price'=>$discount_price, 'active'=>$active, 'insertedBy'=>$this->vendorId, 'insertedOn'=>date('Y-m-d H:i:s'));

        $result = $this->utilities->insertData($IdInfo,'tbl_batch_list');

        if($result > 0)
        {
            $this->session->set_flashdata('success', 'New Batch created successfully');
        }
        else
        {
            $this->session->set_flashdata('error', 'Batch creation failed');
        }
        redirect($this->controller.'?baseID='.$baseID);

        endif;
                
    }




    public function editOld($id = NULL){
            $this->load->helper('form');
            $baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'updateBatch';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;			

            $data['category'] = $this->batch_model->dropdownFromProductWithCondition();

            $data['selectAll'] = $this->utilities->findResultByAttribute('tbl_batch_list', array('id'=>$id));
            
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/editBatch',$data);
            $this->load->view('includes/footer');
        

    }




    function updateBatch()
    {
        
        $baseID = $this->input->get('baseID', TRUE);
        

        $product = $this->input->post('product');
        //$batch_id = $this->input->post('batch_id');
        $base_price = $this->input->post('base_price');
        $selling_price = $this->input->post('selling_price');
        $discount_price = $this->input->post('discount_price');
        $active = $this->input->post('active');

        $id = $this->input->post('existId');

       
        
        
        $IdInfo = array('prod_id'=>$product, 'base_price'=>$base_price, 'selling_price'=>$selling_price, 'discount_price'=>$discount_price, 'active'=>$active, 'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));

        $result = $this->utilities->updateData('tbl_batch_list', $IdInfo, array('id'=>$id));


        if($result > 0)
        {
            $this->session->set_flashdata('success', 'Batch Updated successfully');
        }
        else
        {
            $this->session->set_flashdata('error', 'Batch Updated failed');
        }
        redirect($this->controller.'?baseID='.$baseID);
                
    }





    public function docFileUpload() {
		if(isset($_FILES['fileUpload'])){
			$IMAGE_NAME = '';
			$errors= array();
			$file_name = $_FILES['fileUpload']['name'];
			$file_size =$_FILES['fileUpload']['size'];
			$file_tmp =$_FILES['fileUpload']['tmp_name'];
			$file_type=$_FILES['fileUpload']['type'];
			$tmp = explode('.', $file_name);
			$file_ext = end($tmp);

			$extensions= array("jpeg","jpg","png","pdf","doc","docx");
			
			if(in_array($file_ext,$extensions)=== false){
			   $errors[]="extension not allowed, please choose a JPEG or PNG file.";
			}
			
			if($file_size > 2097152){
			   $errors[]='File size must be extensions 2 MB';
			}
			
			$targetDir = "assets/images/upload";

            $imageNewName = time();

			if(empty($errors)==true){
			   move_uploaded_file($file_tmp, $targetDir.'/'.$imageNewName);
			$IMAGE_NAME =  $imageNewName;
			}else{
			$IMAGE_NAME =  'NOT_FOUND';
			}

			return $IMAGE_NAME;
		 }
		
	 }




    /**
     * This function is used to add new user to the system
     */
    function addSubCat($id)
    {
          
			$baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewCat';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;	
            $data['parent_id']	= $id;	
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addSubCat',$data);
            $this->load->view('includes/footer');
        
    }


    /**
     * This function is used to add new user to the system
     */
    function addSubSubCat($id)
    {
          
			$baseID = $this->input->get('baseID', TRUE);
			$this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
			
			
            $this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'addNewCat';
			$data['shortName'] = $this->pageShortName;
            $data['boxTitle'] = $this->pageShortName .' Details';
            $data['baseID'] = $baseID;	
            $data['parent_id']	= $id;	
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/addSubSubCat',$data);
            $this->load->view('includes/footer');
        
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editcat($id = NULL)
    {
			$baseID = $this->input->get('baseID', TRUE);
			
            if($id == null)
            {
                redirect($this->controller.'?baseID='.$baseID);
            }
            
            $this->load->model('menu_model');
		    $this->global['menu'] =  $this->menu_model->getMenu($this->role);
            $data['userInfo'] = $this->agent_model->getCatInfo($id);
			
			$this->global['pageTitle'] = 'ecommerce : '. $this->pageTitle;
	        $data['pageTitle'] = $this->pageTitle;
			$data['controller'] = $this->controller;
			$data['action'] = 'updatecat';
			$data['shortName'] = $this->pageShortName;
			$data['boxTitle'] = $this->pageShortName .' Details';
			
			
            $this->load->view('includes/header', $this->global);
            $this->load->view($this->controller.'/editcat', $data);
            $this->load->view('includes/footer');
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function updatecat()
    {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
			
			 $baseID = $this->input->get('baseID', TRUE);
			 
			 //print_r($unit_name); die();
            		
			//$this->form_validation->set_rules('unit_code','Unit Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('name','Agent Name','trim|required|max_length[255]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editcat($id);
            }
            else
            {
                
			    $name = $this->input->post('name');
                
                $active = $this->input->post('active');
                
                $IDInfo = array();
                
               	
                $IDInfo = array( 'name'=>$name,
				'active'=>$active,'updatedBy'=>$this->vendorId, 'updatedOn'=>date('Y-m-d H:i:s'));
                
                $result = $this->utilities->updateData('tbl_category_list',$IDInfo, array('id'=>$id));
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Category updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category  update failed');
                }
                
				
				redirect($this->controller.'?baseID='.$baseID);
            }
        
    }




    public function deletecat($id = NULL){
        $baseID = $this->input->get('baseID', TRUE);
        $getItem  = $this->agent_model->getDeleteList($id);

        if(!empty($getItem)){

            foreach($getItem as $va){
                $this->utilities->deleteRowByAttribute('tbl_category_list', array('id'=>$va->id));
            }
        }

        $this->utilities->deleteRowByAttribute('tbl_category_list', array('id'=>$id));

        redirect($this->controller.'?baseID='.$baseID);
    }


    public function pr($data)
    {

            echo '<pre>';
            print_r($data);
            exit;
    }


    
    
}

?>