-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: logistic
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(90) NOT NULL DEFAULT '',
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('01dppa208f5qhjakbrnlp9f9bbg156o2','127.0.0.1',1541528904,_binary '__ci_last_regenerate|i:1541528904;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('0bd5mosctugvknl37rsurd5ck4hok3km','127.0.0.1',1541534674,_binary '__ci_last_regenerate|i:1541534521;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('0f980cnika78hej7o2t3est0291j85jn','::1',1540375117,_binary '__ci_last_regenerate|i:1540374995;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('0f9isbag81bbcvmsddkmtch6n0k7uf15','127.0.0.1',1541196227,_binary '__ci_last_regenerate|i:1541196227;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('0is78auo4ep4e81ctn31828otekkelhp','::1',1540374247,_binary '__ci_last_regenerate|i:1540374247;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:24:\"Bill Submit successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('0ou6vnbeb65gv7p1udn4bt74ut8i1a2b','127.0.0.1',1541849395,_binary '__ci_last_regenerate|i:1541849395;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('0vf20ar9f50qevct0t8pu4893rphub6u','::1',1540369790,_binary '__ci_last_regenerate|i:1540369790;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('11crfjp4laa94qogah2nr2n1s5q8sjn6','127.0.0.1',1541600238,_binary '__ci_last_regenerate|i:1541600238;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:33:\"New Category created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('19pvml01qveo3pvpn588fbt477t2171e','127.0.0.1',1541433285,_binary '__ci_last_regenerate|i:1541433257;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('1c3esrogvvgujaa08q76i0ph7una0hvq','127.0.0.1',1541522750,_binary '__ci_last_regenerate|i:1541522750;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('1i8gmlp4mbjsur11lhlkba96fjc4puhf','127.0.0.1',1541596834,_binary '__ci_last_regenerate|i:1541596834;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('1l6km6kp9cs4q7bdevb7vnt06p39teth','127.0.0.1',1541601546,_binary '__ci_last_regenerate|i:1541601546;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('1n4psdlphda9mc84e3b1nhudouptpan0','::1',1540358991,_binary '__ci_last_regenerate|i:1540358991;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('1o2miv9t4v92in7ghhl71833r8pp88q9','127.0.0.1',1541603008,_binary '__ci_last_regenerate|i:1541603008;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('1p66j75ll6738fig4qg75akpdmee674q','::1',1540371092,_binary '__ci_last_regenerate|i:1540371092;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('1p8olgjihm6k75i9jo2s8ulm8sc34psr','127.0.0.1',1541342280,_binary '__ci_last_regenerate|i:1541342280;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('1pjfrmoucg5vkd38od4g1l7o45qm9vqd','127.0.0.1',1541685705,_binary '__ci_last_regenerate|i:1541685705;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('1svjnnv8pu3jrpge0berd53pp9pc8spe','127.0.0.1',1541679971,_binary '__ci_last_regenerate|i:1541679771;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('1t8ev1339cn592rvl3vuupcvvvfe2bej','127.0.0.1',1541679927,_binary '__ci_last_regenerate|i:1541679927;'),('21rsa4nk3fu8g5kcud4okpv235j57nea','127.0.0.1',1541349115,_binary '__ci_last_regenerate|i:1541349115;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('23bkhhqkrgvna9umdatf6d6l4e1thqmf','127.0.0.1',1541601847,_binary '__ci_last_regenerate|i:1541601847;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('297oo96qcijr4eeq6hqcgdn4kq9gvlkd','127.0.0.1',1541349378,_binary '__ci_last_regenerate|i:1541349115;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('2amahrg4h4taloer88ji175j34tsc4u3','127.0.0.1',1541519833,_binary '__ci_last_regenerate|i:1541519833;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('2g70sgbe5hefd9dht31idhgca66tg5lm','127.0.0.1',1541781177,_binary '__ci_last_regenerate|i:1541781154;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('31c7bti7vchcj57qtcgbcgh0295bl0j1','127.0.0.1',1541847278,_binary '__ci_last_regenerate|i:1541847278;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('3532aks53kev61rhq1nsdcimc9asemq0','127.0.0.1',1541227225,_binary '__ci_last_regenerate|i:1541227186;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('39lg4e4s0mli53adv5pulekf4jvi0maa','127.0.0.1',1541850204,_binary '__ci_last_regenerate|i:1541850204;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('3e3vf5a7kn9953k8srqmcmfdhnuvkdfg','127.0.0.1',1541530084,_binary '__ci_last_regenerate|i:1541530084;'),('3f3rdcn0ue5v8uvggks09ale8jg5d9ng','127.0.0.1',1541534521,_binary '__ci_last_regenerate|i:1541534521;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('3gua7c9e7l7cqmflh11b5shdt6riorhe','127.0.0.1',1541689218,_binary '__ci_last_regenerate|i:1541689218;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('3q8phdj4st9f5ke1cs4vfhq24m1bf7ia','127.0.0.1',1541766996,_binary '__ci_last_regenerate|i:1541766851;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('43pns4j1tm30060mkj0t7nn6iv7dehh2','127.0.0.1',1541594587,_binary '__ci_last_regenerate|i:1541594587;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('4ile4tqhcec7a9aveh9cipjjnqsa0t2s','127.0.0.1',1541522603,_binary '__ci_last_regenerate|i:1541522603;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('4l8bv9omoabasthabselmima4hgbk0bd','::1',1540376905,_binary '__ci_last_regenerate|i:1540376905;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('4o778v5aldjheog9sd8j980mbi1j2j0s','::1',1540358348,_binary '__ci_last_regenerate|i:1540358348;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('4tc9f2k26cj0rvk6qg3nb4282jvaki71','127.0.0.1',1541778845,_binary '__ci_last_regenerate|i:1541778845;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('4uijkjunv79ir32j00usg13v96q9c7pl','::1',1540367217,_binary '__ci_last_regenerate|i:1540367217;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('5akroqm25ecmf68fq7aqgktq6d2o7rui','127.0.0.1',1541777616,_binary '__ci_last_regenerate|i:1541777616;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:30:\"New Batch created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('5m85kom27e187r81f0296gjbta6hhqv7','127.0.0.1',1541688496,_binary '__ci_last_regenerate|i:1541688496;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('66vluqv2aedbtedvh9e0eurgub933j27','127.0.0.1',1541779461,_binary '__ci_last_regenerate|i:1541779461;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('6afte63feom2md6vclekddltu0q6vnck','127.0.0.1',1541519529,_binary '__ci_last_regenerate|i:1541519529;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('6b1ivu5l1rr68rjd1jsduplj32a3dr20','127.0.0.1',1541767320,_binary '__ci_last_regenerate|i:1541767320;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('6fc524drc1juj4fcouvq1crsdm8cpd4h','127.0.0.1',1541188330,_binary '__ci_last_regenerate|i:1541188330;'),('6qsupstb9ijn0jb229sft44fq830dt1e','127.0.0.1',1541341513,_binary '__ci_last_regenerate|i:1541341513;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('6vp3kgh968n7tqfa90q53fj4p9faf6ik','127.0.0.1',1541187815,_binary '__ci_last_regenerate|i:1541187815;'),('753tejmru6vb6en14f489l3g93vcsv6a','127.0.0.1',1541531586,_binary '__ci_last_regenerate|i:1541531586;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('77rllgsne980rb7a54fgm0kenq94r6db','127.0.0.1',1541767002,_binary '__ci_last_regenerate|i:1541767002;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('7j5ivsaqkp0llquq2rfr206v1uq29ic3','127.0.0.1',1541341204,_binary '__ci_last_regenerate|i:1541341204;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('7rsumdjuocctvne791l9dj1g3upp6544','::1',1540358848,_binary '__ci_last_regenerate|i:1540358848;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('7tfr3vligqoknlujjvi2qk475q740k7e','127.0.0.1',1541265234,_binary '__ci_last_regenerate|i:1541265234;'),('7vomuacah0obrt7cvdfp9m7t20k39rm3','127.0.0.1',1541523927,_binary '__ci_last_regenerate|i:1541523927;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('83i57ipfm8thbctjncct1tq1f0qmugal','127.0.0.1',1541692090,_binary '__ci_last_regenerate|i:1541692090;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('8o30cr9vr9skug2acoavr6qgmeph274f','127.0.0.1',1541193796,_binary '__ci_last_regenerate|i:1541193796;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('8opo0h4d6iq95pq61sf3qj5d38489ut8','::1',1540371397,_binary '__ci_last_regenerate|i:1540371397;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('8ttr4hr77a19ejna0ovm2d4ak6i3e6kj','127.0.0.1',1541433257,_binary '__ci_last_regenerate|i:1541433257;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('9f8s3jc8j3ashk4t72mjl3qr1c4dqure','127.0.0.1',1541691438,_binary '__ci_last_regenerate|i:1541691438;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('9fifnkvr4rp3pi945glnn0806cbu5v88','::1',1540358679,_binary '__ci_last_regenerate|i:1540358679;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('9kugrc0mf9ht15ansg7ub7e4kllon4p8','127.0.0.1',1541196227,_binary '__ci_last_regenerate|i:1541196227;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('9ldp0bogc9s60t7gcs8cff935ne0g6e9','127.0.0.1',1541851937,_binary '__ci_last_regenerate|i:1541851870;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('9m4remm04i4uj9qtuvj8dtn0anp4pqd1','127.0.0.1',1541851548,_binary '__ci_last_regenerate|i:1541851548;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('a2hecv5ocqm7u62dnfpt3bh25r80af4o','127.0.0.1',1541348462,_binary '__ci_last_regenerate|i:1541348462;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('a57v35vqioi3rqm2jpkdrglt6biidmak','127.0.0.1',1541847680,_binary '__ci_last_regenerate|i:1541847680;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('acko550s0n72nnqopijuj4p3j44e32of','127.0.0.1',1541691758,_binary '__ci_last_regenerate|i:1541691758;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('aev0347703t8k7ajf7mb6tbhikraini8','127.0.0.1',1541850895,_binary '__ci_last_regenerate|i:1541850895;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('aev68o2p3ettlf0o4q8bp7l81g9kaql9','127.0.0.1',1541778170,_binary '__ci_last_regenerate|i:1541778170;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('ahpue52t2jsac4m8nbpa2g4v9nvv6c1v','127.0.0.1',1541690387,_binary '__ci_last_regenerate|i:1541690387;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('b0aud65ofqo62a7o3rt4cte2sg4d88b2','::1',1540377615,_binary '__ci_last_regenerate|i:1540377615;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('b625kfrubr6hd6rvhu9ar0hbeqoh3evp','127.0.0.1',1541604605,_binary '__ci_last_regenerate|i:1541604605;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('bbbjquam91pcunil4tctfj4iesbcnij1','127.0.0.1',1541598259,_binary '__ci_last_regenerate|i:1541598259;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('bbu6abjaema15a4j4dkl6d58ne8mc9u5','127.0.0.1',1541522603,_binary '__ci_last_regenerate|i:1541522603;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;success|s:30:\"New Role created successfully.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('bd9l14a7dqd65f9f11f32l04i6a4t1rn','127.0.0.1',1541347557,_binary '__ci_last_regenerate|i:1541347557;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('bdf1al26979e81f64q0f1mg0fu460nsk','::1',1540370785,_binary '__ci_last_regenerate|i:1540370785;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;success|s:34:\"New Frowarder created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('brfqmg7fi9ppjg2illpman5q6ocs4s2k','127.0.0.1',1541846471,_binary '__ci_last_regenerate|i:1541846471;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('cc6k06lti10ogtv5rk6reet88d65vpas','127.0.0.1',1541851212,_binary '__ci_last_regenerate|i:1541851212;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('cjbms2i147ncg5oq09vspc1qea1lut57','127.0.0.1',1541692706,_binary '__ci_last_regenerate|i:1541692706;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('ckp40glcf7nme4dkd9of1ueqh68tspr0','127.0.0.1',1541522434,_binary '__ci_last_regenerate|i:1541522434;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('d46t2ta2av7s5jf3c991hfnl5vopn0jk','127.0.0.1',1541533376,_binary '__ci_last_regenerate|i:1541533376;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:33:\"New Category created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('dapnneg24d3laia8t244aab9bjutstbd','127.0.0.1',1541693037,_binary '__ci_last_regenerate|i:1541693037;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('dcgahe4pnn808aknkki21hn5dq7fbl6p','127.0.0.1',1541770987,_binary '__ci_last_regenerate|i:1541770987;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('df1sfvgtvt3of9qqd0s996iht96um6lv','127.0.0.1',1541605647,_binary '__ci_last_regenerate|i:1541605622;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('dguj8987vdifdulaeoaqojsqpmjilep7','::1',1540373907,_binary '__ci_last_regenerate|i:1540373907;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('dpsse610ebqbk4rpimsseobm76skq73t','127.0.0.1',1541518670,_binary '__ci_last_regenerate|i:1541518670;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('dptjnmiorik8cva7se34kaiferdn1gr8','127.0.0.1',1541690715,_binary '__ci_last_regenerate|i:1541690715;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:33:\"New Category created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('e4o0hhpsesqcmlt2q76dq8rc6grrk9ae','127.0.0.1',1541532453,_binary '__ci_last_regenerate|i:1541532453;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:30:\"New Agent created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('e6hogmng83g0532js1obah7ka16s2hcp','127.0.0.1',1541519164,_binary '__ci_last_regenerate|i:1541519164;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('enu609ruj2r1vhgpi970ma2g9ps7k3jg','127.0.0.1',1541336555,_binary '__ci_last_regenerate|i:1541336555;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('f3l16c4rh2qenn7k62dnngsrre1b8a5f','127.0.0.1',1541771434,_binary '__ci_last_regenerate|i:1541771434;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:30:\"New Batch created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('f62vem054mbctc5n5fc2h5lrqikukssv','127.0.0.1',1541599287,_binary '__ci_last_regenerate|i:1541599287;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('f8gfc3s3k33h943k7ff8l6tdlo3f4djs','127.0.0.1',1541521202,_binary '__ci_last_regenerate|i:1541521202;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('f8r3qcji3q1bg367ue4qu4i4c39hu5ck','127.0.0.1',1541769471,_binary '__ci_last_regenerate|i:1541769471;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('fb4430ftqg2b2b1usuq6185ch7d971ah','127.0.0.1',1541783230,_binary '__ci_last_regenerate|i:1541783230;'),('fb8edgp1ncjtqrjo4q2o9diqgsc4lb3u','127.0.0.1',1541688182,_binary '__ci_last_regenerate|i:1541688182;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('fbbgk257ij9hvti19nq9hf16rr3v7fc1','127.0.0.1',1541343472,_binary '__ci_last_regenerate|i:1541343472;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('fdaf7t7a4eknvpctumu7hanfqji6bibh','127.0.0.1',1541188726,_binary '__ci_last_regenerate|i:1541188726;'),('fesirkh8kojus6l43lca2arecbs1tjvb','::1',1540358041,_binary '__ci_last_regenerate|i:1540358041;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('flcv5kej2qgp102i7jq1h5eh96pv0kk8','127.0.0.1',1541532759,_binary '__ci_last_regenerate|i:1541532759;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('fnb7eb265mgspbibb3t87ca3bbpakecb','172.17.100.157',1540357174,_binary '__ci_last_regenerate|i:1540357146;userId|s:2:\"18\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:7:\"Al-amin\";username|s:6:\"alamin\";agent_id|s:1:\"3\";extension|N;email|s:16:\"alamin@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('g146natene37l961pcub2f6eb53apd3n','127.0.0.1',1541850593,_binary '__ci_last_regenerate|i:1541850593;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('g61vsm1b3jua707ecgesacsbhhpq03dh','127.0.0.1',1541341818,_binary '__ci_last_regenerate|i:1541341818;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('ge3jg8tosbapo0880mmt6v0gevpbe66p','127.0.0.1',1541848369,_binary '__ci_last_regenerate|i:1541848369;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('gfcn7ntn30jgknk7p4eb25c5543hc8fh','127.0.0.1',1541690013,_binary '__ci_last_regenerate|i:1541690013;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('gg5gesjau9rj3jsh1cibes54s90egdpn','::1',1540369381,_binary '__ci_last_regenerate|i:1540369381;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('gj8lhkln2t3kc2qvrh3mqk86hrceqfi5','::1',1540361575,_binary '__ci_last_regenerate|i:1540361575;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('glhoga5arvhv265hgu6jr5f0sl7vlgm0','127.0.0.1',1541684202,_binary '__ci_last_regenerate|i:1541684202;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('gous1sbdnvg7tpdkhd1tehg1gg1id46u','127.0.0.1',1541779156,_binary '__ci_last_regenerate|i:1541779156;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('gui24p8j6dvmcatdg0hdcl4lseq3crfb','127.0.0.1',1541597149,_binary '__ci_last_regenerate|i:1541597149;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('h5cvptom58b3656efiba8unuepe1mmgn','127.0.0.1',1541528476,_binary '__ci_last_regenerate|i:1541528476;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('heqed4vbrg6dgrhonqqes6felar3j8k7','::1',1540377615,_binary '__ci_last_regenerate|i:1540377615;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('ho0h7nc014ddki4v127a2cphefihnt1k','127.0.0.1',1541771750,_binary '__ci_last_regenerate|i:1541771750;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('htgb2ib1ce38oq1q3ib0p1eup73u9db4','127.0.0.1',1541845770,_binary '__ci_last_regenerate|i:1541845683;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;success|s:30:\"New Role created successfully.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('i2jae15d5jfgbb0jfaend6flspt3v59s','127.0.0.1',1541686264,_binary '__ci_last_regenerate|i:1541686264;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('i3oo9ti35ohp0lg1auep1m94qhoipqsf','127.0.0.1',1541531216,_binary '__ci_last_regenerate|i:1541531216;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('ia4mtie5l4s6mgatspvsl8n7lfqpspc2','127.0.0.1',1541605542,_binary '__ci_last_regenerate|i:1541605453;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('ibj9orc4ornba0biooqobhgsebs3omkr','127.0.0.1',1541598602,_binary '__ci_last_regenerate|i:1541598602;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('if2ajhcg7foinulcguj7e24alm139c2n','127.0.0.1',1541534220,_binary '__ci_last_regenerate|i:1541534220;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:33:\"New Category created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('if3l8lhu96ccdve9q1kk09feung4vhl9','::1',1540360955,_binary '__ci_last_regenerate|i:1540360955;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('jkogpei9ep77d1tcab4doflu3vnjhb26','127.0.0.1',1541595514,_binary '__ci_last_regenerate|i:1541595514;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:29:\"Category updated successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('jli6eo6lmaor4mp9fvhm905un5mcq578','127.0.0.1',1541530852,_binary '__ci_last_regenerate|i:1541530852;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('jmdjs8i91f3jki5rvam0rr8vtcse8rol','::1',1540371762,_binary '__ci_last_regenerate|i:1540371762;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('jpeb1sik1kt45ekvpbmvpo4kd5sbit2e','127.0.0.1',1541518324,_binary '__ci_last_regenerate|i:1541518324;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('jtrqqgolqsl8ftirnd4oqd66l5uuokuo','::1',1540375767,_binary '__ci_last_regenerate|i:1540375767;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('k1qccjkmc57rg577gbn03t3s96guf986','127.0.0.1',1541528168,_binary '__ci_last_regenerate|i:1541528168;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('k356tetvhk8cdeo4s3oh092lqfptfuhm','127.0.0.1',1541598907,_binary '__ci_last_regenerate|i:1541598907;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('koul3hg0bk4kesqumdfs0dr7af2guf17','127.0.0.1',1541527514,_binary '__ci_last_regenerate|i:1541527514;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('kqh9j71n7hbk5uuqv8pf42epdlfk8q2k','127.0.0.1',1541602230,_binary '__ci_last_regenerate|i:1541602230;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('kvhv7jv0mgjbm5jgjf5e5r9mduccasdd','127.0.0.1',1541529847,_binary '__ci_last_regenerate|i:1541529847;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('kvspuml2nj65lt51ncc6t4mp8tiomm41','127.0.0.1',1541683590,_binary '__ci_last_regenerate|i:1541683590;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('l0lgddr4jessnmrdnek85ugdameo2rj3','127.0.0.1',1541845776,_binary '__ci_last_regenerate|i:1541845776;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('l185mpghh7upo5dgr00pnmm6lgdib23t','127.0.0.1',1541530156,_binary '__ci_last_regenerate|i:1541530156;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('le6tueflgl2362fq23k788nequhm72pd','127.0.0.1',1541604907,_binary '__ci_last_regenerate|i:1541604907;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('lec220450nltqhfrbbekalpra1vp8e9g','127.0.0.1',1541594078,_binary '__ci_last_regenerate|i:1541594078;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('lfj38gb9eff3jqt5275834isc3m51qos','127.0.0.1',1541683899,_binary '__ci_last_regenerate|i:1541683899;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('lokmp1ksbqq3d1aqaajkc2liie68irha','127.0.0.1',1541517768,_binary '__ci_last_regenerate|i:1541517768;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('lp16gpto9kdlkori7s9ra32teggo097t','127.0.0.1',1541685388,_binary '__ci_last_regenerate|i:1541685388;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('lqdg6lb2egfjeg859mi569lmv5gqukkr','127.0.0.1',1541847987,_binary '__ci_last_regenerate|i:1541847987;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:26:\"Stock Updated successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('lrrpnmmgff384omf78pgm8po9kfutfn9','127.0.0.1',1541523622,_binary '__ci_last_regenerate|i:1541523622;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('lu5gb5chah7htpdbv1b2p1j80ggcp9kj','127.0.0.1',1541692391,_binary '__ci_last_regenerate|i:1541692391;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('lumgu30lbrolktm0lpn9shuu42tjhkg5','127.0.0.1',1541533912,_binary '__ci_last_regenerate|i:1541533912;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('m213k3j85li4ac5m6i5kc9p0ct29h8fk','127.0.0.1',1541783110,_binary '__ci_last_regenerate|i:1541783110;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('m308f00gelmn981jnm63qe7vkc4uj4dv','::1',1540374995,_binary '__ci_last_regenerate|i:1540374995;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('m4ovkbn56f94idvdint85h6g7gv44cu8','127.0.0.1',1541769946,_binary '__ci_last_regenerate|i:1541769946;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('m59nipolmmcvteb9u6b8forlgjjldav4','::1',1540375425,_binary '__ci_last_regenerate|i:1540375425;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('mg58s0dqkd3m3dg56sgn12ouu4rqkf0k','127.0.0.1',1541527827,_binary '__ci_last_regenerate|i:1541527827;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('miltmfrih0srrfaar081sd7qkc3qe9bi','127.0.0.1',1541604236,_binary '__ci_last_regenerate|i:1541604236;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('mj7l5uvmvq34fi29drlnbspd86rtbdtj','127.0.0.1',1541520744,_binary '__ci_last_regenerate|i:1541520744;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('n1k06p7kc3nsnbs4865datb7j3ptl8l5','127.0.0.1',1541683135,_binary '__ci_last_regenerate|i:1541683135;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('n6fsl7khnm2s1ua3f3pvrbcqc4n0rgnr','::1',1540368868,_binary '__ci_last_regenerate|i:1540368868;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('nan6m6ia7rn0ksp3os1713i7c33t7dl0','127.0.0.1',1541595004,_binary '__ci_last_regenerate|i:1541595004;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:26:\"Agent updated successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('nfjmqk2k0j5u9k8t56jqunh19tdckgbe','127.0.0.1',1541188587,_binary '__ci_last_regenerate|i:1541188330;'),('nlelnmh3c1tqgv09g4ne6brnb64mr7ga','::1',1540358922,_binary '__ci_last_regenerate|i:1540358848;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('nlgmtqqkm4gvlnh2khebgtv7109ni05v','127.0.0.1',1541605277,_binary '__ci_last_regenerate|i:1541605277;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('nodngmtir21uve8vp9hp42o8lpc5ptr5','127.0.0.1',1541428506,_binary '__ci_last_regenerate|i:1541428506;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('nuat6901tt5it5ehputjqi2f2doflb8i','::1',1540368566,_binary '__ci_last_regenerate|i:1540368566;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('o637cf77bmi685bcsafo782k939ggpjn','127.0.0.1',1541778502,_binary '__ci_last_regenerate|i:1541778502;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('oaru99l29d9s9l5peh9bdofpjoqdgric','127.0.0.1',1541340592,_binary '__ci_last_regenerate|i:1541340592;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('ol76fc1qpociq1pt06315oedl0m884g6','127.0.0.1',1541517465,_binary '__ci_last_regenerate|i:1541517465;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('ovnl5glm7hv1fa41ai0d1t7k6l99m1lb','127.0.0.1',1541851870,_binary '__ci_last_regenerate|i:1541851870;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('p094488kji0h9qou4fcgtm4q6ig0065m','::1',1540358337,_binary '__ci_last_regenerate|i:1540358337;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('p2fll00akf33gtcekqg90c4d8gtb9gbg','127.0.0.1',1541342963,_binary '__ci_last_regenerate|i:1541342963;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('pc0pv123jhslb8c3e6s1vd14n9tdeuoc','127.0.0.1',1541694159,_binary '__ci_last_regenerate|i:1541694159;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('peue5d7dg0jfo6kmuev681aae92pqi21','127.0.0.1',1541520252,_binary '__ci_last_regenerate|i:1541520252;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('pfh385fie2os9ftj036c8l6c6vkjtqrg','127.0.0.1',1541846089,_binary '__ci_last_regenerate|i:1541846089;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('pgjr71gdevcjiohd78fkq7m3dhm3ht7v','::1',1540375073,_binary '__ci_last_regenerate|i:1540375073;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('pote6oerv3k6sfcbnnnt9lqb5gro35pm','127.0.0.1',1541195469,_binary '__ci_last_regenerate|i:1541195469;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('q02dl13hrbm7vd30nfqsaj7h7na13tdo','::1',1540367950,_binary '__ci_last_regenerate|i:1540367950;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('q8b57pe931d619ccs3o7pd220oqs5geq','127.0.0.1',1541770681,_binary '__ci_last_regenerate|i:1541770681;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('qf063md7v1k17c3qurd52cl655offl64','127.0.0.1',1541769125,_binary '__ci_last_regenerate|i:1541769125;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('qmtjacbssamkuurdtbet2uc9tev18qep','127.0.0.1',1541849732,_binary '__ci_last_regenerate|i:1541849732;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('qos1avq95g38gqok57du23hnet5pschm','127.0.0.1',1541429790,_binary '__ci_last_regenerate|i:1541429790;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('riao5hi9h9o82r4usj5ndpsgsnp91l03','::1',1540371705,_binary '__ci_last_regenerate|i:1540371705;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('riqcuamvgm0kul3cb6rdvtjeqdqtfqdq','127.0.0.1',1541194272,_binary '__ci_last_regenerate|i:1541194272;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:7:\"icddr,b\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('rld82n3tec5cngfh502qtm29e23v4kja','127.0.0.1',1541781187,_binary '__ci_last_regenerate|i:1541781187;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('rt87hmjo2tnds2gaiikutj5ld0hq7dbr','127.0.0.1',1541768239,_binary '__ci_last_regenerate|i:1541768239;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('rta8dn4abiif7jqdkgirh3kfpn1kf1gn','127.0.0.1',1541533072,_binary '__ci_last_regenerate|i:1541533072;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:33:\"New Category created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('s34cmhu8cn9psplpfoprmto63774bthn','127.0.0.1',1541687348,_binary '__ci_last_regenerate|i:1541687348;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('s7es8l22egmohtsn7kvajssdd18i1249','::1',1540357740,_binary '__ci_last_regenerate|i:1540357740;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('s8i7b5tg6ut4o3uvgqhq3ve0f4vph05h','::1',1540373044,_binary '__ci_last_regenerate|i:1540373044;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('shprg297fu3vtjpid7e30tnb1gugoqu8','127.0.0.1',1541846963,_binary '__ci_last_regenerate|i:1541846963;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('si7bu556nfkq8t29ira7eoa0odpmgl0f','127.0.0.1',1541532084,_binary '__ci_last_regenerate|i:1541532084;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('sle6bta2guuqups5dc2s1elssrj0s0vp','127.0.0.1',1541605453,_binary '__ci_last_regenerate|i:1541605453;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;success|s:30:\"New Role created successfully.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('st3f3tafs6a1j9aj1ah1auhiktp8dlmu','127.0.0.1',1541339522,_binary '__ci_last_regenerate|i:1541339522;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('stin7i2odefre8h2vt1ciot9ifmfi4vl','::1',1540370410,_binary '__ci_last_regenerate|i:1540370410;'),('svci59m2mg9g3e0qfo0euta8502ors02','127.0.0.1',1541689603,_binary '__ci_last_regenerate|i:1541689603;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('t1mg0ftvs0mv5rot4auhhj60q86j2kj7','127.0.0.1',1541781583,_binary '__ci_last_regenerate|i:1541781583;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:30:\"New Batch created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('t5s4o4dtlbauejt2ff12uojr3fkdftrm','127.0.0.1',1541265228,_binary '__ci_last_regenerate|i:1541265228;userId|s:1:\"1\";role|s:1:\"1\";roleText|s:20:\"System Administrator\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"admin\";agent_id|s:1:\"0\";extension|N;email|s:16:\"admin@icddrb.org\";employee_id|N;isLoggedIn|b:1;'),('tfpombg4ol74euoto5monbi4c3rkdrvc','127.0.0.1',1541339842,_binary '__ci_last_regenerate|i:1541339842;'),('tok9mfjhgp22d75rjo9u5ulikjblv828','127.0.0.1',1541523298,_binary '__ci_last_regenerate|i:1541523298;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('tsmh2idfiqsgkjpj5dvbacjfn71t9il2','127.0.0.1',1541849073,_binary '__ci_last_regenerate|i:1541849073;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('tum4c6pl6hn2n7o68h4q3m7huiq2us5s','::1',1540359298,_binary '__ci_last_regenerate|i:1540359298;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('u66oiqlr0ib7knnsanoqjnmmmomcv28o','127.0.0.1',1541684531,_binary '__ci_last_regenerate|i:1541684531;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('u76o6qj7452q90s1ik1ekv4j5b7cp0bt','127.0.0.1',1541599936,_binary '__ci_last_regenerate|i:1541599936;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('uajqetvmfop79njjq3498e9qhhpctqk8','127.0.0.1',1541691022,_binary '__ci_last_regenerate|i:1541691022;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('ud57fsr392uhr0ho4b8emk875kbjo5c2','127.0.0.1',1541605622,_binary '__ci_last_regenerate|i:1541605622;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('v1mcidgbtl9gq7i01ijsnbsp52m1h8kf','127.0.0.1',1541694159,_binary '__ci_last_regenerate|i:1541694159;userId|s:1:\"2\";role|s:1:\"7\";roleText|s:6:\"Client\";system_user|s:1:\"1\";name|s:10:\"Fahad Khan\";username|s:5:\"fahad\";agent_id|s:1:\"0\";extension|N;email|s:20:\"fahadcse07@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('v2uf175ftd6nhb5g53ufgp6av905a2ds','::1',1540361264,_binary '__ci_last_regenerate|i:1540361264;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;'),('vio7links1hs4grkhp4t8i6h9k7bdies','127.0.0.1',1541530498,_binary '__ci_last_regenerate|i:1541530498;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;success|s:30:\"New Agent created successfully\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('vkk9m1nev5m29n6n513ejan0et11ao5b','::1',1540357400,_binary '__ci_last_regenerate|i:1540357400;userId|s:2:\"17\";role|s:1:\"8\";roleText|s:5:\"Agent\";system_user|s:1:\"1\";name|s:17:\"Md. Khayrul Hasan\";username|s:7:\"khayrul\";agent_id|s:1:\"2\";extension|N;email|s:21:\"khayrul.web@gmail.com\";employee_id|N;isLoggedIn|b:1;');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_advance`
--

DROP TABLE IF EXISTS `tbl_advance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_advance` (
  `fld_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fld_doc_id` int(10) unsigned DEFAULT NULL,
  `fld_date` datetime DEFAULT NULL,
  `fld_amount` decimal(10,2) DEFAULT NULL,
  `fld_origin_amount` decimal(10,2) DEFAULT '0.00',
  `fld_used` tinyint(1) DEFAULT '0',
  `fld_remarks` varchar(255) DEFAULT NULL,
  `fld_active` tinyint(1) DEFAULT NULL,
  `insertedOn` datetime DEFAULT NULL,
  `insertedBy` int(10) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `updatedBy` int(10) DEFAULT NULL,
  PRIMARY KEY (`fld_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_advance`
--

LOCK TABLES `tbl_advance` WRITE;
/*!40000 ALTER TABLE `tbl_advance` DISABLE KEYS */;
INSERT INTO `tbl_advance` VALUES (1,1,'2018-10-17 00:00:00',0.00,0.00,0,'dfdsf',2,'2018-10-11 15:45:23',2,NULL,NULL),(2,5,'2018-10-18 00:00:00',5000.00,5000.00,0,'2121',2,'2018-10-17 09:27:19',2,NULL,NULL),(3,6,'2018-10-26 00:00:00',5012.00,5012.00,0,'',2,'2018-10-17 13:47:04',2,NULL,NULL),(4,10,'2018-10-11 00:00:00',5000.00,5000.00,0,'aa',2,'2018-10-22 12:47:27',2,NULL,NULL);
/*!40000 ALTER TABLE `tbl_advance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_agent_list`
--

DROP TABLE IF EXISTS `tbl_agent_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_agent_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `address` text,
  `mobile` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `remarks` text,
  `active` tinyint(3) unsigned NOT NULL,
  `insertedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(10) unsigned NOT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_agent_list`
--

LOCK TABLES `tbl_agent_list` WRITE;
/*!40000 ALTER TABLE `tbl_agent_list` DISABLE KEYS */;
INSERT INTO `tbl_agent_list` VALUES (1,'Salvation','N3242334','GPO Box 128, 68, Shaheed Tajuddin Ahmed Sarani','017240400251','3541','fahadcse07@gmail.com','fahad',1,'2018-05-22 01:17:55',1,'2018-05-22 01:17:55',1),(2,'Karim & Sons','w35343','GPO Box 128, 68, Shaheed Tajuddin Ahmed Sarani','017240400251','3541','fahadcse07@gmail.com','',1,'2018-08-07 04:01:48',1,NULL,NULL),(3,'The Hub','1236548','Dhaka, Bangladesh','01717455555','01717455555','hub@gmail.com','Remarks',1,'2018-08-30 05:24:39',1,NULL,NULL);
/*!40000 ALTER TABLE `tbl_agent_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_base_setup_chd`
--

DROP TABLE IF EXISTS `tbl_base_setup_chd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_base_setup_chd` (
  `chd_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mst_id` int(10) unsigned NOT NULL,
  `name` varchar(500) NOT NULL,
  `agent_id` tinyint(3) unsigned DEFAULT NULL,
  `percentage` decimal(10,2) DEFAULT NULL,
  `minimum` decimal(10,0) DEFAULT NULL,
  `maximum` decimal(10,0) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `insertBy` int(10) NOT NULL,
  `insertOn` datetime NOT NULL,
  `updateBy` int(10) DEFAULT NULL,
  `updateOn` datetime DEFAULT NULL,
  `fld_air` varchar(50) DEFAULT NULL,
  `fld_sea` varchar(50) DEFAULT NULL,
  `fld_land` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`chd_id`),
  KEY `fk_mst_chd_id` (`mst_id`),
  CONSTRAINT `fk_mst_chd_id` FOREIGN KEY (`mst_id`) REFERENCES `tbl_base_setup_mst` (`mst_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_base_setup_chd`
--

LOCK TABLES `tbl_base_setup_chd` WRITE;
/*!40000 ALTER TABLE `tbl_base_setup_chd` DISABLE KEYS */;
INSERT INTO `tbl_base_setup_chd` VALUES (1,12,'Red',NULL,NULL,NULL,NULL,1,0,'0000-00-00 00:00:00',2,'2018-11-07 21:35:14',NULL,NULL,NULL),(2,12,'Blue',NULL,NULL,NULL,NULL,1,0,'0000-00-00 00:00:00',2,'2018-11-07 21:35:20',NULL,NULL,NULL),(3,13,'Small',NULL,NULL,NULL,NULL,1,0,'0000-00-00 00:00:00',2,'2018-11-07 21:34:31',NULL,NULL,NULL),(4,13,'Mediam',NULL,NULL,NULL,NULL,1,0,'0000-00-00 00:00:00',2,'2018-11-07 21:34:47',NULL,NULL,NULL),(5,13,'Large',NULL,NULL,NULL,NULL,1,0,'0000-00-00 00:00:00',2,'2018-11-07 21:34:55',NULL,NULL,NULL),(6,13,'Extra Large',NULL,NULL,NULL,NULL,1,0,'0000-00-00 00:00:00',2,'2018-11-07 21:35:07',NULL,NULL,NULL),(7,12,'Black',NULL,NULL,NULL,NULL,1,0,'0000-00-00 00:00:00',2,'2018-11-07 21:35:36',NULL,NULL,NULL),(8,12,'White',NULL,NULL,NULL,NULL,1,0,'0000-00-00 00:00:00',2,'2018-11-07 21:35:46',NULL,NULL,NULL),(9,12,'Orange',NULL,NULL,NULL,NULL,1,0,'0000-00-00 00:00:00',2,'2018-11-07 21:35:56',NULL,NULL,NULL),(10,12,'Green',NULL,NULL,NULL,NULL,1,0,'0000-00-00 00:00:00',2,'2018-11-07 21:36:04',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_base_setup_chd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_base_setup_mst`
--

DROP TABLE IF EXISTS `tbl_base_setup_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_base_setup_mst` (
  `mst_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `insertOn` datetime NOT NULL,
  `insertBy` int(10) NOT NULL,
  `updateOn` datetime DEFAULT NULL,
  `updateBy` int(10) DEFAULT NULL,
  PRIMARY KEY (`mst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_base_setup_mst`
--

LOCK TABLES `tbl_base_setup_mst` WRITE;
/*!40000 ALTER TABLE `tbl_base_setup_mst` DISABLE KEYS */;
INSERT INTO `tbl_base_setup_mst` VALUES (12,'Color',1,'2018-11-07 21:33:53',2,NULL,NULL),(13,'Size',1,'2018-11-07 21:34:18',2,NULL,NULL);
/*!40000 ALTER TABLE `tbl_base_setup_mst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_batch_list`
--

DROP TABLE IF EXISTS `tbl_batch_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_batch_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` varchar(255) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `base_price` double(10,2) NOT NULL,
  `selling_price` double(10,2) NOT NULL,
  `discount_price` double(10,2) DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL,
  `insertedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(10) unsigned DEFAULT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_batch_list`
--

LOCK TABLES `tbl_batch_list` WRITE;
/*!40000 ALTER TABLE `tbl_batch_list` DISABLE KEYS */;
INSERT INTO `tbl_batch_list` VALUES (51,'SCM/CUS/-1/18',3,455.00,600.00,122.00,1,'2018-11-09 15:48:28',2,'2018-11-09 15:48:28',2),(52,'SCM/CUS/-52/18',3,1212.00,3232.00,111.00,1,'2018-11-09 15:50:51',2,'2018-11-09 15:50:51',2),(53,'SCM/CUS/-53/18',1,1212.00,3232.00,111.00,1,'2018-11-09 15:54:57',2,'2018-11-09 15:54:57',2),(54,'SCM/CUS/-54/18',3,1212.00,3232.00,111.00,1,'2018-11-09 15:49:55',2,'2018-11-09 15:49:55',2),(55,'SCM/CUS/-55/18',3,55.00,56.00,55.50,1,'2018-11-09 16:36:47',2,NULL,NULL);
/*!40000 ALTER TABLE `tbl_batch_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_billing_chd`
--

DROP TABLE IF EXISTS `tbl_billing_chd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_billing_chd` (
  `id` int(10) NOT NULL,
  `mst_id` int(10) NOT NULL,
  `charges_mst_id` int(10) NOT NULL,
  `charges_chd_id` int(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `a_order_number` varchar(50) DEFAULT NULL,
  `insertOn` datetime NOT NULL,
  `insertBy` int(10) NOT NULL,
  `updateOn` datetime DEFAULT NULL,
  `updateBy` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_billing_chd`
--

LOCK TABLES `tbl_billing_chd` WRITE;
/*!40000 ALTER TABLE `tbl_billing_chd` DISABLE KEYS */;
INSERT INTO `tbl_billing_chd` VALUES (1,1,3,5,54.00,'45','2018-10-11 15:48:03',17,'2018-10-11 16:05:59',17),(2,1,4,17,52.00,'0','2018-10-11 15:48:03',17,'2018-10-11 16:05:59',17),(3,1,5,13,54.00,'0','2018-10-11 15:48:03',17,'2018-10-11 16:05:59',17),(7,3,3,5,125.00,'665','2018-10-11 16:53:42',17,'2018-10-11 16:56:03',17),(8,3,4,17,444.00,'0','2018-10-11 16:53:42',17,'2018-10-11 16:56:03',17),(9,3,5,13,5.00,'0','2018-10-11 16:53:43',17,'2018-10-11 16:56:03',17),(10,4,3,4,43.00,'454','2018-10-16 16:40:56',17,'2018-10-16 16:44:13',17),(11,4,3,6,34.00,'44','2018-10-16 16:40:56',17,'2018-10-16 16:44:13',17),(12,4,4,17,434.00,'0','2018-10-16 16:40:57',17,'2018-10-16 16:44:13',17),(13,4,5,12,434.00,'0','2018-10-16 16:40:57',17,'2018-10-16 16:44:13',17),(14,5,3,5,12.00,'4545','2018-10-16 17:16:02',17,'2018-10-17 08:58:40',17),(15,5,4,17,4545.00,'0','2018-10-16 17:16:02',17,'2018-10-17 08:58:40',17),(16,5,5,13,454.00,'0','2018-10-16 17:16:02',17,'2018-10-17 08:58:40',17),(17,5,3,4,125.00,'452','2018-10-17 08:58:28',17,'2018-10-17 08:58:40',17),(18,5,3,6,4521.00,'654','2018-10-17 08:58:28',17,'2018-10-17 08:58:40',17),(19,5,3,7,125.00,'542','2018-10-17 08:58:29',17,'2018-10-17 08:58:40',17),(20,5,3,8,454.00,'454','2018-10-17 08:58:29',17,'2018-10-17 08:58:40',17),(21,5,3,9,125.00,'3254','2018-10-17 08:58:29',17,'2018-10-17 08:58:40',17),(22,5,5,14,12.00,'0','2018-10-17 08:58:29',17,'2018-10-17 08:58:41',17),(23,5,5,11,52.00,'0','2018-10-17 08:58:29',17,'2018-10-17 08:58:41',17),(24,5,5,12,12.00,'0','2018-10-17 08:58:29',17,'2018-10-17 08:58:41',17),(25,6,3,6,5421.00,'45213','2018-10-17 09:29:24',17,'2018-10-24 15:58:32',17),(26,6,4,17,5412.00,'0','2018-10-17 09:29:24',17,'2018-10-24 15:58:32',17),(27,6,5,13,65221.00,'0','2018-10-17 09:29:24',17,'2018-10-24 15:58:32',17),(28,6,5,14,400.00,'0','2018-10-17 09:30:06',17,'2018-10-24 15:58:32',17),(30,6,5,12,200.00,'0','2018-10-17 09:31:48',17,'2018-10-24 15:58:32',17),(31,6,3,7,2351.00,'5421','2018-10-17 09:32:52',17,'2018-10-24 15:58:32',17),(32,6,5,11,1254.00,'0','2018-10-17 09:38:11',17,'2018-10-24 15:58:32',17),(33,7,3,5,2154.00,'1245','2018-10-17 13:47:52',18,'2018-10-17 13:49:58',18),(34,7,4,18,121.00,'0','2018-10-17 13:47:52',18,'2018-10-17 13:49:58',18),(35,7,5,12,541.00,'0','2018-10-17 13:47:52',18,'2018-10-17 13:49:59',18),(36,7,5,13,5421.00,'0','2018-10-17 13:48:24',18,'2018-10-17 13:49:59',18),(38,8,4,17,555.00,'0','2018-10-21 14:58:55',17,'2018-10-22 11:49:29',17),(39,8,5,11,50000000.00,'0','2018-10-21 14:58:55',17,'2018-10-22 11:49:29',17),(40,8,3,5,555.00,'44','2018-10-21 15:06:50',17,'2018-10-22 11:49:29',17),(42,8,5,13,41.00,'0','2018-10-21 15:08:05',17,'2018-10-22 11:49:29',17),(43,8,3,7,520.00,'51','2018-10-21 15:26:46',17,'2018-10-22 11:49:29',17),(44,9,3,4,50.00,'12','2018-10-22 12:28:17',17,NULL,NULL),(45,9,4,17,60.00,'0','2018-10-22 12:28:17',17,NULL,NULL),(46,9,5,11,70.00,'0','2018-10-22 12:28:17',17,NULL,NULL),(47,10,3,4,50.00,'12','2018-10-22 12:49:31',17,'2018-10-22 15:44:32',17),(48,10,3,6,80.00,'55','2018-10-22 12:49:31',17,'2018-10-22 15:44:32',17),(49,10,4,17,60.00,'0','2018-10-22 12:49:31',17,'2018-10-22 15:44:32',17),(50,10,5,11,5001.00,'0','2018-10-22 12:49:31',17,'2018-10-22 15:44:32',17),(51,10,5,12,500.00,'0','2018-10-22 12:49:31',17,'2018-10-22 15:44:32',17),(52,11,3,4,5000.00,'532w3','2018-10-22 15:56:23',18,NULL,NULL),(53,11,3,6,500.00,'34534','2018-10-22 15:56:24',18,NULL,NULL),(54,11,4,18,500.00,'0','2018-10-22 15:56:24',18,NULL,NULL),(55,11,5,11,4000.00,'0','2018-10-22 15:56:24',18,NULL,NULL),(56,12,3,4,12.00,'12','2018-10-23 11:36:40',17,'2018-10-23 11:36:53',17),(57,12,4,17,54.00,'0','2018-10-23 11:36:40',17,'2018-10-23 11:36:53',17),(58,12,5,12,65.00,'0','2018-10-23 11:36:40',17,'2018-10-23 11:36:53',17),(59,13,3,4,555.00,'565','2018-10-23 13:36:02',17,NULL,NULL),(60,13,4,17,50.00,'0','2018-10-23 13:36:02',17,NULL,NULL),(61,13,5,11,60.00,'0','2018-10-23 13:36:02',17,NULL,NULL),(62,6,5,12,45.00,'0','2018-10-24 15:58:32',17,NULL,NULL);
/*!40000 ALTER TABLE `tbl_billing_chd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_billing_mst`
--

DROP TABLE IF EXISTS `tbl_billing_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_billing_mst` (
  `id` int(10) unsigned NOT NULL,
  `doc_id` int(10) unsigned DEFAULT NULL,
  `billing_id` varchar(50) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `vate` decimal(10,2) DEFAULT NULL,
  `grnd_total` decimal(10,2) DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `advance` decimal(10,2) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0=save, 1=submit+pending, 2=approve, 3=reject ',
  `insertedOn` datetime NOT NULL,
  `insertedBy` int(10) NOT NULL,
  `updatedOn` datetime NOT NULL,
  `updatedBy` int(10) NOT NULL,
  `payment_receive_remarks` text,
  `payment_receive_status` tinyint(1) NOT NULL DEFAULT '0',
  `payment_receive_date_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_billing_mst`
--

LOCK TABLES `tbl_billing_mst` WRITE;
/*!40000 ALTER TABLE `tbl_billing_mst` DISABLE KEYS */;
INSERT INTO `tbl_billing_mst` VALUES (1,1,'SCM/CUS/1','icddr,b',24.00,184.00,184.00,0.00,'',2,'2018-10-11 15:48:03',17,'2018-10-15 11:55:50',2,'dddddddddddddddddddddddddddd',0,NULL),(3,2,'SCM/CUS/2','icddr,b',86.10,660.10,660.10,0.00,'',2,'2018-10-11 16:53:42',17,'2018-10-15 11:53:54',2,'helllo test',1,NULL),(4,4,'SCM/CUS/4','icddr,b',141.75,1086.75,1086.75,0.00,NULL,1,'2018-10-16 16:40:56',17,'2018-10-16 16:44:12',17,NULL,0,NULL),(5,3,'SCM/CUS/5','icddr,b',1565.55,12002.55,12002.55,0.00,NULL,1,'2018-10-16 17:16:02',17,'2018-10-17 08:58:39',17,NULL,0,NULL),(6,5,'SCM/CUS/6','icddr,b',16060.80,96364.80,91364.80,5000.00,'4555 555',1,'2018-10-17 09:29:24',17,'2018-10-24 15:58:32',17,NULL,0,NULL),(7,6,'SCM/CUS/7','icddr,b',1235.55,9472.55,4460.55,5012.00,'',2,'2018-10-17 13:47:52',18,'2018-10-17 14:41:46',2,NULL,0,NULL),(10,10,'SCM/CUS/8','icddr,b',1138.20,6829.20,1829.20,5000.00,'',2,'2018-10-22 12:49:31',17,'2018-10-24 15:40:31',2,NULL,0,NULL),(11,15,'SCM/CUS/11','icddr,b',2000.00,12000.00,12000.00,0.00,'',2,'2018-10-22 15:56:23',18,'2018-10-22 15:57:17',2,'dddddddddddddddddddd',1,NULL),(12,8,'SCM/CUS/12','icddr,b',26.20,157.20,157.20,0.00,'',2,'2018-10-23 11:36:40',17,'2018-10-23 11:46:11',2,'ss ddd',1,'2018-10-19 18:15:00'),(13,16,'SCM/CUS/13','icddr,b',133.00,798.00,798.00,0.00,'',2,'2018-10-23 13:36:02',17,'2018-10-23 13:36:24',2,'dsdfsdf',1,NULL);
/*!40000 ALTER TABLE `tbl_billing_mst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_category_list`
--

DROP TABLE IF EXISTS `tbl_category_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_category_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `level` tinyint(1) NOT NULL,
  `url` varchar(255) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL,
  `insertedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(10) unsigned DEFAULT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_category_list`
--

LOCK TABLES `tbl_category_list` WRITE;
/*!40000 ALTER TABLE `tbl_category_list` DISABLE KEYS */;
INSERT INTO `tbl_category_list` VALUES (25,'Man',0,0,'addSubCat',1,'2018-11-07 15:24:05',17,'2018-11-07 15:24:05',2),(37,'M-Pant',25,1,'addSubSubCat',1,'2018-11-08 13:42:22',17,'2018-11-08 13:42:22',2),(39,'Women',0,0,'addSubCat',1,'2018-11-09 16:33:43',17,'2018-11-09 16:33:43',2),(41,'W-Tee Shirt',39,1,'addSubSubCat',1,'2018-11-08 13:43:10',17,'2018-11-08 13:43:10',2),(42,'M-red pant',37,2,'addSubSubSubCat',1,'2018-11-08 13:42:34',2,'2018-11-08 13:42:34',2),(43,'M-Tee Shirt',37,2,'addSubSubSubCat',1,'2018-11-08 13:42:42',2,'2018-11-08 13:42:42',2),(44,'M-Under Wear',37,2,'addSubSubSubCat',1,'2018-11-08 13:42:51',2,'2018-11-08 13:42:51',2),(45,'W-Under wear',41,2,'addSubSubSubCat',1,'2018-11-08 13:44:19',2,'2018-11-08 13:44:19',2),(46,'longi',41,2,'addSubSubSubCat',1,'2018-11-08 15:58:50',2,NULL,NULL),(47,'Mantion',41,2,'addSubSubSubCat',1,'2018-11-08 16:00:08',2,NULL,NULL),(48,'Abir',0,0,'addSubCat',1,'2018-11-08 16:01:14',2,NULL,NULL),(49,'Khan',48,1,'addSubSubCat',1,'2018-11-08 16:01:21',2,NULL,NULL),(50,'Fishy',49,2,'addSubSubSubCat',1,'2018-11-08 16:02:07',2,NULL,NULL),(51,'Hossain',0,0,'addSubCat',1,'2018-11-09 16:33:20',2,NULL,NULL),(52,'Md',51,1,'addSubSubCat',1,'2018-11-09 16:33:28',2,NULL,NULL);
/*!40000 ALTER TABLE `tbl_category_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_currency_type`
--

DROP TABLE IF EXISTS `tbl_currency_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_currency_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(255) NOT NULL,
  `bdt` decimal(10,2) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `insertedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(11) NOT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_currency_type`
--

LOCK TABLES `tbl_currency_type` WRITE;
/*!40000 ALTER TABLE `tbl_currency_type` DISABLE KEYS */;
INSERT INTO `tbl_currency_type` VALUES (1,'TK','TK',1.00,1,'2018-08-30 07:55:28',1,'2018-08-30 07:55:28',1),(2,'USD','$',80.00,1,'2018-08-30 07:59:26',1,'2018-08-30 07:59:26',1),(3,'EURO','€',97.81,1,'2018-08-30 07:54:48',1,'2018-08-30 07:54:48',1);
/*!40000 ALTER TABLE `tbl_currency_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_email_send_service`
--

DROP TABLE IF EXISTS `tbl_email_send_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_email_send_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject` text NOT NULL,
  `email_copy` varchar(255) DEFAULT NULL,
  `email_from` varchar(255) NOT NULL,
  `email_to` varchar(255) NOT NULL DEFAULT '',
  `email_body` text,
  `insert_by` varchar(255) NOT NULL DEFAULT '',
  `insert_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_email_send_service`
--

LOCK TABLES `tbl_email_send_service` WRITE;
/*!40000 ALTER TABLE `tbl_email_send_service` DISABLE KEYS */;
INSERT INTO `tbl_email_send_service` VALUES (1,'Email notification for assigning C&F agent .',NULL,'info@icddrb.org','fahadcse07@gmail.com','Dear icddr,b Team<br><p>The subject reference bill have been authorized and forwarded to Finance. You are requested to contact with our Finance team, if required.</p><br><p>Best regards</p><p>Kazi Emran Hossain</p><p>Supply Chain Management</p><p>68, Shaheed Tajuddin Ahmed Sarani, Mohakhali, Dhaka-1212, Bangladesh </p><p>Tel: +880-2-9827001-10 |Ext.4413| |Direct:+88 02 58810002|Cell:+8801709-649296  </p><p>imran@icddrb.org  |Web: www.icddrb.org   </p>','1','2018-10-24 15:40:32'),(2,'Email notification for assigning C&F agent .',NULL,'info@icddrb.org','fahadcse07@gmail.com','Dear icddr,b Team<br><p>The subject reference bills have been rejected. You are requested to contact with the undersigned and collect the bill for revision.</p><br><p>Best regards</p><p>Kazi Emran Hossain</p><p>Supply Chain Management</p><p>68, Shaheed Tajuddin Ahmed Sarani, Mohakhali, Dhaka-1212, Bangladesh </p><p>Tel: +880-2-9827001-10 |Ext.4413| |Direct:+88 02 58810002|Cell:+8801709-649296  </p><p>imran@icddrb.org  |Web: www.icddrb.org   </p>','1','2018-10-24 15:42:56');
/*!40000 ALTER TABLE `tbl_email_send_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_frowarder_list`
--

DROP TABLE IF EXISTS `tbl_frowarder_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_frowarder_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` text,
  `active` tinyint(3) unsigned NOT NULL,
  `insertedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(10) unsigned NOT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `shipment_mode` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_frowarder_list`
--

LOCK TABLES `tbl_frowarder_list` WRITE;
/*!40000 ALTER TABLE `tbl_frowarder_list` DISABLE KEYS */;
INSERT INTO `tbl_frowarder_list` VALUES (1,'Air Arabia(G9)','',1,'2018-10-24 08:42:13',1,NULL,NULL,'AIR'),(2,'Air Asia Cargo(QZ/AK/D7)','',1,'2018-10-24 08:42:30',1,NULL,NULL,'AIR'),(3,'Air Hong Kong(LD)','',1,'2018-10-24 08:42:45',1,NULL,NULL,'AIR'),(4,'Air India(AI)','',1,'2018-10-24 08:43:00',1,NULL,NULL,'AIR'),(5,'Atlantic Airways(RC)','',1,'2018-10-24 08:43:15',1,NULL,NULL,'AIR'),(6,'Biman Bangladesh(BG)','',1,'2018-10-24 08:43:28',1,NULL,NULL,'AIR'),(7,'Cathay Pacific Airways (CX)','',1,'2018-10-24 08:43:42',1,NULL,NULL,'AIR'),(8,'China Airlines(CI)','',1,'2018-10-24 08:43:56',1,NULL,NULL,'AIR'),(9,'China Cargo Airlines(CK)','',1,'2018-10-24 08:44:09',1,NULL,NULL,'AIR'),(10,'Egypt Air(MS)','',1,'2018-10-24 08:44:22',1,NULL,NULL,'AIR'),(11,'Emirates Airlines (EK)','',1,'2018-10-24 08:44:40',1,NULL,NULL,'AIR'),(12,'Hong Kong Air Cargo','',1,'2018-10-24 08:44:53',1,NULL,NULL,'AIR'),(13,'Japan Airlines(JL)','',1,'2018-10-24 08:47:17',1,'2018-10-24 08:47:17',1,'AIR'),(14,'Qatar Airways(QR)','',1,'2018-10-24 08:47:28',1,NULL,NULL,'AIR'),(15,'Saudi Airlines Cargo(SV)','',1,'2018-10-24 08:47:43',1,NULL,NULL,'AIR'),(16,'Singapore Airlines(SQ)','',1,'2018-10-24 08:47:56',1,NULL,NULL,'AIR'),(17,'Thai Airways (TG)','',1,'2018-10-24 08:48:10',1,NULL,NULL,'AIR'),(18,'Turkish Airlines','',1,'2018-10-24 08:48:26',1,NULL,NULL,'AIR'),(19,'United Cargo(UA)','',1,'2018-10-24 08:48:40',1,NULL,NULL,'AIR'),(20,'DSV Air & Sea Ltd','',1,'2018-10-24 08:48:51',1,NULL,NULL,'AIR'),(21,'DHL Global Forwarding','',1,'2018-10-24 08:49:03',1,NULL,NULL,'AIR'),(22,'Hellman Worldwide Logistics','',1,'2018-10-24 08:49:17',1,NULL,NULL,'AIR'),(23,'Monem Business District','',1,'2018-10-24 08:49:30',1,NULL,NULL,'AIR'),(24,'Reliable Express Ltd.','',1,'2018-10-24 08:50:25',1,NULL,NULL,'AIR'),(25,'Sunmoon Shipping Intl','',1,'2018-10-24 08:50:43',1,NULL,NULL,'AIR'),(26,'Kuehne Nagel','',1,'2018-10-24 08:50:57',1,NULL,NULL,'AIR'),(27,'SG Logistics Private Ltd','',1,'2018-10-24 08:51:11',1,NULL,NULL,'AIR'),(28,'Expeditors Limited','',1,'2018-10-24 08:51:22',1,NULL,NULL,'AIR'),(29,'Dart Global Logistics (Pvt) Ltd.','',1,'2018-10-24 08:51:33',1,NULL,NULL,'AIR'),(30,'Geodis Wilson Bangladesh Ltd.','',1,'2018-10-24 08:51:47',1,NULL,NULL,'AIR'),(31,'WAC Logistics Limited','',1,'2018-10-24 08:52:01',1,NULL,NULL,'AIR'),(32,'DHL Global Forwarding','',1,'2018-10-24 08:52:15',1,NULL,NULL,'AIR'),(33,'33. Topstar Cargo Systems Ltd','',1,'2018-10-24 08:52:27',1,NULL,NULL,'AIR'),(34,'Integrated Transportation Services Ltd','',1,'2018-10-24 08:52:40',1,NULL,NULL,'AIR'),(35,'AYZ Express Services','',1,'2018-10-24 08:53:10',1,NULL,NULL,'AIR'),(36,'Legend Shipping Service','',1,'2018-10-24 08:53:24',1,NULL,NULL,'AIR'),(37,'Air Alliance Ltd','',1,'2018-10-24 08:53:35',1,NULL,NULL,'AIR'),(38,'MAC-ABC Supply Chain Solutions (PVT) Ltd','',1,'2018-10-24 08:53:49',1,NULL,NULL,'AIR'),(39,'ACE Bangladesh Ltd','',1,'2018-10-24 08:54:00',1,NULL,NULL,'AIR'),(40,'Asia Star Logistics Ltd','',1,'2018-10-24 08:54:12',1,NULL,NULL,'AIR'),(41,'Badal and Company','',1,'2018-10-24 08:54:24',1,NULL,NULL,'AIR'),(42,'Jupiter Logistics Bangladesh Co, Ltd.','',1,'2018-10-24 08:54:38',1,NULL,NULL,'AIR'),(43,'Interocean Cargo Services Ltd.','',1,'2018-10-24 08:54:52',1,NULL,NULL,'AIR'),(44,'Multi Freight Ltd','',1,'2018-10-24 08:55:07',1,NULL,NULL,'AIR'),(45,'Agility Ltd','',1,'2018-10-24 08:55:19',1,NULL,NULL,'AIR'),(46,'Nippon Express','',1,'2018-10-24 08:55:34',1,NULL,NULL,'AIR'),(47,'UPS','',1,'2018-10-24 08:55:47',1,NULL,NULL,'AIR'),(48,'FedEx','',1,'2018-10-24 08:56:01',1,NULL,NULL,'AIR'),(49,'TNT','',1,'2018-10-24 08:56:13',1,NULL,NULL,'AIR'),(50,'DHL','',1,'2018-10-24 08:56:26',1,NULL,NULL,'AIR'),(51,'EMS','',1,'2018-10-24 08:56:37',1,NULL,NULL,'AIR'),(52,'Aramex','',1,'2018-10-24 08:56:51',1,NULL,NULL,'AIR'),(53,'MCC','',1,'2018-10-24 08:57:11',1,NULL,NULL,'SEA'),(54,'PIL','',1,'2018-10-24 08:57:25',1,NULL,NULL,'SEA'),(55,'COSCO','',1,'2018-10-24 08:57:39',1,NULL,NULL,'SEA'),(56,'YML','',1,'2018-10-24 08:57:50',1,NULL,NULL,'SEA'),(57,'OOCL','',1,'2018-10-24 08:58:04',1,NULL,NULL,'SEA'),(58,'APL','',1,'2018-10-24 08:58:14',1,NULL,NULL,'SEA'),(59,'HMM','',1,'2018-10-24 08:58:26',1,NULL,NULL,'SEA'),(60,'K-Line','',1,'2018-10-24 08:58:40',1,NULL,NULL,'SEA'),(61,'Wan Hai Lines','',1,'2018-10-24 08:59:10',1,NULL,NULL,'SEA'),(62,'Nippon – NYK','',1,'2018-10-24 08:59:22',1,NULL,NULL,'SEA'),(63,'Hanjin Shipping','',1,'2018-10-24 08:59:34',1,NULL,NULL,'SEA'),(64,'Hapag-Lloyd','',1,'2018-10-24 08:59:48',1,NULL,NULL,'SEA'),(65,'Evergreen Marine','',1,'2018-10-24 09:00:00',1,NULL,NULL,'SEA'),(66,'Samudera Shipping Line Ltd.','',1,'2018-10-24 09:00:14',1,NULL,NULL,'SEA'),(67,'Airtropolis Express(s) PTE LTD.','',1,'2018-10-24 09:00:27',1,NULL,NULL,'SEA'),(68,'File Shipping Line (PTE ) LTD.','',1,'2018-10-24 09:00:40',1,NULL,NULL,'SEA'),(69,'JBB Groupage PTE Ltd.','',1,'2018-10-24 09:00:52',1,NULL,NULL,'SEA'),(70,'Oceanair Inc','',1,'2018-10-24 09:01:06',1,NULL,NULL,'SEA'),(71,'DACHSER','',1,'2018-10-24 09:01:17',1,NULL,NULL,'SEA'),(72,'DIMOTRANS','',1,'2018-10-24 09:01:29',1,NULL,NULL,'SEA'),(73,'BRIZO GLOBAL LOGISTICS PVT. LTD.','',1,'2018-10-24 09:01:56',1,'2018-10-24 09:01:56',1,'SEA');
/*!40000 ALTER TABLE `tbl_frowarder_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_location`
--

DROP TABLE IF EXISTS `tbl_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_location` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL,
  `insertedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(10) unsigned NOT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_location`
--

LOCK TABLES `tbl_location` WRITE;
/*!40000 ALTER TABLE `tbl_location` DISABLE KEYS */;
INSERT INTO `tbl_location` VALUES (1,'Dhaka',1,'2017-07-24 13:43:34',1,'2017-07-24 09:43:34',1),(2,'Chattogram',1,'2018-05-22 07:05:59',1,'2018-05-22 07:05:59',1);
/*!40000 ALTER TABLE `tbl_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `menu_key` varchar(55) NOT NULL,
  `parent_menu_id` int(10) unsigned NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `menu_order` tinyint(3) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `icon` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `insert_by` int(10) unsigned NOT NULL DEFAULT '0',
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(10) unsigned DEFAULT '0',
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menu`
--

LOCK TABLES `tbl_menu` WRITE;
/*!40000 ALTER TABLE `tbl_menu` DISABLE KEYS */;
INSERT INTO `tbl_menu` VALUES (1,'Home','home',0,'dashboard',1,1,0,'fa fa-dashboard','test',0,'2018-05-10 09:16:35',1,'2018-05-10 05:16:35'),(3,'Master Setup','mt',0,'#',6,1,2,'fa fa-sitemap','',0,'2018-05-10 09:16:49',1,'2018-05-10 05:16:49'),(5,'Administration','adm',0,'#',7,1,1,'fa fa-shield','',0,'2018-05-10 09:17:13',1,'2018-05-10 05:17:13'),(6,'User Management','um',5,'user',1,1,0,'','',0,'2018-05-10 09:18:04',1,'2018-05-10 05:18:04'),(7,'Device Type','dt',3,'device_type',1,0,0,'','Device Type',0,'2018-05-22 06:21:53',1,'2018-05-22 06:21:53'),(9,'Status Type','st',3,'status_type',2,0,0,'','Status Type',0,'2018-05-22 06:22:00',1,'2018-05-22 06:22:00'),(14,'Menu Management','mm',5,'menu',2,1,1,'fa fa-plus-square','',0,'2018-05-10 09:19:04',1,'2018-05-10 05:19:04'),(49,'Role Management','rm',5,'role',3,1,0,'fa fa-diamond','Role Management',1,'2018-05-10 09:19:12',1,'2018-05-10 05:19:12'),(50,'Location','loc',3,'location',3,1,0,'','Location',1,'2018-05-22 07:05:12',1,'2018-05-22 07:05:12'),(51,'Currency','cur',3,'currency',4,1,0,'fa fa-users','Currency',1,'2018-05-22 06:04:45',0,'0000-00-00 00:00:00'),(52,'PI List','pi',3,'pi',5,1,0,'','PI List',1,'2018-05-22 06:22:54',0,'0000-00-00 00:00:00'),(55,'Frowarder List','frowar',3,'frowarder',8,1,0,'','Frowarder List',1,'2018-05-22 07:16:25',0,'0000-00-00 00:00:00'),(60,'Base Setup','bs',3,'base_setup',11,1,0,'fa fa-gear','All base setup',1,'2018-07-15 10:08:26',1,'2018-07-15 10:08:26'),(61,'Category','category',0,'category',4,1,0,'fa fa-certificate','Category ',1,'2018-11-06 15:11:46',0,'0000-00-00 00:00:00'),(62,'Product Setup','product',0,'product',5,1,0,'fa fa-shield','product',1,'2018-11-07 15:45:14',0,'0000-00-00 00:00:00'),(63,'Batch Production','batch',0,'batch',5,1,0,'fa fa-certificate','Add batch product ',1,'2018-11-09 15:51:59',0,'0000-00-00 00:00:00'),(64,'Stock','stock',0,'stock',5,1,0,'fa fa-certificate','Stock management',1,'2018-11-10 10:30:00',0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tbl_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pi_list`
--

DROP TABLE IF EXISTS `tbl_pi_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pi_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `remarks` text,
  `active` tinyint(3) unsigned NOT NULL,
  `insertedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(10) unsigned NOT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pi_list`
--

LOCK TABLES `tbl_pi_list` WRITE;
/*!40000 ALTER TABLE `tbl_pi_list` DISABLE KEYS */;
INSERT INTO `tbl_pi_list` VALUES (1,'Md. Fahad Khan','test',1,'2018-05-22 06:30:30',1,'2018-05-22 06:30:30',1),(2,'Sanyad','Sanyad',1,'2018-11-03 17:05:46',1,'2018-11-03 17:05:46',1),(3,'Khayrul','Khayrul',1,'2018-09-27 05:02:21',1,NULL,NULL);
/*!40000 ALTER TABLE `tbl_pi_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_products`
--

DROP TABLE IF EXISTS `tbl_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `path` varchar(45) DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL,
  `insertedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(10) unsigned DEFAULT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_products`
--

LOCK TABLES `tbl_products` WRITE;
/*!40000 ALTER TABLE `tbl_products` DISABLE KEYS */;
INSERT INTO `tbl_products` VALUES (1,44,1,6,'1541690620',1,'2018-11-08 15:46:22',2,NULL,NULL),(2,42,1,4,'1541688394',1,'2018-11-09 15:54:08',2,'2018-11-09 15:54:08',2),(3,44,9,6,'1541692354',1,'2018-11-09 15:54:17',2,'2018-11-09 15:54:17',2),(4,45,10,5,'1541692372',1,'2018-11-09 15:56:54',2,'2018-11-09 15:56:54',2),(5,46,9,5,'NOT_FOUND',1,'2018-11-09 16:34:50',2,NULL,NULL);
/*!40000 ALTER TABLE `tbl_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_progress_type`
--

DROP TABLE IF EXISTS `tbl_progress_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_progress_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `insertedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(11) NOT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_progress_type`
--

LOCK TABLES `tbl_progress_type` WRITE;
/*!40000 ALTER TABLE `tbl_progress_type` DISABLE KEYS */;
INSERT INTO `tbl_progress_type` VALUES (1,'Pre-Alert Received',1,1,'2018-05-23 03:10:04',1,'2018-05-23 03:10:04',1),(2,'Authorized Document Received',2,1,'2018-05-23 03:38:27',1,NULL,0),(3,'AWB/BL collected from Freight Forwarder',3,1,'2018-05-23 03:38:42',1,NULL,0),(4,'IGM Verification Completed',4,1,'2018-05-23 03:38:51',1,NULL,0),(5,'Entry Noting Complete',5,1,'2018-05-23 03:39:05',1,NULL,0),(6,'Assessment Completed',6,1,'2018-05-23 03:39:16',1,NULL,0),(7,'Duties/Levies paid to custom',7,1,'2018-05-23 03:39:25',1,NULL,0),(8,'Examination Completed',8,1,'2018-05-23 03:39:33',1,NULL,0),(9,'Released Order collected from Port Authority',9,1,'2018-05-23 03:39:43',1,NULL,0),(10,'Shipment Released and Delivered',10,1,'2018-05-23 03:39:53',1,NULL,0),(11,'Others',11,1,'2018-05-23 03:40:07',1,NULL,0);
/*!40000 ALTER TABLE `tbl_progress_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_progress_variance_type`
--

DROP TABLE IF EXISTS `tbl_progress_variance_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_progress_variance_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `progress_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `insertedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(11) NOT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_progress_variance_type`
--

LOCK TABLES `tbl_progress_variance_type` WRITE;
/*!40000 ALTER TABLE `tbl_progress_variance_type` DISABLE KEYS */;
INSERT INTO `tbl_progress_variance_type` VALUES (1,1,'Shipment not located in port yard',1,1,'2018-05-23 03:35:25',1,'2018-05-23 03:35:25',1),(2,1,'Shipment partially located',2,1,'2018-05-23 03:32:20',1,NULL,0),(3,3,'HAWB/BL issuance delayed by Forwarder',1,1,'2018-05-23 03:40:36',1,NULL,0),(4,4,'Wrong declaration of Consignee/Shipper in IGM',1,1,'2018-05-23 03:40:57',1,NULL,0),(5,4,'HAWB/BL not inserted in IGM',2,1,'2018-05-23 03:41:09',1,NULL,0),(6,4,'Miss-declaration of Package/Quantity/Weight in IGM',3,1,'2018-05-23 03:41:22',1,NULL,0),(7,5,'Custom Server Down',1,1,'2018-05-23 03:41:38',1,NULL,0),(8,5,'Document Marked by Custom',2,1,'2018-05-23 03:41:50',1,NULL,0),(9,5,'Red Marked by Custom',3,1,'2018-05-23 03:42:00',1,NULL,0),(10,6,'Document Marked by Custom',1,1,'2018-05-23 03:42:17',1,NULL,0),(11,6,'Red Marked by Custom',2,1,'2018-05-23 03:42:30',1,NULL,0),(12,6,'Wrong Declaration of Goods /HS Code',3,1,'2018-05-23 03:42:46',1,NULL,0),(13,6,'Clarification asked by Custom',4,1,'2018-05-23 03:42:59',1,NULL,0),(14,6,'Waiting for Brochure/Catalogue',5,1,'2018-05-23 03:43:10',1,NULL,0),(15,6,'Waiting for Import Permit/License',6,1,'2018-05-23 03:43:20',1,NULL,0),(16,7,'Duty/Taxes not paid due to Bank',1,1,'2018-05-23 03:43:36',1,NULL,0),(17,8,'Goods Not De-stuffed',1,1,'2018-05-23 03:43:50',1,NULL,0),(18,8,'Waiting for Brochure/Catalogue',2,1,'2018-05-23 03:44:01',1,NULL,0),(19,8,'Waiting for Import Permit/License',3,1,'2018-05-23 03:44:19',1,NULL,0),(20,10,'Unavoidable circumstances (Strike, Unrest)',1,1,'2018-05-23 03:44:35',1,NULL,0);
/*!40000 ALTER TABLE `tbl_progress_variance_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_roles`
--

DROP TABLE IF EXISTS `tbl_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text',
  `active` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `insert_by` int(10) unsigned NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(45) DEFAULT '0',
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`roleId`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_roles`
--

LOCK TABLES `tbl_roles` WRITE;
/*!40000 ALTER TABLE `tbl_roles` DISABLE KEYS */;
INSERT INTO `tbl_roles` VALUES (1,'System Administrator',1,'System Administrator',0,'2018-09-20 06:51:41','1','2018-09-20 06:51:41'),(2,'Mentee',1,'Mentee',0,'2018-08-28 05:44:59','1','2018-08-28 05:44:59'),(3,'Admin',1,'',0,'2017-10-14 14:18:34','1','2017-10-14 10:18:34'),(4,'Mentor (Level I)',1,'Mentor (Level I)',1,'2017-10-02 04:18:27','1','2017-10-02 00:18:27'),(5,'Mentor (Level II)',1,'Mentor (Level II)',1,'2017-10-02 04:19:16','1','2017-10-02 00:19:16'),(6,'Observer',1,'Observer',1,'2018-07-09 05:40:25','1','2018-07-09 05:40:25'),(7,'Client',1,'Role for Client',1,'2018-11-10 10:29:10','1','2018-11-10 10:29:10'),(8,'Agent',1,'Role for agent',1,'2018-11-10 10:29:30','1','2018-11-10 10:29:30');
/*!40000 ALTER TABLE `tbl_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_shipper_list`
--

DROP TABLE IF EXISTS `tbl_shipper_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_shipper_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `address` text,
  `mobile` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `remarks` text,
  `active` tinyint(3) unsigned NOT NULL,
  `insertedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(10) unsigned NOT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_shipper_list`
--

LOCK TABLES `tbl_shipper_list` WRITE;
/*!40000 ALTER TABLE `tbl_shipper_list` DISABLE KEYS */;
INSERT INTO `tbl_shipper_list` VALUES (1,'Fahad khan','N3242334','GPO Box 128, 68, Shaheed Tajuddin Ahmed Sarani','017240400251','3541','fahadcse07@gmail.com','test',1,'2018-05-23 08:32:40',1,NULL,NULL),(2,'Khayrul Hasan','N3243335','GPO Box 128, 68, Shaheed Tajuddin Ahmed Sarani','01781483634','01781483634','khayrul.web@gmail.com','This is dummy text',1,'2018-08-28 03:18:34',1,NULL,NULL);
/*!40000 ALTER TABLE `tbl_shipper_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_status`
--

DROP TABLE IF EXISTS `tbl_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_status` (
  `status_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_status`
--

LOCK TABLES `tbl_status` WRITE;
/*!40000 ALTER TABLE `tbl_status` DISABLE KEYS */;
INSERT INTO `tbl_status` VALUES (0,'No'),(1,'Yes');
/*!40000 ALTER TABLE `tbl_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_status_type`
--

DROP TABLE IF EXISTS `tbl_status_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_status_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `insertedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(11) NOT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_status_type`
--

LOCK TABLES `tbl_status_type` WRITE;
/*!40000 ALTER TABLE `tbl_status_type` DISABLE KEYS */;
INSERT INTO `tbl_status_type` VALUES (1,'Good',1,'2017-07-24 13:31:01',1,'2017-07-24 09:31:01',1),(2,'Faulty',1,'2017-07-24 17:12:17',1,'2017-07-24 13:12:17',1),(3,'Good/Faulty',1,'2017-07-24 17:12:46',1,'2017-07-24 13:12:46',1),(4,'Expired',1,'2017-07-24 17:13:09',1,'2017-07-24 13:13:09',1),(5,'In Use',1,'2017-07-24 17:13:28',1,'2017-07-24 13:13:28',1),(6,'Need to check',1,'2017-07-24 17:13:48',1,'2017-07-24 13:13:48',1),(7,'New',1,'2017-07-24 17:14:03',1,'2017-07-24 13:14:03',1),(8,'Other',1,'2017-07-24 13:14:16',1,NULL,0);
/*!40000 ALTER TABLE `tbl_status_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stock_list`
--

DROP TABLE IF EXISTS `tbl_stock_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stock_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) NOT NULL,
  `quantity` double(10,2) NOT NULL,
  `qnt_less` double(10,2) DEFAULT NULL,
  `qnt_damage` double(10,2) DEFAULT NULL,
  `qnt_return` double(10,2) DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL,
  `insertedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` int(10) unsigned DEFAULT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stock_list`
--

LOCK TABLES `tbl_stock_list` WRITE;
/*!40000 ALTER TABLE `tbl_stock_list` DISABLE KEYS */;
INSERT INTO `tbl_stock_list` VALUES (56,51,500.00,11.00,122.00,22.00,1,'2018-11-10 11:06:20',2,'2018-11-10 11:06:20',2),(57,55,454521.00,22.00,22.00,232.00,1,'2018-11-10 12:10:29',2,'2018-11-10 12:10:29',2),(58,55,50.00,22.00,0.50,25.25,1,'2018-11-10 12:10:34',2,'2018-11-10 12:10:34',2),(59,55,4324.00,22.00,44.00,22.00,1,'2018-11-10 12:10:21',2,'2018-11-10 12:10:21',2);
/*!40000 ALTER TABLE `tbl_stock_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user_role_menu`
--

DROP TABLE IF EXISTS `tbl_user_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user_role_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=550 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user_role_menu`
--

LOCK TABLES `tbl_user_role_menu` WRITE;
/*!40000 ALTER TABLE `tbl_user_role_menu` DISABLE KEYS */;
INSERT INTO `tbl_user_role_menu` VALUES (172,1,4),(173,1,5),(202,1,3),(203,3,3),(204,7,3),(205,9,3),(206,50,3),(321,1,6),(421,1,2),(422,61,2),(423,59,2),(424,3,2),(425,50,2),(426,60,2),(427,5,2),(428,6,2),(429,14,2),(430,49,2),(451,3,1),(452,50,1),(453,51,1),(454,52,1),(455,53,1),(456,54,1),(457,55,1),(458,56,1),(459,57,1),(460,60,1),(461,5,1),(462,6,1),(463,14,1),(464,49,1),(465,1,1),(538,1,7),(539,61,7),(540,62,7),(541,63,7),(542,3,7),(543,60,7),(544,64,7),(545,1,8),(546,61,8),(547,62,8),(548,63,8),(549,64,8);
/*!40000 ALTER TABLE `tbl_user_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'login password md5',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  `system_user` int(10) unsigned NOT NULL DEFAULT '0',
  `employee_id` varchar(20) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `extension` varchar(45) DEFAULT NULL,
  `agent_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` VALUES (1,'admin@icddrb.org','e10adc3949ba59abbe56e057f20f883e','Fahad Khan','8698368846',1,0,0,'2015-07-01 18:56:49',1,'2017-12-27 05:45:39',1,'','admin',NULL,NULL,NULL,0),(2,'fahadcse07@gmail.com','e10adc3949ba59abbe56e057f20f883e','Fahad Khan','0172404025',7,0,1,'2017-01-15 05:56:31',1,'2018-09-12 16:24:06',1,'','fahad','Programmer',NULL,NULL,0),(17,'khayrul.web@gmail.com','e10adc3949ba59abbe56e057f20f883e','Md. Khayrul Hasan','01781483634',8,0,1,'2018-08-29 11:34:34',17,'2018-09-11 12:34:55',1,'','khayrul','Consultant',NULL,NULL,2),(18,'alamin@gmail.com','e10adc3949ba59abbe56e057f20f883e','Al-amin','0178454445',8,0,17,'2018-08-30 08:58:28',1,'2018-09-11 12:27:42',1,'','alamin','Consultant',NULL,NULL,3);
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-10 18:13:00
