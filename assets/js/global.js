/*$.extend( true, $.fn.dataTable.defaults, {
	"language": {
		"processing": "<i class=\"fa fa-3x text-center fa-spin text-red ion-load-c loading-z-index\"><\/i>",
		"loadingRecords": "<i class=\"fa fa-3x text-center fa-spin text-red ion-load-c loading-z-index\"><\/i>",
		"search": "Search",
		"searchPlaceholder": "Search Records..."
	},
	"lengthMenu": [
		[10, 25, 50, 75, 100, -1],
		[10, 25, 50, 75, 100, "All"]
	],
	"dom": "<\"mrg0A col-md-2 pull-left\"l><\"col-md-offset-1 col-md-5\"B><\"col-md-3 pull-right\"f><\"clearfix\">rtip",
	buttons: {
		dom: {
			button: {
				tag: 'button',
				className: 'btn btn-sm btn-primary',
				exportOptions: {
					columns: 'th:not(.no-export)'
				}
			}
		},
		buttons: [{
			extend: 'copyHtml5',
			text: ' Copy',
			titleAttr: 'Copy to clipboard'
		}, {
			extend: 'excelHtml5',
			text: ' Excel',
			titleAttr: 'Export to excel'
		}, {
			extend: 'csvHtml5',
			text: ' CSV',
			titleAttr: 'Export to CSV'
		}, {
			extend: 'pdfHtml5',
			text: ' PDF',
			titleAttr: 'Export to PDF',
			orientation: 'landscape',
			pageSize: 'A4'
		}, {
			extend: 'print',
			text: ' Print',
			titleAttr: 'Print',
			autoPrint: true
		}]
	},
	"pagingType": "listbox",
	"drawCallback": function() {
		const SELECTOR_PAGING_LISTBOX_SELECT = $(".paging_listbox");
		if (SELECTOR_PAGING_LISTBOX_SELECT.length > 0 && typeof $.fn.select2 != "undefined") {
			SELECTOR_PAGING_LISTBOX_SELECT.find("select").css({
				"display": "inline-block"
			}).select2({
				width: "100%",
				allowClear: true,
				placeholder: function() {
					$(this).data("placeholder");
				},
				escapeMarkup: function(markup) {
					return markup;
				}
			}).focus(function() {
				$(this).select2("focus");
			});
			SELECTOR_PAGING_LISTBOX_SELECT.find(".select2-container").css({
				"display": "inline-block",
				"width": "40%"
			});
		}
	}
});*/


/*
*
* Document Ready Function
*
* */
(function($) {

  $(function() {

    const SELECTOR_BOOTSTRAP_SELECT_DROPDOWN    = $('select.form-control');
    const SELECTOR_SELECT2_CONTAINER            = $('.select2-container');

    // INITIALIZE BOOTSTRAP SELECT2 DROPDOWN
    if (SELECTOR_BOOTSTRAP_SELECT_DROPDOWN.length > 0 && typeof $.fn.select2 != 'undefined') {
      SELECTOR_BOOTSTRAP_SELECT_DROPDOWN.select2({
        // theme: 'classic'
        width: '100%',
        allowClear: true,
        placeholder: function() {
          $(this).data('placeholder');
        },
        escapeMarkup: function(markup) {
          return markup;
        }
      }).focus(function () {
        $(this).select2('focus');
      });
    }

    // INITIALIZE BOOTSTRAP SELECT2 DROPDOWN VALIDATION
    if (SELECTOR_SELECT2_CONTAINER.length > 0 && typeof $.fn.select2 != 'undefined') {
      $.each(SELECTOR_SELECT2_CONTAINER, function (i, n) {
        $(n).next().show().fadeTo(0, 0).height('0px').css('left', 'auto'); // make the original select visible for validation engine and hidden for us
        $(n).prepend($(n).next());
        $(n).delay(500).queue(function () {
          $(this).removeClass('validate[required]'); //remove the class name from select2 container(div), so that validation engine dose not validate it
          $(this).dequeue();
        });
      });
    }

  });

}) (jQuery);
// End of Document Ready Function