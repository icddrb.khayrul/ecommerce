$(document).ready(function() {

    $('.add-to-cart').on('click', function(){
        var qnt = $('#quantity').val();
        var id = $(this).attr('data-id');
        var path = $(this).attr('path'); 
        var plus_minus = $(this).attr('plusMinus');
        var post_data = {};

        if(typeof(plus_minus) != "undefined" && plus_minus !== null)
        {
            post_data = { id : id, qnt : qnt, plus_minus : plus_minus };
        }else{
            post_data = { id : id, qnt : qnt };
        }

        
            
            $.ajax({
                type: "POST",
                url: path,
                data : post_data,
                dataType: "json",
                success: function (data) {
                    if (data.status == 'plus') {
                        var curval = parseInt($('.cart-count').html());
                        $('.cart-count').html(curval+(parseInt(data.quantity)));
                    } else {
                        var curval = parseInt($('.cart-count').html());
                        $('.cart-count').html(curval-(parseInt(data.quantity)));
                    }
                }
            });
        
    });



    
    $('.cart-bag').on('click', function(){
    var path = $(this).attr('path');
    $.ajax({
            type:"post",
            url: path+'addtocart/get_cart_bag',
            dataType : 'json',
            data:{ path : path},
            success:function(response)
            {
                $(".modal-header").html(response.header);
                $(".modal-body").html(response.body);
                $(".modal-footer").html(response.footer);
                
            }
        });
    });



    $('.customer_info_save').on('click', function () {
        $('#form').validate();
    });



    $('.checkMobile').on('keyup', function () {
        var response;
        var baseurl = $(this).attr('path');
        var value = $(this).val();
    	var post_url_check_username = baseurl + "fontend/checkUsernameExist/";

    	$.ajax({
    		type: "POST",
    		url: post_url_check_username,
    		data: {
    			username: value
    		},
    		dataType: "json",
    		async: false
    	}).done(function (result) {
    		if (result == 2){
                $('#exitPhone').html('Phone number already exists!');
                $('.checkMobile').val('');
    		}else{
                $('#exitPhone').html('');
            }
    	});
    	return response;
    });



    $(document).on('click', '.delete_item',  function(){

        var path = $(this).attr('path');
        var id = $(this).attr('id');
        var quantity = $(this).attr('quantity');
        $.ajax({
            type:"post",
            url: path+'addtocart/delete_item',
            dataType : 'html',
            data:{ id : id},
            success:function(response)
            {
                $('#media'+id).remove();
                var curval = parseInt($('.cart-count').html());
                $('.cart-count').html(curval-(parseInt(quantity)));
            }
        });
    });


});